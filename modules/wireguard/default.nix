{ config, lib, pkgs, ... }:

with lib;

# configure a wireguard site-to-site network

let
  cfg = config.liketechnik.wireguard;

  server-options = { ... }: {

    options = {

      hostName = mkOption {
        example = "chameleon";
        type = types.str;
        description = ''
            The hostname of the server.

            If the server is also managed with this configuration, the hostname should match `networking.hostName`.

            Used to identify the key file names (`publicKey`, `presharedKeyFile`, `privateKeyFile`) to use when configuring the wireguard interface.
        '';
      };

      ips = mkOption {
        example = [ "10.69.0.1/16" "fdf8:c8d3:cef6:1::1/48" ];
        type = with types; listOf str;
        description = ''
          The IP addresses of the interface.
        '';
      };

      listenPort = mkOption {
        example = 4200;
        default = 4200;
        type = types.int;
        description = ''
          16-bit port for listening.

          Used to specify the endpoint to use for all client type peers of the network managed by this configuration.

          If `hostName` == `networking.hostName` of the current configuration, configures the `listenPort` of the wireguard interface
          and whitelists it in the firewall.
        '';
      };

      address = mkOption {
        example = "liketechnik.name";
        type = types.str;
        description = ''
          Hostname or IP adress under which the serve is reachable for all client peers.

          Used to specify the `endpoint`.
        '';
      };

    };

  };

  client-options  = { ... }: {

    options = {

      hostName = mkOption {
        example = "mantis";
        type = types.str;
        description = ''
            The hostname of the server. 

            This is used to specify the endpoint to use for all client type peers managed by this configuration.

            If the server is also managed with this configuration, the hostname should match `networking.hostName`.

            Used to identify the key file names (`publicKey`, `presharedKeyFile`, `privateKeyFile`) to use when configuring the wireguard interface.
        '';
      };

      ips = mkOption {
        example = [ "10.69.1.1/16" "fdf8:c8d3:cef6:2::1/48" ];
        type = with types; listOf str;
        description = ''
          The IP addresses of the interface.
        '';
      };

    };

  };

  generatePeer = allowedIPs: endpoint: server: client: hostName:
    {
      allowedIPs = allowedIPs;
      publicKey = fileContents (./. + "/../../wireguard-keys/peer_${hostName}.pub");
      presharedKeyFile = config.sops.secrets."wireguard-keys/peer_${server.hostName}-peer_${client.hostName}.psk".path;
      endpoint = endpoint;
    };

  isIPv6 = ip: hasInfix ":" ip;

  generateAllowedIPsFromClient = client:
    map (ipCIDR: let ip = head (splitString "/" ipCIDR ); in if isIPv6 ipCIDR then "${ip}/128" else "${ip}/32") client.ips;

  generateServerPeers = server: clients:
    map (client: generatePeer (generateAllowedIPsFromClient client) null server client client.hostName) clients;

  generateServer = clients: server:
    {
      ips = server.ips;
      listenPort = server.listenPort;

      privateKeyFile = config.sops.secrets."wireguard-keys/peer_${server.hostName}.key".path;

      peers = generateServerPeers server clients;
      mtu = 1280;
    };

  generateClientPeers = networks: client: servers:
    map (server: generatePeer networks "${server.address}:${toString server.listenPort}" server client server.hostName) servers;

  generateClient = networks: servers: client:
    {
      ips = client.ips;

      privateKeyFile = config.sops.secrets."wireguard-keys/peer_${client.hostName}.key".path;

      peers = generateClientPeers networks client servers;
      mtu = 1280;
    };

  generatePskSopsSecret = client: server:
    nameValuePair "wireguard-keys/peer_${server.hostName}-peer_${client.hostName}.psk" {
      format = "binary";
      sopsFile = ./. + "/../../wireguard-keys/peer_${server.hostName}-peer_${client.hostName}.psk";
    };

  generateClientSopsSecrets = client: servers: 
    {
      "wireguard-keys/peer_${client.hostName}.key" = {
        format = "binary";
        sopsFile = ./. + "/../../wireguard-keys/peer_${client.hostName}.key";
      };
    } // (listToAttrs (map (server: generatePskSopsSecret client server) servers));

  generateServerSopsSecrets = server: clients: 
    {
      "wireguard-keys/peer_${server.hostName}.key" = {
        format = "binary";
        sopsFile = ./. + "/../../wireguard-keys/peer_${server.hostName}.key";
      };
    } // (listToAttrs (map (client: generatePskSopsSecret client server) clients));

  generateHostMapping = ip: hostname:
    nameValuePair "${ip}" [ "${hostname}.liketechnik" ];

  generateHostMappings = peer:
    map (ip: generateHostMapping (head (splitString "/" ip)) peer.hostName) peer.ips;

in {

  options = {

    liketechnik.wireguard = {

      enable = mkOption {
        description = "Whether to enable WireGuard.";
        type = types.bool;
        default = cfg.servers != [] && cfg.clients != [];
        example = true;
      };

      servers = mkOption {
        description = ''
          Servers of the WireGuard network, e. g. publicly reachable peers.
        '';
        default = [];
        type = with types; listOf (submodule server-options);
      };

      clients = mkOption {
        description = ''
          Clients of the WireGuard network, e. g. not publicly reachable/sporadically online peers.
        '';
        default = [];
        type = with types; listOf (submodule client-options);
      };

      networks = mkOption {
        default = [];
        description = ''
          Network in CIDR notation. 
        '';
        example = [ "10.69.0.0/16" "fdf8:c8d3:cef6::/48" ];
        type = with types; listOf str;
      };

    };

  };

  config = mkIf cfg.enable (let
      maybeThisServer = filter (server: server.hostName == config.networking.hostName) cfg.servers;
      maybeThisClient = filter (client: client.hostName == config.networking.hostName) cfg.clients;

      onlyOneOf = (((length maybeThisServer) == 0) || ((length maybeThisClient) == 0));
      atLeastOneOf = (((length maybeThisServer) == 1) || ((length maybeThisClient) == 1));
    in {
      assertions = [
        {
          assertion = (onlyOneOf && atLeastOneOf);
          message = "The same host cannot be configured both as a server and a client and exactly one client or server must be the host this configuration is built for.";
        }
        {
          assertion = cfg.networks != [];
          message = "If the module is enabled, at least one network must be defined";
        }
      ];

      networking.hosts = listToAttrs (flatten (map generateHostMappings cfg.clients)) // listToAttrs (flatten (map generateHostMappings cfg.servers));

      networking.firewall.trustedInterfaces = [ "liketechnik" ];

      networking.wireguard.enable = true;

      networking.firewall.allowedUDPPorts = if (length maybeThisServer) == 1 then [ (head maybeThisServer).listenPort ] else [ ];

      services.dnsmasq.settings = if (length maybeThisServer) == 1 then
        let
          listenAddresses = concatMapStringsSep "," (address: head (splitString "/" address)) (head maybeThisServer).ips; 
        in {
          listen-address = listenAddresses;
        } else {};

      # make sure dnsmasq starts after the interface is established, since it crashes otherwise
      systemd.services.dnsmasq.after = if (length maybeThisServer) == 1 then [ "wireguard-liketechnik.service" ] else [];


      networking.wireguard.interfaces.liketechnik = if (length maybeThisServer) == 1 then generateServer cfg.clients (head maybeThisServer) else generateClient cfg.networks cfg.servers (head maybeThisClient);

      sops.secrets = (if (length maybeThisServer) == 1 then generateServerSopsSecrets (head maybeThisServer) cfg.clients else generateClientSopsSecrets (head maybeThisClient) cfg.servers);
  });

}
