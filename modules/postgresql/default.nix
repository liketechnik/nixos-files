{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.liketechnik.postgresql;

  user-options = { ... }: {

    options = {

      name = mkOption {
        type = types.str;
      };

      ensurePermissions = mkOption {
        type = types.attrsOf types.str;
        default = {};
        description = ''
          See services.postgresql.users.*.ensurePermissions. 
        '';
      };

    };

  };

  full-access-db-options = { ... }: {

    options = {

      dbName = mkOption {
        example = "grafana";
        type = types.str;
        description = "The name of the database.";
      };

      user = mkOption {
        example = literalExpression ''
          {
            name = "grafana";
          }
        '';
        type = types.submodule user-options;
        description = "The name of the user with full access to the database.";
      };

      schemaName = mkOption {
        example = "grafana";
        type = types.str;
        description = "The name of the schema that grants full access to the db for the user.";
      };

    };

  };

in {

  options = {

    liketechnik.postgresql = {

      enable = mkOption {
        description = "Wether to enable (and configure) services.postgresql.";
        type = types.bool;
        example = true;
      };

      fullAccessDbs = mkOption {
        description = "Name of DBs to create with a user and a default schema for the db.";
        type = with types; listOf (submodule full-access-db-options);
        default = [];
      };

      additionalUsers = mkOption {
        description = "Additional users.";
        type = with types; listOf (submodule user-options);
        default = [];
      };

    };

  };

  config = mkIf cfg.enable (let
    databaseNames = map (dbCfg: dbCfg.dbName) cfg.fullAccessDbs;
    allUsers = map (dbCfg: dbCfg.user) cfg.fullAccessDbs ++ cfg.additionalUsers;
  in {

    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_13; # hardcode current stable's default and explicitly update this :)
      ensureDatabases = databaseNames;
      # filter out ensurePermissions here,
      # which got removed for the reasons outlined in
      # https://github.com/NixOS/nixpkgs/pull/266270
      ensureUsers = map (userCfg: {name = userCfg.name; }) allUsers;
    };

    # use mkAfter so that this runs after user and db creation
    systemd.services.postgresql.postStart = lib.mkAfter ''
      $PSQL -tAc 'REVOKE USAGE ON SCHEMA public FROM PUBLIC'
      $PSQL -tAc 'REVOKE CREATE ON SCHEMA public FROM PUBLIC'

      $PSQL -tAc 'REVOKE TEMPORARY ON DATABASE postgres FROM PUBLIC'
      $PSQL -tAc 'REVOKE CONNECT ON DATABASE postgres FROM PUBLIC'

      ${lib.concatMapStrings (databaseCfg: ''
        $PSQL -tAc 'REVOKE USAGE ON SCHEMA public FROM PUBLIC' -d '${databaseCfg.dbName}'
        $PSQL -tAc 'REVOKE CREATE ON SCHEMA public FROM PUBLIC' -d '${databaseCfg.dbName}' 

        $PSQL -tAc 'REVOKE TEMPORARY ON DATABASE "${databaseCfg.dbName}" FROM PUBLIC'
        $PSQL -tAc 'REVOKE CONNECT ON DATABASE "${databaseCfg.dbName}" FROM PUBLIC'

        $PSQL -tAc 'CREATE SCHEMA IF NOT EXISTS "${databaseCfg.schemaName}" AUTHORIZATION "${databaseCfg.user.name}"' -d '${databaseCfg.dbName}'

        $PSQL -tAc 'ALTER DATABASE "${databaseCfg.dbName}" SET search_path TO "${databaseCfg.schemaName}"'
      '') cfg.fullAccessDbs}

        ${lib.concatMapStringsSep
          "\n"
          (user:
            lib.concatStringsSep
            "\n"
            (mapAttrsToList
              (database: permission:
                ''$PSQL -tAc 'GRANT ${permission} ON ${database} TO "${user.name}"' ''
              )
              user.ensurePermissions
            )
          )
          allUsers
        }
    '';

  });
}
