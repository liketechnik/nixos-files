{ config, lib, pkgs, sops-expand, ... }:

with lib;

let 
  cfg = config.services.ilias-copy;
in {
  options.services.ilias-copy.copies = mkOption {
    description = ''
      Periodic copies from ilias and automatic push of the git repo.
    '';
    default = {};
    type = types.attrsOf (types.submodule ({ name, ... }: {
      options = {
        davfsMount = mkOption {
          type = types.str;
          description = ''
            Mount point for the dav-filesystem to copy from.
          '';
        };

        davfsUrl = mkOption {
          type = types.str;
          description = ''
            Url of the dav-filesystem to copy from.
          '';
        };

        syncTarget = mkOption {
          type = types.str;
          description = ''
            Folder containing the git repo to copy to.
          '';
        };
      };
    }));
  };

  config = {
    powerManagement.resumeCommands =
      lib.strings.concatMapStrings (name:
        ''
          systemctl --no-block restart rclone-webdav-${name}.service
        ''
      ) (builtins.attrNames cfg.copies);

    powerManagement.powerDownCommands =
      lib.strings.concatMapStrings (name:
        ''
          systemctl stop ilias-copy-${name}.service
          systemctl stop rclone-webdav-${name}.service
        ''
      ) (builtins.attrNames cfg.copies);


    systemd.timers =
      mapAttrs' (name: copy:
        let
        in nameValuePair "ilias-copy-${name}" ({
          timerConfig = {
            Unit = "ilias-copy-${name}.service";
            OnBootSec = "15min";
            OnUnitActiveSec = "2h";
          };
          wantedBy = [ "multi-user.target" ];
        })
      ) cfg.copies;

    systemd.services =
      mapAttrs' (name: copy:
        let
        in nameValuePair "ilias-copy-${name}" ({
          path = with pkgs; [ coreutils rsync gnupg git openssh ];
          script = ''
            export SSH_AUTH_SOCK=$(${pkgs.gnupg}/bin/gpgconf --list-dirs agent-ssh-socket)

            ${pkgs.coreutils}/bin/echo "Syncing Ilias files"
            ${pkgs.rsync}/bin/rsync -rc --exclude="lost+found" ${copy.davfsMount}/ ${copy.syncTarget}

            cd ${copy.syncTarget}
            ${pkgs.coreutils}/bin/echo "Adding all changes to git, commit & push"
            ${pkgs.git}/bin/git add .
            # check if working tree has changes (https://stackoverflow.com/a/25149786)
            if [[ `${pkgs.git}/bin/git status --porcelain` ]]; then
              ${pkgs.git}/bin/git commit -m "UPDATE: auto am `date`"
              ${pkgs.git}/bin/git push
            else
              echo "No changes to add, no commit, no push"
            fi
          '';
          requires = [
            "rclone-webdav-${name}.service"
          ];
          serviceConfig = {
            Type = "oneshot";
            User = "florian";
          };
        })
      ) cfg.copies

      //

      mapAttrs' (name: copy: 
        let
        in nameValuePair "rclone-webdav-${name}" ({
          path = with pkgs; [ rclone coreutils "/run/wrappers" ];
          after = [ "network-online.target" ];
          requires = [ "fh-vpn.service" ];
          wantedBy = [ "default.target" ];
          # run umount to make really, really, really, sure,
          # there's not unclean state here
          script = ''
            /run/wrappers/bin/fusermount -u "${copy.davfsMount}" || true

            export USER="$(cat ${config.sops.secrets."fh/user".path})"
            export LOGIN="$(cat ${config.sops.secrets."fh/login".path} | rclone obscure -)"

            set +x
            exec rclone --verbose \
              --vfs-read-chunk-size=0 --vfs-read-chunk-size-limit=0 \
              --cache-dir "$CACHE_DIRECTORY" \
              --contimeout 5s \
              --human-readable \
              --log-systemd \
              --retries 3 \
              --retries-sleep 1s \
              --timeout 5s \
              --webdav-user="''${USER}" --webdav-pass="''${LOGIN}" --webdav-url="${copy.davfsUrl}" \
              mount :webdav:/ "${copy.davfsMount}"  \
              --read-only \
              --vfs-cache-mode full
          '';
          serviceConfig = {
            Type = "notify";
            NotifyAccess = "all";
            User = "florian";
            Restart = "always";
            RestartSec = 10;
            ExecStartPre = [
              ''${pkgs.coreutils}/bin/test -d "${copy.davfsMount}"''
              ''${pkgs.coreutils}/bin/test -w "${copy.davfsMount}"''
              ''${pkgs.coreutils}/bin/test -z "$(ls -A "${copy.davfsMount}")"''
            ];
            ExecStop = ''/run/wrappers/bin/fusermount -u "${copy.davfsMount}"'';
            CacheDirectory = "rclone-webdav-${name}";
            PrivateTmp = false; # important for mounts to actually show up...
          };
        })
      ) config.services.ilias-copy.copies;

  };
}
