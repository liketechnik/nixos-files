{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.sops-expand;
  users = config.users.users;
in {
  options.sops-expand.secrets = mkOption {
    description = ''
      Add additional content around secrets that doesn't need to be encrypted.
    '';
    type = types.attrsOf (types.submodule ({ config, ... }: {
      options = {
        targetFile = mkOption {
          type = types.str;
          description = ''
            File name for the expanded secret under /run/secrets/.

            If the file is inside a subdirectory (e. g. /run/secrets/a/file) the subdirectory
            must already exist.
          '';
        };
        expandCmd = mkOption  {
          type = types.str;
          description = ''
            Command to use for creating the expanded file.

            The stdout of the command is redirected into the target file.
          '';
        };
        mode = mkOption {
          type = types.str;
          default = "0400";
          description = ''
            Permissions mode of the in octal.
          '';
        };
        owner = mkOption {
          type = types.str;
          default = "root";
          description = ''
            User of the file.
          '';
        };
        group = mkOption {
          type = types.str;
          default = users.${config.owner}.group;
          description = ''
            Group of the file.
          '';
        };
      };
    }));
  };

  config = {
    system.activationScripts.setup-secrets-expanded = let
      expanded-secrets = attrValues cfg.secrets;
      setups = map (secret: let
        targetPath = "/run/secrets/${secret.targetFile}";
      in ''
        touch ${targetPath}
        chmod =${secret.mode} ${targetPath}
        chown ${secret.owner}:${secret.group} ${targetPath}
        ${secret.expandCmd} > ${targetPath}
      '') expanded-secrets;
    in lib.stringAfter [ "setupSecrets" ] ''
      echo setting up expanded secrets...
      ${foldl' (acc: elem: "${acc}\n${elem}") "" setups}
    '';
  };
}
