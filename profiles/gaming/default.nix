{ pkgs, inputs, lib, ... }:
let
in {
  environment.systemPackages = with pkgs; [
    lutris
    prismlauncher
  ];
  programs.steam.enable = true;

  systemd.tmpfiles.rules = [
    # inspired by opengl-driver in nixpkgs
    # minecraft versions > 20.1 (20.2+ works) (compatible with wayland too, I believe)
    "L+ /run/minecraft-libs/glfw-wayland-minecraft - - - - ${pkgs.glfw3-minecraft}"
    # minecraft versions using glfw2
    "L+ /run/minecraft-libs/glfw2 - - - - ${pkgs.glfw2}"
    "L+ /run/minecraft-libs/openal - - - - ${pkgs.openal}"
  ];
}
