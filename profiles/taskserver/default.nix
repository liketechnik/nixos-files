{ config, lib, pkgs, ... }:

{
  services.taskserver = {
    enable = true;

    listenHost = "::";
    fqdn = "${config.networking.hostName}.liketechnik";

    organisations = {
      personal = {
        users = [ "florian" ];
      };
    };
  };
}
