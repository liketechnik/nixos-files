{ pkgs, lib, sops, sops-expand, config, ... }:
let
in {
  imports = [ ../../modules/ilias-copy ];

  sops.secrets."fh/user" = {
    sopsFile = ../../secrets/fh.yaml;
    owner = "florian";
  };
  sops.secrets."fh/login" = {
    sopsFile = ../../secrets/fh.yaml;
    owner = "florian";
  };
  sops.secrets."fh/otp" = {
    sopsFile = ../../secrets/fh.yaml;
    owner = "florian";
  };
  sops.secrets."fh/vpn/server".sopsFile = ../../secrets/fh.yaml;
  sops.secrets."fh/vpn/domains".sopsFile = ../../secrets/fh.yaml;
  sops.secrets."iwd/eduroam.8021x" = {
    format = "binary";
    sopsFile = ../../secrets/iwd/eduroam.8021x;
    path = "/var/lib/iwd/eduroam.8021x";
  };

  services.davfs2 = {
    enable = true;
    settings = {
      globalSection = {
        cache_size = 500; # MiByte, default 50
        gui_optimize = 1; # 0 false, 1 true; tries to check for changes in a directory with only 1 request
        connect_timeout = 5;
        read_timeout = 5;
        max_retry = 15;
      };
    };
  };

  # systemd.services."ensure-dns-available" = {
  #   script = ''
  #     export SERVER="$(cat ${config.sops.secrets."fh/vpn/server".path})"
  #     while ! ${pkgs.host}/bin/host $SERVER
  #     do
  #       sleep 30
  #     done
  #   '';
  #   requires = [ "network-online.target" ];
  #   serviceConfig = {
  #     Type = "oneshot";
  #   };
  # };

  # environment.etc.hosts.mode = "0644"; # allow vpn-slice to edit available hosts
  # systemd.services."fh-vpn" = {
  #   path = with pkgs; [
  #     coreutils
  #     openconnect
  #     vpn-slice
  #     oath-toolkit
  #   ];
  #   /**
  #     Usage: oathtool [OPTION]... [KEY [OTP]]...
# Generate and validate OATH one-time passwords.  KEY and OTP is the string '-'
# to read from standard input, '@FILE' to read from indicated filename, or a hex
# encoded value (not recommended on multi-user systems).

  # -h, --help                    Print help and exit
  # -V, --version                 Print version and exit
  #     --hotp                    use event-based HOTP mode  (default=on)
  #     --totp[=MODE]             use time-variant TOTP mode (values "SHA1",
  #                                 "SHA256", or "SHA512")  (default=`SHA1')
  # -b, --base32                  use base32 encoding of KEY instead of hex
  #                                 (default=off)
  # -c, --counter=COUNTER         HOTP counter value
  # -s, --time-step-size=DURATION TOTP time-step duration  (default=`30s')
  # -S, --start-time=TIME         when to start counting time steps for TOTP
  #                                 (default=`1970-01-01 00:00:00 UTC')
  # -N, --now=TIME                use this time as current time for TOTP
  #                                 (default=`now')
  # -d, --digits=DIGITS           number of digits in one-time password
  # -w, --window=WIDTH            number of additional OTPs to generate or
  #                                 validate against
  # -v, --verbose                 explain what is being done  (default=off)
  # **/
  #   script = ''
  #     export PASSWD="$(cat ${config.sops.secrets."fh/login".path})"
  #     export OTP="$(cat ${config.sops.secrets."fh/otp".path} | oathtool --totp -b -)"
  #     export USER="$(cat ${config.sops.secrets."fh/user".path})"
  #     export DOMAINS="$(cat ${config.sops.secrets."fh/vpn/domains".path})"
  #     export SERVER="$(cat ${config.sops.secrets."fh/vpn/server".path})"
  #     echo -e "$PASSWD\n$OTP" | openconnect --passwd-on-stdin $SERVER -u $USER -s \
  #   "vpn-slice $DOMAINS" 2>&1
  #   '';
  #   requires = [ "network-online.target" "ensure-dns-available.service" ];
  #   wantedBy = [ "default.target"];
  #   serviceConfig = {
  #     Restart = "on-failure";
  #     RestartSec = "60s";
  #   };
  # };

  environment.systemPackages = lib.optionals (config.services.xserver.enable) [
    pkgs.teamspeak_client
    pkgs.jetbrains.idea-ultimate
    pkgs.jetbrains.pycharm-professional
    #pkgs.staruml
  ];

}
