{ config, pkgs, lib, ... }:
{
  services.rss2email = {
    enable = true;

    to = "rss@liketechnik.name";

    interval = "7m";

    config = {
      from = "rss2email@liketechnik.name";
      force-from = true;
      name-format = "{feed-name}: {author} - {feed-title}";
      subject-format = "{feed-name}: {feed-title}";
    };

    feeds = {
    };
  };

  sops.secrets."rss2email/feeds.cfg" = {
    format = "binary";
    sopsFile = ../../secrets/rss2email/feeds.cfg;
    owner = config.users.users.rss2email.name;
    group = config.users.groups.rss2email.name;
  };

  systemd.services.rss2email-setup = let
    conf = pkgs.writeText "rss2email.cfg" (lib.generators.toINI {} ({
      DEFAULT = config.services.rss2email.config;
    }));
  in {
    description = "Setup for rss2email";
    serviceConfig.RemainAfterExit = true;
    serviceConfig.Type = "oneshot";
    script = ''
      if [ -f /var/rss2email/conf.cfg ]
      then
        chmod +w /var/rss2email/conf.cfg
      fi
      cp ${conf} /var/rss2email/conf.cfg
      chmod +w /var/rss2email/conf.cfg
      cat ${config.sops.secrets."rss2email/feeds.cfg".path} >> /var/rss2email/conf.cfg
      chmod -w /var/rss2email/conf.cfg
    '';
    serviceConfig = {
      User = config.systemd.services.rss2email.serviceConfig.User;
    };
  };

  systemd.services.rss2email.after = [ "rss2email-setup.service" ];
  systemd.services.rss2email.requires = [ "rss2email-setup.service" ];
  systemd.services.rss2email.serviceConfig.ExecStart = lib.mkForce
    "${pkgs.rss2email}/bin/r2e -c /var/rss2email/conf.cfg -d /var/rss2email/db.json run";
}
