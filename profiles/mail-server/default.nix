{ config, lib, pkgs, ... }:
let
  dovecot-sql-conf = pkgs.writeText "dovecot-sql.conf" ''
    driver=pgsql
    connect = "host=/run/postgresql/ dbname=vmail user=vmail"
    default_pass_scheme = SHA512-CRYPT

    password_query = SELECT username AS user, domain, password FROM accounts WHERE username = '%Ln' AND domain = '%Ld' and enabled = true;
    user_query = SELECT concat('*:storage=', quota, 'M') AS quota_rule FROM accounts WHERE username = '%Ln' AND domain = '%Ld' AND sendonly = false;
    iterate_query = SELECT username, domain FROM accounts where sendonly = false;
  '';

  postfix-submission-header-cleanup = pkgs.writeText "submission_header_cleanup" ''
    # Remove privacy related headers

    /^Received:/            IGNORE
    /^X-Originating-IP:/    IGNORE
    /^X-Mailer:/            IGNORE
    /^User-Agent:/          IGNORE
  '';

  postfix-sql-accounts = pkgs.writeText "accounts.cf" ''
    user = vmail
    hosts = unix:/run/postgresql/
    dbname = vmail
    query = select 1 as found from accounts where username = '%u' and domain = '%d' and enabled = true LIMIT 1;
  '';

  postfix-sql-aliases = pkgs.writeText "aliases.cf" ''
    user = vmail
    hosts = unix:/run/postgresql/
    dbname = vmail
    query = SELECT DISTINCT concat(destination_username, '@', destination_domain) AS destinations FROM aliases
        WHERE (source_username = '%u' OR source_username IS NULL) AND source_domain = '%d'
        AND enabled = true
        AND NOT EXISTS (SELECT id FROM accounts WHERE username = '%u' and domain = '%d');
  '';

  postfix-sql-domains = pkgs.writeText "domains.cf" ''
    user = vmail
    hosts = unix:/run/postgresql/
    dbname = vmail
    query = SELECT domain FROM domains WHERE domain='%s';
  '';

  postfix-sql-recipient-access = pkgs.writeText "recipient-access.cf" ''
    user = vmail
    hosts = unix:/run/postgresql/
    dbname = vmail
    query = select case when sendonly = true then 'REJECT' else 'OK' end AS access from accounts where username = '%u' and domain = '%d' and enabled = true LIMIT 1;
  '';

  postfix-sql-sender-login-maps = pkgs.writeText "sender-login-maps.cf" ''
    user = vmail
    hosts = unix:/run/postgresql/
    dbname = vmail
    query = select concat(username, '@', domain) as 'owns' from accounts where username = '%u' AND domain = '%d' and enabled = true union select 
        concat(destination_username, '@', destination_domain) AS 'owns' from aliases 
        where source_username = '%u' and source_domain = '%d' and enabled = true;
  '';

  postfix-sql-tls-policy = pkgs.writeText "tls-poliy.cf" ''
    user = vmail
    hosts = unix:/run/postgresql/
    dbname = vmail
    query = SELECT policy, params FROM tlspolicies WHERE domain = '%s';
  '';
in {
  networking.firewall.allowedTCPPorts = [ 993 ];

  users.extraUsers.vmail = {
    description = "vmail";
    isSystemUser = true;
    home = "/var/vmail";
    createHome = true;
  };

  users.groups.vmail = {};
  users.users.vmail.group = "vmail";

  users.extraUsers.vmailadmin = {
    description = "vmailadmin";
    isSystemUser = true;
  };

  users.groups.vmailadmin = {};
  users.users.vmailadmin.group = "vmailadmin";

  liketechnik.postgresql = {
    enable = true;
    fullAccessDbs = [
      {
        dbName = "vmail";
        schemaName = "vmail";
        user = {
          name = "vmailadmin";
          ensurePermissions = {
            "DATABASE \"vmail\"" = "CONNECT";
          };
        };
      }
    ];
    additionalUsers = [
      {
        name = "vmail";
        ensurePermissions = {
          "DATABASE \"vmail\"" = "CONNECT";
        };
      }
    ];
  };

  services.postgresql.identMap = ''
    mailmap dovecot2 vmail
    mailmap postfix vmail
    mailmap postgres vmail
    mailmap postgres vmailadmin
    mailmap vmailadmin vmailadmin
    mailmap vmail vmail
  '';
  services.postgresql.authentication = ''
    local vmail vmail   peer map=mailmap
    local vmail vmailadmin   peer map=mailmap
  '';

  sops.secrets."mail-server/table-content.sql" = {
    format = "binary";
    sopsFile = ../../secrets/mail-server/table-content.sql;
    owner = config.users.extraUsers.vmailadmin.name;
    group = config.users.groups.vmailadmin.name;
  };

  sops.secrets."mail-server/dkim_2022.key" = {
    format = "binary";
    sopsFile = ../../secrets/mail-server/dkim_2022.key;
    owner = config.users.extraUsers.rspamd.name;
    group = config.users.groups.rspamd.name;
  };

  systemd.services."vmail-db-setup" = {
    wants = [ "postgresql.service" ]; # not supposed to be restarted with postgres, therefore no require
    after = [ "postgresql.service" ];   
    wantedBy = [ "default.target" "postfix.target" "dovecot.target" ];

    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
      User = config.users.extraUsers.vmailadmin.name;
    };

    path = [ config.services.postgresql.package ];

    script = ''
      set -e
      PSQL="psql -v ON_ERROR_STOP=1 --port=${toString config.services.postgresql.settings.port} -d vmail"

      $PSQL -f ${./sql/table-creation.sql}
      $PSQL -f ${./sql/table-cleanup.sql}
      $PSQL -f ${config.sops.secrets."mail-server/table-content.sql".path}
    '';
  };

  # prevent postfix from erroring out if the ip is not assigned yet
  systemd.services.postfix = {
    wants = [ "network-online.target" ];
    after = [ "network-online.target" ];
  };
  systemd.services.postfix-setup = {
    wants = [ "network-online.target" ];
    after = [ "network-online.target" ];
  };

  security.acme.certs."liketechnik.name".extraDomainNames = [ "chameleon.liketechnik.name" "imap.liketechnik.name" "smtp.liketechnik.name" ];

  services.dovecot2 = {
    enable = true;

    enableImap = true;
    enableLmtp = true;
    protocols = [ "sieve" ];

    sslServerCert = config.security.acme.certs."liketechnik.name".directory + "/fullchain.pem";
    sslServerKey = config.security.acme.certs."liketechnik.name".directory + "/key.pem";

    mailUser = config.users.extraUsers.vmail.name;
    mailGroup = config.users.groups.vmail.name;
    createMailUser = false;

    mailLocation = "maildir:~/mail:LAYOUT=fs";

    mailboxes = {
      spam = {
        name = "Spam";
        auto = "subscribe";
        specialUse = "Junk";
      };
      trash = {
        name = "Trash";
        auto = "subscribe";
        specialUse = "Trash";
      };
      drafts = {
        name = "Drafts";
        auto = "subscribe";
        specialUse = "Drafts";
      };
      sent = {
        name = "Sent";
        auto = "subscribe";
        specialUse = "Sent";
      };
    };

    modules = [ pkgs.dovecot_pigeonhole ];

    sieve.scripts = {
      global = ./sieve-scripts;
    };

    mailPlugins.globally.enable = [ "old_stats" ];

    # override the module generated configuration file
    # (some parts conflict with what I want to achieve,
    # but I still want to benefit from e. g. the sieve script pre-compilation)
    configFile = pkgs.writeText "dovecot.conf" ''
      default_internal_user = ${config.services.dovecot2.user}
      default_internal_group = ${config.services.dovecot2.group}

      protocols = imap lmtp sieve

      # rest of ssl is configured with module options above
      # https://ssl-config.mozilla.org/#server=dovecot&version=2.3.9&config=intermediate&openssl=1.1.1d&guideline=5.6
      ssl = required
      ssl_cert = <${config.security.acme.certs."liketechnik.name".directory + "/fullchain.pem"}
      ssl_key = <${config.security.acme.certs."liketechnik.name".directory + "/key.pem"}
      ssl_dh = <${config.security.dhparams.params.dovecot2.path}

      ssl_min_protocol = TLSv1.2
      ssl_cipher_list = ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
      ssl_prefer_server_ciphers = no

      mail_plugins = $mail_plugins old_stats

      service imap-login {
        inet_listener imap {
          port = 143
        }
      }

      service managesieve-login {
        inet_listener sieve {
          port = 4190
        }
      }

      service lmtp {
        unix_listener ${config.services.postfix.config.queue_directory}/private/dovecot-lmtp  {
          mode = 0660
          group = postfix
          user = postfix
        }

        user = vmail
      }

      service auth {
          ### Auth socket für Postfix
          unix_listener ${config.services.postfix.config.queue_directory}/private/auth {
              mode = 0660
              user = postfix
              group = postfix
          }
      
          ### Auth socket für LMTP-Dienst
          unix_listener auth-userdb {
              mode = 0660
              user = vmail
              group = vmail
          }
      }

      service old-stats {
        unix_listener old-stats {
          user = dovecot-exporter
          group = dovecot-exporter
          mode = 0600
        }
        fifo_listener old-stats-mail {
          mode = 0600 
          user = vmail
          group = vmail
        }
        fifo_listener old-stats-user {
          mode = 0660
          user = vmail
          group = vmail
        }
      }

      protocol imap {
        mail_plugins = $mail_plugins quota imap_quota imap_sieve imap_old_stats
        mail_max_userip_connections = 20
        imap_idle_notify_interval = 29 mins
      }
      
      protocol lmtp {
          postmaster_address = postmaster@liketechnik.name
          mail_plugins = $mail_plugins sieve notify push_notification
      }

      disable_plaintext_auth = yes
      auth_mechanisms = plain login
      auth_username_format = %Lu

      passdb {
        driver = sql
        args = ${dovecot-sql-conf}
      }

      userdb {
        driver = sql
        args = ${dovecot-sql-conf}
      }

      recipient_delimiter = +

      mail_uid = vmail
      mail_gid = vmail
      mail_privileged_group = vmail

      mail_home = /var/vmail/mailboxes/%d/%n
      mail_location = maildir:~/mail:LAYOUT=fs

      namespace inbox {
        inbox = yes
    
        mailbox Spam {
          auto = subscribe
          special_use = \Junk
        }
    
        mailbox Trash {
          auto = subscribe
          special_use = \Trash
        }
    
        mailbox Drafts {
          auto = subscribe
          special_use = \Drafts
        }
    
        mailbox Sent {
          auto = subscribe
          special_use = \Sent
        }
      }

      plugin {
        sieve_plugins = sieve_imapsieve sieve_extprograms
        sieve_before = /var/lib/dovecot/global/spam-global.sieve
        sieve = file:/var/vmail/sieve/%d/%n/scripts;active=/var/vmail/sieve/%d/%n/active-script.sieve

        ###
        ### Spam learning
        ###
        # From elsewhere to Spam folder
        imapsieve_mailbox1_name = Spam
        imapsieve_mailbox1_causes = COPY
        imapsieve_mailbox1_before = file:/var/lib/dovecot/sieve/global/learn-spam.sieve

        # From Spam folder to elsewhere
        imapsieve_mailbox2_name = *
        imapsieve_mailbox2_from = Spam
        imapsieve_mailbox2_causes = COPY
        imapsieve_mailbox2_before = file:/var/lib/dovecot/sieve/global/learn-ham.sieve

        sieve_pipe_bin_dir = /usr/bin
        sieve_global_extensions = +vnd.dovecot.pipe

        quota = maildir:User quota
        quota_exceeded_message = Benutzer %u hat das Speichervolumen überschritten. / User %u has exhausted allowed storage space.

        ###
        ### Stats
        ###

        old_stats_refresh = 30 secs
        old_status_track_cmds = yes
      }
    '';
  };
  services.prometheus.exporters.dovecot = {
    enable = config.services.prometheus.enable;
    socketPath = "/var/run/dovecot/old-stats";
    scopes = [ "global" "user" "domain" "ip" ]; # session and command disabled for now since it causes errors 
  };

  security.dhparams = {
    enable = true;
    params.postfix = {};
  };

  services.postfix = {
    enable = true;

    networks = [ "127.0.0.0/8" "[::ffff:127.0.0.0]/104" "[::1]/128" ];
    hostname = "chameleon.liketechnik.name";

    sslCert = config.security.acme.certs."liketechnik.name".directory + "/fullchain.pem";
    sslKey = config.security.acme.certs."liketechnik.name".directory + "/key.pem";

    recipientDelimiter = "+";

    config = {
      inet_interfaces = "127.0.0.1, ::1, 95.217.161.154, 2a01:4f9:c011:75c6::1, 10.69.0.1, fdf8:c8d3:cef6:1::1";
    
      maximal_queue_lifetime = "1h";
      bounce_queue_lifetime = "1h";
      maximal_backoff_time = "15m";
      minimal_backoff_time = "5m";
      queue_run_delay = "5m";

      tls_preempt_cipherlist = "no";
      tls_ssl_options = "NO_COMPRESSION";
      tls_medium_cipherlist = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";

      smtp_tls_security_level = "dane";
      smtp_dns_support_level = "dnssec";
      smtp_tls_policy_maps = "proxy:pgsql:${postfix-sql-tls-policy}";
      smtp_tls_session_cache_database = "btree:$${data_directory}/smtp_scache";
      smtp_tls_ciphers = "medium";

      smtpd_tls_security_level = "may";
      smtpd_tls_auth_only = "yes";
      smtpd_tls_ciphers = "medium";
      smtpd_tls_mandatory_protocols = "!SSLv2, !SSLv3, !TLSv1, !TLSv1.1";
      smtpd_tls_protocols = "!SSLv2, !SSLv3, !TLSv1, !TLSv1.1";
      smtpd_tls_session_cache_database = "btree:$${data_directory}/smtpd_scache";
      smtpd_tls_dh1024_param_file = "${config.security.dhparams.params.postfix.path}";

      virtual_transport = "lmtp:unix:private/dovecot-lmtp";

      smtpd_milters = "inet:127.0.0.1:11332";
      non_smtpd_milters = "inet:127.0.0.1:11332";
      milter_protocol = "6";
      milter_mail_macros =  "i {mail_addr} {client_addr} {client_name} {auth_authen}";
      milter_default_action = "accept";
      smtpd_relay_restrictions = ''reject_non_fqdn_recipient
        reject_unknown_recipient_domain
        permit_mynetworks
        reject_unauth_destination
      '';

      smtpd_recipient_restrictions = "check_recipient_access proxy:pgsql:${postfix-sql-recipient-access}";

      smtpd_client_restrictions = ''permit_mynetworks
        reject_unknown_client_hostname
      '';

      smtpd_helo_required = "yes";
      smtpd_helo_restrictions = ''permit_mynetworks
        reject_invalid_helo_hostname
        reject_non_fqdn_helo_hostname
        reject_unknown_helo_hostname
      '';

      smtpd_data_restrictions = "reject_unauth_pipelining";

      mua_relay_restrictions = "reject_non_fqdn_recipient,reject_unknown_recipient_domain,permit_mynetworks,permit_sasl_authenticated,reject";
      mua_sender_restrictions = "permit_mynetworks,reject_non_fqdn_sender,reject_sender_login_mismatch,permit_sasl_authenticated,reject";
      mua_client_restrictions = "permit_mynetworks,permit_sasl_authenticated,reject";

      proxy_read_maps = ''proxy:pgsql:${postfix-sql-aliases}
        proxy:pgsql:${postfix-sql-accounts}
        proxy:pgsql:${postfix-sql-domains}
        proxy:pgsql:${postfix-sql-recipient-access}
        proxy:pgsql:${postfix-sql-sender-login-maps}
        proxy:pgsql:${postfix-sql-tls-policy}
      '';

      virtual_alias_maps = "proxy:pgsql:${postfix-sql-aliases}";
      virtual_mailbox_maps = "proxy:pgsql:${postfix-sql-accounts}";
      virtual_mailbox_domains = "proxy:pgsql:${postfix-sql-domains}";
      local_recipient_maps = "$virtual_mailbox_maps ";

      mailbox_size_limit = "0";

      message_size_limit = "52428800";

      biff = "no";

      append_dot_mydomain = "no";

      disable_vrfy_command = "yes";
    };

    enableSmtp = false; # manually enabled below
    masterConfig = {
      smtp = {
        type = "inet";
        private = false;
        chroot = true;
        maxproc = 1;
        command = "smtpd";
        args = [ "-o" "smtpd_sasl_auth_enable=no" ];
      };

      smtps = {
        type = "inet";
        private = false;
        chroot = true;
        command = "smtpd";
        args = [
          "-o" "syslog_name=postfix/smtps"
          "-o" "smtpd_tls_wrappermode=yes"
          "-o" "smtpd_tls_security_level=encrypt"
          "-o" "smtpd_sasl_auth_enable=yes"
          "-o" "smtpd_sasl_type=dovecot"
          "-o" "smtpd_sasl_path=private/auth"
          "-o" "smtpd_sasl_security_options=noanonymous"
          "-o" "smtpd_client_restrictions=$mua_client_restrictions"
          "-o" "smtpd_sender_restrictions=$mua_sender_restrictions"
          "-o" "smtpd_relay_restrictions=$mua_relay_restrictions"
          "-o" "milter_macro_daemon_name=ORIGINATING"
          "-o" "smtpd_sender_login_maps=proxy:pgsql:${postfix-sql-sender-login-maps}"
          "-o" "smtpd_helo_required=no"
          "-o" "smtpd_helo_restrictions="
          "-o" "cleanup_service_name=submission-header-cleanup"
        ];
      };

      submission = {
        type = "inet";
        private = false;
        chroot = true;
        command = "smtpd";
        args = [
          "-o" "syslog_name=postfix/submission"
          "-o" "smtpd_tls_security_level=encrypt"
          "-o" "smtpd_sasl_auth_enable=yes"
          "-o" "smtpd_sasl_type=dovecot"
          "-o" "smtpd_sasl_path=private/auth"
          "-o" "smtpd_sasl_security_options=noanonymous"
          "-o" "smtpd_client_restrictions=$mua_client_restrictions"
          "-o" "smtpd_sender_restrictions=$mua_sender_restrictions"
          "-o" "smtpd_relay_restrictions=$mua_relay_restrictions"
          "-o" "milter_macro_daemon_name=ORIGINATING"
          "-o" "smtpd_sender_login_maps=proxy:pgsql:${postfix-sql-sender-login-maps}"
          "-o" "smtpd_helo_required=no"
          "-o" "smtpd_helo_restrictions="
          "-o" "cleanup_service_name=submission-header-cleanup"
        ];
      };

      pickup = {
        type = "unix";
        private = false;
        chroot = true;
        wakeup = 60;
        maxproc = 1;
      };

      cleanup = {
        type = "unix";
        private = false;
        chroot = true;
        maxproc = 0;
      };

      qmgr = {
        type = "unix";
        private = false;
        chroot = false;
        wakeup = 300;
        maxproc = 1;
      };

      tlsmgr = {
        type = "unix";
        chroot = true;
        wakeup = 1000;
        wakeupUnusedComponent = false;
        maxproc = 1;
      };

      rewrite = {
        type = "unix";
        chroot = true;
        command = "trivial-rewrite";
      };

      bounce = {
        type = "unix";
        chroot = true;
        maxproc = 0;
      };

      defer = {
        type = "unix";
        chroot = true;
        maxproc = 0;
        command = "bounce";
      };

      trace = {
        type = "unix";
        chroot = true;
        maxproc = 0;
        command = "bounce";
      };

      verify = {
        type = "unix";
        chroot = true;
        maxproc = 1;
      };

      flush = {
        type = "unix";
        private = false;
        chroot = true;
        wakeup = 1000;
        wakeupUnusedComponent = false;
        maxproc = 0;
      };

      proxymap = {
        type = "unix";
        chroot = false;
      };

      proxywrite = {
        type = "unix";
        chroot = false;
        maxproc = 1;
        command = "proxymap";
      };

      smtp_unix = {
        type = "unix";
        chroot = true;
        command = "smtp";
      };

      relay = {
        type = "unix";
        chroot = true;
        command = "smtp";
      };

      showq = {
        type = "unix";
        private = false;
        chroot = true;
      };

      error = {
        type = "unix";
        chroot = true;
      };

      retry = {
        type = "unix";
        chroot = true;
        command = "error";
      };

      discard = {
        type = "unix";
        chroot = true;
      };

      local = {
        type = "unix";
        privileged = true;
        chroot = false;
      };

      virtual = {
        type = "unix";
        privileged = true;
        chroot = false;
      };

      lmtp = {
        type = "unix";
        chroot = true;
      };

      anvil = {
        type = "unix";
        chroot = true;
        maxproc = 1;
      };

      scache = {
        type = "unix";
        chroot = true;
        maxproc = 1;
      };

      submission-header-cleanup = {
        type = "unix";
        private = false;
        chroot = true;
        maxproc = 0;
        command = "cleanup";
        args = [ "-o" "header_checks=regexp:${postfix-submission-header-cleanup}" ];
      };
    };

    extraConfig = ''

    '';
  };
  services.prometheus.exporters.postfix = {
    enable = config.services.prometheus.enable;
    systemd = {
      enable = true;
    };
  };

  services.rspamd = let
    dkim-signing-conf = ''
      path = "${config.sops.secrets."mail-server/dkim_2022.key".path}";
      selector = "2022";

      ### enable DKIM signing for alias sender addresses
      allow_username_mismatch = true;
    '';
  in {
    enable = true;

    workers = {
      normal = {
        includes = [ "$CONFDIR/worker-normal.inc" ];
        bindSockets = [ "*:11333" ];
      };
      controller = {
        includes = [ "$CONFDIR/worker-controller.inc" ];
        bindSockets = [ "*:11334" ];
      };
      rspamd_proxy = {
        includes = [ "$CONFDIR/worker-proxy.inc" ];
        bindSockets = [ "*:11332" ];
      };
    };

    locals = {
      "worker-controller.inc".text = ''
        secure_ip = ["::1", "127.0.0.1", "10.69.0.1", "10.69.1.1", "fdf8:c8d3:cef6:1::1", "fdf8:c8d3:cef6:2::1"];
      '';
      "logginc.inc".text = ''
        type = "syslog";
        level = "warning";
      '';
      "milter_headers.conf".text = ''
        use = ["x-spamd-bar", "x-spam-level", "authentication-results"];
        authenticated_headers = ["authentication-results"];
      '';
      "classifier-bayes.conf".text = ''
        backend = "redis";
      '';
      "redis.conf".text = ''
        servers = "127.0.0.1";
      '';
      "dkim_signing.conf".text = dkim-signing-conf;
      "arc.conf".text = dkim-signing-conf;
    };
    overrides = {
      "classifier-bayes.conf".text = ''
        autolearn = true;
      '';
    };
  };

  services.redis = {
    vmOverCommit = true;
    servers."" = {
      enable = true;
      settings = {
        maxmemory = "100M";
      };
    };
  };
  services.prometheus.exporters.redis = {
    enable = config.services.prometheus.enable;
  };
}
