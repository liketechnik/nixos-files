{ config, lib, pkgs, ... }: let
  localhost_static_conf = port: {
    targets = [
      "localhost:${toString port}"
    ];
    labels = {
      "host" = "${config.networking.hostName}";
    };
  };
in {
  liketechnik.postgresql = {
    enable = true;
    fullAccessDbs = [
      {
        dbName = "grafana";
        schemaName = "grafana";
        user = {
          name = "grafana";
          ensurePermissions = {
            "DATABASE grafana" = "CONNECT";
          };
        };
      }
    ];
  };

  services.prometheus = {
    enable = true;

    port = 9090;
    webExternalUrl = "http://${config.networking.hostName}.liketechnik";

    scrapeConfigs = [
      {
        job_name = "prometheus";
        static_configs = [
          (localhost_static_conf config.services.prometheus.port)
        ];
      }
      { 
        job_name = "node";
        scrape_interval = "10s";
        static_configs = [
          (localhost_static_conf config.services.prometheus.exporters.node.port)
        ];
      }
      {
        job_name = "grafana";
        static_configs = [
          (localhost_static_conf config.services.grafana.settings.server.http_port)
        ];
      }
    ] ++ lib.optionals (config.services.prometheus.exporters.redis.enable) [
      {
        job_name = "redis";
        static_configs = [
          (localhost_static_conf config.services.prometheus.exporters.redis.port)
        ];
      }
    ] ++ lib.optionals (config.services.prometheus.exporters.nginx.enable) [
      {
        job_name = "nginx";
        static_configs = [
          (localhost_static_conf config.services.prometheus.exporters.nginx.port)
        ];
      }
    ] ++ lib.optionals (config.services.prometheus.exporters.postfix.enable) [
      {
        job_name = "postfix";
        static_configs = [
          (localhost_static_conf config.services.prometheus.exporters.postfix.port)
        ];
      }
    ] ++ lib.optionals (config.services.prometheus.exporters.dovecot.enable) [
      {
        job_name = "dovecot";
        static_configs = [
          (localhost_static_conf config.services.prometheus.exporters.dovecot.port)
        ];
      }
    ] ++ lib.optionals (config.services.prometheus.exporters.dnsmasq.enable) [
      {
        job_name = "dnsmasq";
        static_configs = [
          (localhost_static_conf config.services.prometheus.exporters.dnsmasq.port)
        ];
      }
    ] ++ lib.optionals (config.services.prometheus.exporters.postgres.enable) [
      {
        job_name = "postgres";
        static_configs = [
          (localhost_static_conf config.services.prometheus.exporters.postgres.port)
        ];
      }
    ];

    retentionTime = "30d";

    exporters = {
      node = {
        enable = true;
        port = 9100;
        enabledCollectors = [
          "ethtool"
          "interrupts"
          "logind"
          "processes"
          "systemd"
          "wifi"
        ];
      };
    };

    alertmanager = {
      enable = true;
      configuration = {
        global = {
          smtp_from = "prometheus@liketechnik.name";
          smtp_smarthost = "chameleon.liketechnik.name:25";
          smtp_hello = "chameleon.liketechnik.name";
          smtp_require_tls = false;
          resolve_timeout = "5m";
        };
        route = {
          group_by = [ "host" ];
          group_wait = "30s";
          group_interval = "5m";
          repeat_interval = "4h";
          receiver = "email-alerts-liketechnik";
          routes = [];
        };
        receivers = [
          {
            name = "email-alerts-liketechnik";
            email_configs = [
              {
                send_resolved = true;
                to = "alerts@liketechnik.name";
              }
            ];
          }
        ];
      };
    };
  };

  services.grafana = {
    enable = true;

    settings.server.domain = "${config.networking.hostName}.liketechnik";
    settings.server.http_port = 3000;
    settings.server.http_addr = ""; # listen on all interfaces, which is ok due to to firewall

    settings.database = {
      type = "postgres";
      name = "grafana";
      host = "/run/postgresql:${toString config.services.postgresql.settings.port}";
      user = "grafana";
    };

    settings = {
      smtp = {
        enable = true;
        user = "";
        password = "";
        fromAddress = "grafana@liketechnik.name";
        host = "chameleon.liketechnik.name:25";
      };
    };

    provision = {
      enable = true;
      datasources.settings.datasources = [
        {
          name = "Prometheus - ${config.networking.hostName}";
          type = "prometheus";
          access = "proxy";
          orgId = 1;
          isDefault = true;
          editable = false;
          url = "http://localhost:${toString config.services.prometheus.port}";
        }
        {
          name = "Alertmanager (Prometheus) - ${config.networking.hostName}";
          type = "alertmanager";
          access = "proxy";
          orgId = 1;
          isDefault = false;
          editable = false;
          url = "http://localhost:${toString config.services.prometheus.alertmanager.port}";
          jsonData = {
            implementation = "prometheus";
          };
        }
      ];
    };
  };
}
