{ pkgs, config, ... }:
{
  services.dnsmasq = {
    enable = true;
    settings = {
      domain-needed = true;
      bogus-priv = true;
      listen-address = [ "127.0.0.1,::1" ];
      bind-interfaces = true;
      cache-size = 5000;
      # read (and update with)
      addn-hosts = "/etc/hosts";
      local = [
        # do not forward any dns queries for fritz.box to upstream dns servers
        # (only looked up from /etc/hosts)
        "/fritz.box/"
      ];
      server = [
        "2620:fe::fe"
        "9.9.9.9"
        "2620:fe::9"
        "149.112.112.112"
      ];
    };
    # strict-order: query in order of specified name servers,
    # should make sure the customs ones are queried before the one
    # of the active connection set via NetworkManager
  };
  services.prometheus.exporters.dnsmasq = {
    enable = config.services.prometheus.enable;
    leasesPath = "/var/lib/dnsmasq/dnsmasq.leases";
  };

  networking.networkmanager = {
    dns = "default";
    # rc-manager = "resolvconf";
  };

  networking.nameservers = [ "127.0.0.1" ];

  services.vnstat.enable = true;

  networking.wireguard.enable = true;
  networking.firewall.checkReversePath = false; # get wireguard to work?

  liketechnik.wireguard = {
    networks = [
      "10.69.0.0/16"
      "fdf8:c8d3:cef6::/48"
    ];
    servers = [
      {
        hostName = "chameleon";
        ips = [
          "10.69.0.1/16"
          "fdf8:c8d3:cef6:1::1/48"
        ];
        listenPort = 4200;
        address = "liketechnik.name";
      }
    ];
    clients = [
      {
        hostName = "mantis";
        ips = [
          "10.69.1.1/16"
          "fdf8:c8d3:cef6:2::1/48"
        ];
      }
      {
        hostName = "grasshopper";
        ips = [
          "10.69.1.2/16"
          "fdf8:c8d3:cef6:2::2/48"
        ];
      }
      {
        hostName = "florian-76k";
        ips = [
          "10.69.1.3/16"
          "fdf8:c8d3:cef6:2::3/48"
        ];
      }
    ];
  };
}
