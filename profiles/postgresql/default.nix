{ config, pkgs, lib, ... }:
{
  liketechnik.postgresql = {
    enable = true;
    fullAccessDbs = [
      {
        dbName = "florian";
        schemaName = "florian";
        user = {
          name = "florian";
          ensurePermissions = {
            "DATABASE florian" = "ALL PRIVILEGES";
          };
        };
      }
    ];
  };

  services.postgresql.ensureUsers = [
    {
      name = config.services.prometheus.exporters.postgres.user;
    }
  ];
  systemd.services.postgresql.postStart = lib.mkAfter ''
    $PSQL -tAc 'GRANT pg_monitor TO "${config.services.prometheus.exporters.postgres.user}"'
    ${lib.concatMapStrings (dbName: ''
      $PSQL -tAc 'GRANT CONNECT ON DATABASE "${dbName}" TO "${config.services.prometheus.exporters.postgres.user}"'
    '') config.services.postgresql.ensureDatabases}
    $PSQL -tAc 'GRANT CONNECT ON DATABASE postgres TO "${config.services.prometheus.exporters.postgres.user}"'
  '';
  services.prometheus.exporters.postgres = {
    enable = config.services.prometheus.enable;
    dataSourceName = "user=${config.services.prometheus.exporters.postgres.user} host=/var/run/postgresql/ sslmode=disable dbname=postgres";
    extraFlags = [ "--auto-discover-databases" "--exclude-databases" "telegraf" ];
  };
}
