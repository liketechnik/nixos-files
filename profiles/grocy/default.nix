{ config, lib, pkgs, ... }:
let
  hostName = "${config.networking.hostName}.liketechnik";
in {
  services.grocy = {
    inherit hostName;

    enable = lib.warn "fixme: grocy currently marked as broken" false;

    settings = {
      currency = "EUR";
      culture = "de";
      calendar = {
        showWeekNumber = true;
        firstDayOfWeek = 1;
      };
    };
    nginx.enableSSL = false;
  };

  # keep this local (/vpn) and http only for now
  # make sure to add common header from nginx when enabling
  # publicly with ssl
  services.nginx.virtualHosts.${hostName} = {
    useACMEHost = "liketechnik.name";
    listen = [
      {
        addr = "0.0.0.0";
        port = 6543;
      }
      {
        addr = "[::0]";
        port = 6543;
      }
    ];
  };
}
