{ config, lib, pkgs, ... }:
let
  workspace = "/var/lib/ki-lecture-builder/workspace";
  webroot = "/var/lib/ki-lecture-builder/www";
  website-builder = ''
set -xe -o pipefail

if [[ -d ${workspace}/bin ]]
then
  rm -rf ${workspace}/bin
fi
mkdir ${workspace}/bin
ln -s ${pkgs.bash}/bin/bash ${workspace}/bin/sh
export PATH="$PATH:${workspace}/bin"

repo=https://github.com/KI-Vorlesung/Lecture.git
workspace=${workspace}
webroot=${webroot}

if [ ! -d $workspace ]
then
    mkdir $workspace
fi
if [ ! -d $webroot ]
then
    mkdir $webroot
fi

cd $workspace

if [ ! -d repo ]
then
    mkdir repo
    git clone --recurse-submodules $repo repo
fi

if [ -d tmp-webroot ]
then
    rm -r tmp-webroot
fi
mkdir tmp-webroot

cd repo

# checkout branc that always exists
git checkout master
# update repo, respecting rebases
git pull --rebase
# update submodules
git submodule update
# update remotes
git remote update --prune origin

set +e
for branch in $(git branch -r --format '%(refname:lstrip=-1)' | grep -v HEAD | grep -v gh-pages)
do
    echo Building branch $branch

    # cleanup previous results
    make clean
    git clean -fd
    git reset --hard

    # update
    git checkout $branch
    git pull --rebase
    git submodule update

    echo "baseURL: \"https://ki-lecture-builds.liketechnik.name/$branch/\"" > local.yaml

    export DOCKER=false
    make web
    mv docs ../tmp-webroot/$branch
done
set -e

cd ..

rm -rf $webroot/*
mv tmp-webroot/* $webroot/
cat >> $webroot/index.html<< EOF
<!DOCTYPE html>
    <html lang="de">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>KI W21 Preview Builds</title>
      </head>
      <body>
        <h1>KI W21 Preview Builds</h1>
        <h2 style="color: red;">Important: All builds (except 'master') are pre-published versions. Therfore they may contain incorrect content; if unsure wait for the official release of the relevant section(s) and make sure to recheck key parts against the published version later on!</h2>

        <section>
          The following branches are currently worked on in the repo:
          <ul>
EOF
for branch in $(ls $webroot/ | grep -v index.html)
do
    echo "<li><a href=\"./$branch\">$branch</a></li>" >> $webroot/index.html
done
cat >> $webroot/index.html<< EOF
          </ul>
        </section>

        <footer>
        The source of all builds is the <a href="https://github.com/KI-Vorlesung/Lecture">offical GitHub repository of the lecture</a>.
        I did not create any of the content, I'm just providing builds of the in-progress versions.
      </body>
    </html>
EOF
  '';
in {
  systemd.timers.ki-lecture-builder = {
    timerConfig = {
      Unit = "ki-lecture-builder";
      OnCalendar = "00/2:00"; # build every 2 hours
    };
    wantedBy = [ "multi-user.target" ];
  };

  systemd.services.ki-lecture-builder = {
    path = with pkgs; [ 
      git
      gnumake
      hugo
      pandoc
      graphviz
      texlive.combined.scheme-full
    ];
    script = website-builder;
    serviceConfig = {
      Type = "oneshot";
      User = "ki-lecture-builder";
      StateDirectory = "ki-lecture-builder";
    };
  };

  users.extraUsers.ki-lecture-builder = {
    name = "ki-lecture-builder";
    group = "ki-lecture-builder";
    isSystemUser = true;
  };
  users.extraGroups.ki-lecture-builder = {
    members = [ "ki-lecture-builder" ];
  };
}
