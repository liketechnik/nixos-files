{ pkgs, inputs, lib, ... }:
{

  programs.neovim = {
    enable = true;
    package = pkgs.neovim-unwrapped;
    # package = pkgs.neovim.overrideAttrs (oa: {
    #   buildInputs = oa.buildInputs ++ ([
    #     pkgs.unstable.tree-sitter
    #   ]);
    # });
    # package = pkgs.neovim-nightly.overrideAttrs (oa: {
    #   buildInputs = oa.buildInputs ++ ([
    #     pkgs.unstable.tree-sitter
    #   ]);
    # });
    defaultEditor = true;
    withRuby = false;
    configure = (import ./customization.nix { pkgs = pkgs; lib = lib; inputs = inputs; });
  };

  environment = {

    systemPackages = with pkgs; [
      deno
      python3
      ctags
      nodePackages.bash-language-server
      ccls
      (lib.warn "fixme: vscode-langservers-extracted build failure" (lib.mkIf false vscode-langservers-extracted))
      nodePackages.dockerfile-language-server-nodejs
      python3Packages.python-lsp-server
      nodePackages.yaml-language-server
      nodePackages.vim-language-server
      texlab
      #rnix-lsp
      nil
      nodePackages.diagnostic-languageserver
      shellcheck
      languagetool
      elixir_ls
      elmPackages.elm-language-server
      haskell-language-server
      metals
      tinymist

      jdt-language-server

      # clangd (c, c++, etc lang server)
      # and other cli tools
      clang-tools clang-manpages
      cmake-language-server
      # create compilation db for clangd from e.g. plain make
      bear
    ];

  };

}
