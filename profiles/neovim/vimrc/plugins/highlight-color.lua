require('nvim-highlight-colors').setup({})
-- disable by default
-- (it does lag the screen for a uncomfortable amount, sadly)
-- (keybind is in keybinds.vim)
require("nvim-highlight-colors").turnOff()
