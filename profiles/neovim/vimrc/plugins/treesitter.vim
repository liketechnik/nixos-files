lua <<EOF
require'nvim-treesitter.configs'.setup {
  ignore_install = "all",
  -- TODO deduplicate disable function
  highlight = { 
    enable = true,
    disable = function(lang, buf)
        if lang == "hare" then
            return
        end
        return true
    end,
  }, 
  incremental_selection = { 
    enable = false, 
  },
  indent = {
    enable = true,
    disable = function(lang, buf)
        if lang == "hare" then
            return
        end
        return true
    end,
  },
}
EOF

set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
