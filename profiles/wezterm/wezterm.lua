local wezterm = require 'wezterm';

local opacity = 0.9;

wezterm.on("toggle-opacity", function(window, pane)
    local overrides = window:get_config_overrides() or {}

    if not overrides.window_background_opacity or overrides.window_background_opacity == opacity then
        overrides.window_background_opacity = 1.0;
    else
        overrides.window_background_opacity = opacity;
    end

    window:set_config_overrides(overrides)
end);

wezterm.on("my-color-scheme-toggle", function(window, pane)
    local overrides = window:get_config_overrides() or {}

    local config = window:effective_config()

    if config.color_scheme == "Atelier Savanna (base16)" then
        overrides.color_scheme = "Atelier Savanna Light (base16)"
    else
        overrides.color_scheme = "Atelier Savanna (base16)"
    end

    window:set_config_overrides(overrides)
end);

wezterm.on("window-config-reloaded", function(window, pane)
end)

wezterm.on("update-right-status", function(window, pane)
   -- Each element holds the text for a cell in a "powerline" style << fade
   local cells = {};
  

  -- Figure out the cwd and host of the current pane.
  -- This will pick up the hostname for the remote host if your
  -- shell is using OSC 7 on the remote host.
  local cwd_uri = pane:get_current_working_dir()
  if cwd_uri then
    local cwd = ''
    local hostname = ''

    if type(cwd_uri) == 'userdata' then
      -- Running on a newer version of wezterm and we have
      -- a URL object here, making this simple!

      cwd = cwd_uri.file_path
      hostname = cwd_uri.host or wezterm.hostname()
    else
      -- an older version of wezterm, 20230712-072601-f4abf8fd or earlier,
      -- which doesn't have the Url object
      cwd_uri = cwd_uri:sub(8)
      local slash = cwd_uri:find '/'
      if slash then
        hostname = cwd_uri:sub(1, slash - 1)
        -- and extract the cwd from the uri, decoding %-encoding
        cwd = cwd_uri:sub(slash):gsub('%%(%x%x)', function(hex)
          return string.char(tonumber(hex, 16))
        end)
      end
    end

    -- Remove the domain name portion of the hostname
    local dot = hostname:find '[.]'
    if dot then
      hostname = hostname:sub(1, dot - 1)
    end
    if hostname == '' then
      hostname = wezterm.hostname()
    end

    table.insert(cells, cwd)
    table.insert(cells, hostname)
  end
  
    local date = wezterm.strftime("KW%W %Y-%m-%d %H:%M:%S");
    table.insert(cells, date);
  
    -- An entry for each battery (typically 0 or 1 battery)
    for _, b in ipairs(wezterm.battery_info()) do
        table.insert(cells, string.format("%.0f%%", b.state_of_charge * 100))
    end
  
    -- The powerline < symbol
    local LEFT_ARROW = utf8.char(0xe0b3);
    -- The filled in variant of the < symbol
    local SOLID_LEFT_ARROW = utf8.char(0xe0b2)
  
    -- Color palette for the backgrounds of each cell
    -- local colors = {
    --     "#171c19", -- base00
    --     "#232a25", -- base01
    --     "#526057", -- base02
    --     "#5f6d64", -- base03
    --     "#ecf4ee", -- base07 (was base05 with tomorrow night colors)
    -- };
    local colors = {
        "#070d0b", -- base00
        "#2a3c35", -- base02
        "#070d0b", -- base00
        "#2a3c35", -- base02
        "#070d0b", -- base00
    };
  
    -- Foreground color for the text across the fade
    -- local text_fg = "#87928a"; -- base05 (was base04 with tomorrow night colors)
    local text_fg = "cafbe9"; -- base05 (was base04 with tomorrow night colors)
  
    -- The elements to be formatted
    local elements = {};
    -- How many cells have been formatted
    local num_cells = 0;
  
    -- Translate a cell into elements
    function push(text, is_last)
        local cell_no = num_cells + 1
        table.insert(elements, {Foreground={Color=text_fg}})
        table.insert(elements, {Background={Color=colors[cell_no]}})
        table.insert(elements, {Text=" "..text.." "})
        if not is_last then
            table.insert(elements, {Foreground={Color=colors[cell_no+1]}})
            table.insert(elements, {Text=SOLID_LEFT_ARROW})
        end
        num_cells = num_cells + 1
    end
  
    while #cells > 0 do
        local cell = table.remove(cells, 1)
        push(cell, #cells == 0)
    end
  
    window:set_right_status(wezterm.format(elements));
end);

return {
  -- work around https://github.com/wez/wezterm/issues/5990
  -- front_end = "WebGpu",
    font = wezterm.font({
        family="Recursive Mn Lnr St",
        harfbuzz_features={"dlig=1", "ss01=1", "ss02=1", "ss03=1", "ss04=1", "ss05=1", "ss06=1", "ss07=1", "ss08=1", "ss09=1", "ss10=1", "ss11=1", "ss12=1", "frac=0"}
    }),
    -- font_rules = {
    --     {
    --         italic = true,
    --         intensity = "Bold",
    --         font = wezterm.font("VictorMono Nerd Font", {italic=true, weight="Bold"}),
    --     },
    --     {
    --         italic = true,
    --         font = wezterm.font("VictorMono Nerd Font", {italic=true}),
    --     },
    --     {
    --         intensity = "Bold",
    --         font = wezterm.font("VictorMono Nerd Font", {weight="Bold"}),
    --     },
    -- },
    font_size = 12,
    window_background_opacity = opacity,
    scrollback_lines = 10000,
    -- force enable ligatures
    -- harfbuzz_features = {"calt=1", "clig=1", "liga=1"},
    prefer_egl = true,
    -- using wayland with ime requires zwp_text_input_v3 support
    -- (https://wezfurlong.org/wezterm/config/lua/config/use_ime.html)
    -- river does not support that yet :/
    -- but let's ignore that for now; I'm not using IME
    enable_wayland = true,
    use_ime = false,
    -- I like to just close windows and reopen them later on...
    window_close_confirmation = "AlwaysPrompt",
    keys = {
        {key="0", mods="ALT", action="ResetFontSize"},
        {key="Enter", mods="ALT", action="DisableDefaultAssignment"},
        {key="o", mods="ALT", action=wezterm.action{EmitEvent="toggle-opacity"}},
        {key="c", mods="ALT", action=wezterm.action{EmitEvent="my-color-scheme-toggle"}},
        {key="t", mods="SHIFT|SUPER", action=wezterm.action{SpawnTab="CurrentPaneDomain"}},
        {key="t", mods="SUPER", action=wezterm.action{SpawnCommandInNewTab={
          -- hack to make nu have all env variables loaded...
          args = { "bash", "-l", "-c", "exec nu" },
        }}},
        {
            key = 'u',
            mods = 'CTRL|SHIFT',
            action = wezterm.action.DisableDefaultAssignment,
        },
    },
    ssh_domains = {
        {
            name = "liketechnik.name",
            remote_address = "liketechnik.name",
            username = "florian",
        }
    },
    unix_domains = {
        {
            name = "default",
            -- doesn't really work currently
            connect_automatically = false,
        }
    },
    use_fancy_tab_bar = false,
    -- circus theme
    -- colors = {
    --     foreground = "#a7a7a7",
    --     background = "#191919",
    --     cursor_bg = "#a7a7a7",
    --     cursor_border = "#a7a7a7",
    --     cursor_fg = "#202020",
    --     selection_bg = "#303030",
    --     selection_fg = "#a7a7a7",

    --     ansi = {"#191919", "#dc657d", "#4bb1a7", "#84b97c", "#639ee4", "#b888e2", "#c3ba63", "#a7a7a7"},
    --     brights = {"#303030", "#dc657d", "#84b97c", "#c3ba63", "#639ee4", "#b888e2", "#4bb1a7", "#ffffff"},
    -- }
    -- colors = {
    --     foreground = "#c5c8c6",
    --     background = "#1d1f21",
    --     cursor_bg = "#c5c8c6",
    --     cursor_border = "#c5c8c6",
    --     cursor_fg = "#282a23",
    --     selection_bg = "#373b41",
    --     selection_fg = "#c5c8c6",

    --     ansi = {"#1d1f21", "#cc6666", "#b5bd68", "#a3685a", "#81a2be", "#b294bb", "#8abeb7", "#c5c8c6"},
    --     brights = {"#373b41", "#cc6666", "#b294bb", "#f0c674", "#8abeb7", "#de935f", "#81a2be", "#e0e0e0"},
    -- }
    -- color_scheme = "Atelier Savanna (base16)",
    -- colors = {
    --     foreground = '#b7e0cd',
    --     background = '#020b06',
    --     cursor_bg = '#b7e0cd',
    --     cursor_fg = '#020b06',
    --     cursor_border = '#020b06',

    --     selection_fg = '#020b06',
    --     selection_bg = '#b7e0cd',

    --     ansi = {
    --         '#061b12',
    --         '#b786ec',
    --         '#a4c89c',
    --         '#cacd7d',
    --         '#74c6e6',
    --         '#77abef',
    --         '#61d7d6',
    --         '#a3c2b3',
    --     },

    --     brights = {
    --         '#a9b7b5',
    --         '#cda2ff',
    --         '#9af583',
    --         '#dee04a',
    --         '#45d0fe',
    --         '#89bcff',
    --         '#22f2f3',
    --         '#a9d2be',
    --     },
    -- },
    -- base16 glowing-greens,
    -- baseXX to property taken from https://github.com/wez/wezterm/blob/ff2743748c238e5cdd6e7cb4ed19517d08dd511f/lua-api-crates/color-funcs/src/schemes/base16.rs
    colors = {
        foreground = 'cafbe9', -- base05
        background = '070d0b', -- base00

        cursor_fg = '070d0b', -- base00
        cursor_bg = 'cafbe9', -- base05
        cursor_border = 'cafbe9', -- base05

        selection_bg = 'cafbe9', -- base05
        selection_fg = '070d0b', -- base00

        -- ansi = {
        --     '001c0f', -- base00
        --     'eb4344', -- base08
        --     '4f9f3d', -- base0b
        --     'aa58f3', -- base0a
        --     'e6329d', -- base0d
        --     'ab8219', -- base0e
        --     '229e8c', -- base0c
        --     'eadbfd', -- base05
        -- },
        ansi = { -- customized per base16 scheme to better match ansi meaning
                 -- (cf. https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit)
            '131e1a', -- base01 black
            'fee0dd', -- base08 red
            'b4fdb5', -- base0b green
            'fee6a7', -- base09 yellow
            'd2ecfe', -- base0d blue
            'f1e0fe', -- base0e purple
            '9cfbfa', -- base0c cyan
            'd3ffee', -- base06 white
        },

        -- general idea: red greens blues
        -- get enough attention already,
        -- don't need differentiation between bright/non-bright
        -- green need to differentiate for nushell though
        -- so make cyans the same
        -- blacks: 00, 0c
        -- reds: 08, 08
        -- greens: 0b, 0f
        -- yellows: 0e, 04
        -- blues: 09, 05
        -- magentas: 0a, 0d
        -- cyans: 03, 03
        -- whites: 06, 07

        -- brights = {
        --     'd6b6fc', -- base03
        --     'eb4344', -- base08
        --     '4f9f3d', -- base0b
        --     'aa58f3', -- base0a
        --     'e6329d', -- base0d
        --     'ab8219', -- base0e
        --     '229e8c', -- base0c
        --     'f9f4ff', -- base07
        -- },
        brights = {
                 -- (cf. https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit)
            '2a3c35', -- base02 bright black
            'fff0ee', -- base08 bright red
            'dcffdc', -- base0f bright gree
            'fff3d4', -- base04 bright yellow
            'e9f6ff', -- base05 bright blue
            'f8f0ff', -- base0d bright magenta
            'cefffe', -- base03 bright cyan
            'ebfff7', -- base07 bright white
        },

        -- indexed = {
        --     [16] = '4186f0', -- base09
        --     [17] = '1d9ba1', -- base0f
        --     [18] = '003822', -- base01
        --     [19] = '12281d', -- base02
        --     [20] = 'e1c8ff', -- base04
        --     [21] = 'f4edfe', -- base06
        -- },
        -- indexed = { -- customized
        --     [16] = '003822', -- base0e
        --     [17] = 'ab8219', -- base0b
        --     [18] = '229e8c', -- base0c
        --     [19] = '1d9ba1', -- base0f
        --     [20] = '4f9f3d', -- base03
        --     [21] = 'd6b6fc', -- base09
        -- },
    },
}
