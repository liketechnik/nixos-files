{ 
  pkgs, 
  inputs,
  lib,
  config, 
  ...
}:
let
  wezterm = pkgs.symlinkJoin {
    name = "${pkgs.wezterm.name}-configured";

    nativeBuildInputs = [ pkgs.makeWrapper ];

    paths = [ pkgs.wezterm ];

    postBuild = ''
      for wrap in wezterm wezterm-gui wezterm-mux-server
      do
          rm "$out/bin/$wrap"
          makeWrapper "${pkgs.wezterm}/bin/$wrap" "$out/bin/$wrap" --add-flags "--config-file ${./wezterm.lua}"
      done
    '';
    };
in {
    # although wezterm is a gui program,
    # I want it on servers too, so I can use it's ssh/tcp server features
    environment.systemPackages = [
      wezterm
    ];
}
