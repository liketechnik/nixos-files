{ lib, pkgs, config, ... }:
{
  services.jupyterhub = {
    enable = lib.warn "fixme: jupyterhub build failure; jupyterhub service temporarily disabled" false;
    host = "127.0.0.1";
    port = 8000;
    extraConfig = ''
        c.SystemdSpawner.mem_limit = '0.5G'
        c.SystemdSpawner.cpu_limit = 0.5
        c.SystemdSpawner.isolate_tmp = True
        c.SystemdSpawner.isolate_devices = True
        c.SystemdSpawner.disable_user_sudo = True
        c.SystemdSpawner.readonly_paths = ['/']
        # unfortunenately the whole home directory needs to be writable :(
        c.SystemdSpawner.readwrite_paths = ['/home/{USERNAME}']
        c.SystemdSpawner.user_workingdir = '/home/{USERNAME}'
        # TODO use BindPaths= + TemporaryFileSystem= to make /home and /home/{USERNAME}/.ssh invisble + unwritable but /home/{USERNAME} itself available
        c.SystemdSpawner.unit_extra_properties = {'ProtectKernelTunables': 'true', 'ProtectKernelLogs': 'true', 'ProtectControlGroups': 'true', 'OOMScoreAdjust': '1000', 'ProtectProc': 'invisible', 'NoNewPrivileges': 'true', 'RestrictAddressFamilies': 'AF_UNIX AF_INET AF_INET6 AF_NETLINK', 'DevicePolicy': 'closed', 'RestrictSUIDSGID': 'yes', 'LockPersonality': 'yes', 'RestrictRealtime': 'yes'}

        c.Authenticator.allowed_groups = {'jupyterhub-users'}
        c.Authenticator.admin_users = {'florian'}

        c.Authenticator.create_system_users = True
        c.Authenticator.add_user_cmd = ['${pkgs.shadow}/bin/useradd', '--create-home', '--home-dir', '/home/USERNAME', '--shell', '/run/current-system/sw/bin/nologin', '--gid', 'nogroup', '--groups', 'jupyterhub-users']
    '';
    authentication = "jupyterhub.auth.PAMAuthenticator";
    spawner = "systemdspawner.SystemdSpawner";
    kernels = {
      python3 = let
        env = (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
          ipykernel
        ]));
      in {
        displayName = "Python 3";
        argv = [
          env.interpreter
          "-m"
          "ipykernel_launcher"
          "-f"
          "{connection_file}"
        ];
        language = "python";
        logo32 = "${env}/${env.sitePackages}/ipykernel/resources/logo-32x32.png";
        logo64 = "${env}/${env.sitePackages}/ipykernel/resources/logo-64x64.png";
      };
      python3-ai = let
        env = (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
          ipykernel
          numpy
          matplotlib
          matplotlib-inline
          scikit-learn
          h5py
          scipy
          pillow
        ]));
      in {
        displayName = "Python 3 for AI";
        argv = [
          env.interpreter
          "-m"
          "ipykernel_launcher"
          "-f"
          "{connection_file}"
        ];
        language = "python";
        logo32 = "${env}/${env.sitePackages}/ipykernel/resources/logo-32x32.png";
        logo64 = "${env}/${env.sitePackages}/ipykernel/resources/logo-64x64.png";
      };
      # ruby = let
      # in {
      #   displayName = "Ruby";
      #   argv = [
      #     "${pkgs.iruby}/bin/iruby" 
      #     "kernel"
      #     "{connection_file}"
      #   ];
      #   language = "ruby";
      #   logo32 = "${pkgs.iruby.gems.iruby}/lib/ruby/gems/2.7.0/gems/iruby-0.3/logo/logo-32x32.png";
      #   logo64 = "${pkgs.iruby.gems.iruby}/lib/ruby/gems/2.7.0/gems/iruby-0.3/logo/logo-64x64.png";
      # };
    };
  };
  users.extraGroups."jupyterhub-users" = {
    members = [ "florian" ];
  };
}
