{ config, pkgs, lib, ... }:
{
  users.extraUsers.staticman = {
    description = "Staticman Service";
    isSystemUser = true;
  };

  users.groups.staticman = {};
  users.users.staticman.group = "staticman";

  sops.secrets."staticman".owner = config.users.extraUsers.staticman.name;

  systemd.services.staticman = {
    enable = false;
    wantedBy = [ "default.target" ];
    path = [ pkgs.bash pkgs.staticman ];
    script = lib.warn "staticman outdated, service disabled" "";
    # ''
    #   set -e
    #   export NODE_ENV=production
    #   set -a
    #   . ${config.sops.secrets."staticman".path}
    #   exec ${pkgs.staticman}/bin/staticman
    # '';
    serviceConfig = {
      User = config.users.extraUsers.staticman.name;
    };
  };
}
