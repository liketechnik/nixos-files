{ inputs, pkgs, lib, config, ... }:
let
  commonHeaders = ''
    # Add HSTS header with preloading to HTTPS requests.
    # Adding this header to HTTP requests is discouraged
    add_header Strict-Transport-Security $hsts_header always;

    # Enable CSP for your services.
    add_header Content-Security-Policy $cspheader always;

    # Minimize information leaked to other domains
    add_header 'Referrer-Policy' 'origin-when-cross-origin';

    # Disable embedding as a frame
    add_header X-Frame-Options DENY;

    # Prevent injection of code in other mime types (XSS Attacks)
    add_header X-Content-Type-Options nosniff always;

    # Enable XSS protection of the browser.
    # May be unnecessary when CSP is configured properly (see above)
    add_header X-XSS-Protection "1; mode=block" always;
  '';
in {
  sops.secrets."fh/sgse-deploy/basic-auth" = {
    sopsFile = ../../secrets/fh/website-auth-sgse2023.htpasswd;
    owner = config.users.extraUsers.nginx.name;
    group = config.users.extraUsers.nginx.group;
    format = "binary";
  };

  users.groups.sgse-deploy = {};
  users.extraUsers.sgse-deploy = {
    description = "user for deployments during current semester";
    group = config.users.groups.sgse-deploy.name;
    openssh.authorizedKeys.keys = let
      rsyncdConf = pkgs.writeText "rsyncd.conf" ''
        port = 8873
        [staging]
          use chroot = false
          syslog tag = rsyncd.%RSYNC_USER_NAME%.deploy-staging
          path = /var/www/sgse2023/./staging
          munge symlinks = yes
          max connections = 5
          open noatime = yes
          incoming chmod = 750
          lock file = /var/www/sgse2023/rsyncd.lock
          read only = false
        [prod]
          use chroot = false
          syslog tag = rsyncd.%RSYNC_USER_NAME%.prod
          path = /var/www/sgse2023/./prod
          munge symlinks = yes
          max connections = 5
          open noatime = yes
          incoming chmod = 750
          lock file = /var/www/sgse2023/rsyncd.lock
          read only = false
        [latest]
          use chroot = false
          syslog tag = rsyncd.%RSYNC_USER_NAME%.latest
          path = /var/www/sgse2023/./latest
          munge symlinks = yes
          max connections = 5
          open noatime = yes
          incoming chmod = 750
          lock file = /var/www/sgse2023/rsyncd.lock
          read only = false
      '';
    in [
      "restrict,command=\"${pkgs.rsync}/bin/rsync --server --daemon --config=${rsyncdConf} /var/www/sgse2023/\" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL4wpR/q3O8FmRbXKqBD4qU1tH9M6N+/xs9G4m+hMpYT sgse-deploy-2023-05-08"
    ];
    home = "/var/www/sgse2023";
    createHome = true;
    homeMode = "750";
    isNormalUser = true;
  };
  users.extraUsers.${config.services.nginx.user}.extraGroups = [ config.users.groups.sgse-deploy.name ];

  security.acme.certs."rdfcsa-js.liketechnik.name".extraDomainNames = [ "staging.rdfcsa-js.liketechnik.name" "latest.rdfcsa-js.liketechnik.name" ];

  services.nginx.virtualHosts."rdfcsa-js.liketechnik.name" = {
    forceSSL = true;
    enableACME = true;

    basicAuthFile = config.sops.secrets."fh/sgse-deploy/basic-auth".path;

    root = "/var/www/sgse2023/prod/";

    locations = {

      "~* \"^/([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?(?<p10>[^/]*/)?(?!index\\.html)(?<p11>[^/]*)\\.(?<p12>.*)$\"" = {
        priority = 990;
        tryFiles = ''
          /$p11.$p12
          /$p10/$p11.$p12
          /$9/$p10/$p11.$p12
          /$8/$9/$p10/$p11.$p12
          /$7/$8/$9/$p10/$p11.$p12
          /$6/$7/$8/$9/$p10/$p11.$p12
          /$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$2/$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$1/$2/$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          =404
        '';
      };

      # error handler for url matching above: if the path is too long,
      # the regex fails, and without this handler would simply server index.html
      "~* \"^/?.*/(?!index\\.html)[^/]*\\.[^/]*$\"" = {
        priority = 991;
        return = "500";
      };

      "/" = {
        priority = 992;
        index = "index.html";
        tryFiles = "$uri $uri/index.html /index.html";
      };
    };

    extraConfig = commonHeaders + ''
      include ${pkgs.mailcap}/etc/nginx/mime.types;
      # mailcap has a list that is too large for the default size
      types_hash_max_size 4096;
      # this one is missing..
      types {
        application/javascript mjs;
      }
    '';
  };

  services.nginx.virtualHosts."latest.rdfcsa-js.liketechnik.name" = {
    forceSSL = true;
    useACMEHost = "rdfcsa-js.liketechnik.name";

    basicAuthFile = config.sops.secrets."fh/sgse-deploy/basic-auth".path;

    root = "/var/www/sgse2023/latest/";

    locations = {

      "~* \"^/([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?(?<p10>[^/]*/)?(?!index\\.html)(?<p11>[^/]*)\\.(?<p12>.*)$\"" = {
        priority = 990;
        tryFiles = ''
          /$p11.$p12
          /$p10/$p11.$p12
          /$9/$p10/$p11.$p12
          /$8/$9/$p10/$p11.$p12
          /$7/$8/$9/$p10/$p11.$p12
          /$6/$7/$8/$9/$p10/$p11.$p12
          /$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$2/$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          /$1/$2/$3/$4/$5/$6/$7/$8/$9/$p10/$p11.$p12
          =404
        '';
      };

      # error handler for url matching above: if the path is too long,
      # the regex fails, and without this handler would simply server index.html
      "~* \"^/?.*/(?!index\\.html)[^/]*\\.[^/]*$\"" = {
        priority = 991;
        return = "500";
      };

      "/" = {
        priority = 992;
        index = "index.html";
        tryFiles = "$uri $uri/index.html /index.html";
      };
    };

    extraConfig = commonHeaders + ''
      include ${pkgs.mailcap}/etc/nginx/mime.types;
      # mailcap has a list that is too large for the default size
      types_hash_max_size 4096;
      # this one is missing..
      types {
        application/javascript mjs;
      }
    '';
  };

  services.nginx.virtualHosts."staging.rdfcsa-js.liketechnik.name" = {
    forceSSL = true;
    useACMEHost = "rdfcsa-js.liketechnik.name";

    basicAuthFile = config.sops.secrets."fh/sgse-deploy/basic-auth".path;

    root = "/var/www/sgse2023/staging/";

    locations = {

      "= /" = {
        extraConfig = ''
          autoindex on;
        '';
      };

      "~* \"^/(pr[0-9]{1,6})/([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?([^/]*/)?(?<p10>[^/]*/)?(?<p11>[^/]*/)?(?!index\\.html)(?<p12>[^/]*)\\.(?<p13>.*)$\"" = {
        priority = 990;
        tryFiles = ''
          /$1/$p12.$p13
          /$1/$p11/$p12.$p13
          /$1/$p10/$p11/$p12.$p13
          /$1/$9/$p10/$p11/$p12.$p13
          /$1/$8/$9/$p10/$p11/$p12.$p13
          /$1/$7/$8/$9/$p10/$p11/$p12.$p13
          /$1/$6/$7/$8/$9/$p10/$p11/$p12.$p13
          /$1/$5/$6/$7/$8/$9/$p10/$p11/$p12.$p13
          /$1/$4/$5/$6/$7/$8/$9/$p10/$p11/$p12.$p13
          /$1/$3/$4/$5/$6/$7/$8/$9/$p10/$p11/$p12.$p13
          /$1/$2/$3/$4/$5/$6/$7/$8/$9/$p10/$p11/$p12.$p13
          =404
        '';
      };

      # error handler for url matching above: if the path is too long,
      # the regex fails, and without this handler would simply server index.html
      "~* \"^/(pr[0-9]{1,6})/?.*/(?!index\\.html)[^/]*\\.[^/]*$\"" = {
        priority = 991;
        return = "500";
      };

      "~* \"^\\/(pr[0-9]{1,6})\\/\"" = {
        priority = 992;
        index = "index.html";
        tryFiles = "$uri $uri/index.html /$1/index.html";
      };
    };

    extraConfig = commonHeaders + ''
      include ${pkgs.mailcap}/etc/nginx/mime.types;
      # mailcap has a list that is too large for the default size
      types_hash_max_size 4096;
      # this one is missing..
      types {
        application/javascript mjs;
      }
    '';
  };

}
