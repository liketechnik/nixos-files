{ config, lib, pkgs, inputs, ... }:
let
  # the extra options currently break starting nextcloudcmd,
  # due to resulting in "''" being part of the arguments when
  # added like `"''${NC_CMD_EXTRA_OPTIONS}" \` to the options
  serviceScript = pkgs.writeShellScript "nextcloud-sync-start" ''
      export NC_CMD_EXTRA_OPTIONS=
      .  "$1"

      exec nextcloudcmd --non-interactive \
        --path "''${NC_REMOTE_ROOT:?}" \
        --user "''${NC_REMOTE_USER:?}" \
        --password "''${NC_REMOTE_PASSWORD:?}" \
        "''${NC_LOCAL_DIR:?}" \
        "''${NC_SERVER_URL:?}"
  '';
  service = pkgs.writeTextDir "lib/systemd/user/nextcloud-sync@.service" ''
    [Unit]
    Description=nextcloudcmd folder sync (%i)

    [Service]
    Type=oneshot
    ExecStart=${serviceScript} %h/.config/nextcloud-sync/%i.env
  '';
  timer = pkgs.writeTextDir "lib/systemd/user/nextcloud-sync@.timer" ''
    [Unit]
    Description=run nextcloud-sync@%i hourly

    [Timer]
    OnUnitActiveSec=1h
    OnBootSec=30m
  '';
  # combine service and timer into one common package.
  # interestingly, needs to be part of both `environment.systemPackages`
  # *and* `systemd.packages` for the units to show up in /run/current-system/sw/lib/systemd/user/
  # Usage:
  # ln -s /run/current-system/sw/lib/systemd/user/nextcloud-sync@.timer ~/.config/systemd/user/timers.target.wants/nextcloud-sync@notes.timer
  # ln -s /run/current-system/sw/lib/systemd/user/nextcloud-sync@.timer ~/.config/systemd/user/nextcloud-sync@notes.timer
  # nvim ~/.config/nextcloud-sync/notes.env
  # (systemctl --user enable resolves the symlinks in /run/current-system fully,
  #  which would make it impossible to update the service)
  nextcloud-sync= pkgs.symlinkJoin {
    name = "nextcloud-sync";

    paths = [ service timer ];
  };
in {
  environment.systemPackages = with pkgs; [
    chafa
    broot

    espeak
    eva
    yt-dlp # youtube-dl fork
    ffmpeg_6-full
    mediainfo
    xournalpp
    vym
    vlc
    treesheets
    time
    remmina
    qdirstat
    filelight
    playerctl
    nextcloud-client
    musescore
    ktouch
    ktorrent
    # kmymoney failed to build after update to 21.05
    klavaro
    kcolorchooser

    xplr

    freeplane
    umlet

    hunspell
    hunspellDicts.en_US
    hunspellDicts.de_DE
    aspell
    aspellDicts.en
    aspellDicts.de

    # xmpp client
    dino

    signal-desktop
    zulip

    # matrix client
    fractal

    calibre

    krita
    inkscape
    imagemagick
    # gimp-with-plugins
    (lib.warn "gimp-with-plugins build broken" gimp)
    blender

    tiny

    archivemount

    openssl
    
    ledger

    qmk
    wb32-dfu-updater

    nextcloud-sync

    taskwarrior
    khal
  ] ++ lib.optionals (config.services.xserver.enable) [ 
    scrcpy

    # matrix client; (depends on libolm though)
    # kdePackages.neochat
  ];

  services.udev = let
    customQmkRules = pkgs.writeTextDir "etc/udev/rules.d/51-qmk-extra.rules" ''
      SUBSYSTEM=="usb", ATTRS{idVendor}=="342d", ATTRS{idProduct}=="dfa0", TAG+="uaccess"
    '';
  in {
    packages = [ pkgs.qmk-udev-rules customQmkRules ];
    extraRules = ''
      SUBSYSTEM=="usb", ATTRS{idVendor}=="342d", ATTRS{idProduct}=="dfa0", TAG+="uaccess"
    '';
  };
  # needed for qmk rules
  users.groups.plugdev = {};

  systemd.packages = [
    nextcloud-sync
  ];
}
