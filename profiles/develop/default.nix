{ pkgs, lib, config, inputs, flakeSelf, ... }:
let
  inherit (builtins) readFile;

  advanced-rest-client = import ../../packages/advanced-rest-client.nix { pkgs = pkgs; };

  systemPackages = with pkgs; lib.warn "fixme: beekeeper-studio build failure, see 321571" (lib.warn "fixme: gnuplot{,qt} build failure; see #229154" (lib.warn "fixme: sequeler build failure" (lib.warn "fixme: swiProlog, create-slidev opensll dep broken" [
    man-pages

    wireshark-cli
    ngrep

    httpie

    silver-searcher

    stress
    stress-ng

    # gnuplot

    gdb

    dos2unix

    cdecl

    texlive.combined.scheme-full
    pandoc
    typst
    polylux2pdfpc

    plantuml
    graphviz

    reuse

    mercury

    elixir

    elmPackages.elm
    elmPackages.elm-json
    elmPackages.elm-format
    elmPackages.elm-analyse
    elmPackages.elm-test
    elmPackages.elm-review

    racket

    gnumake

    ruby bundix bundler rake rubocop

    # swiProlog

    ghc

    # create-slidev

    scala
    scala-cli
    scalafmt
    ammonite

    base16-spectrum-generator
    base16-color-palette-creator
  ] ++ lib.optionals (config.services.xserver.enable) [ 
    # gnuplot_qt sequeler
    hexyl ghex wireshark-qt advanced-rest-client

    # currently fails to build
    # beekeeper-studio

    android-studio

  ])));
in {
  environment.systemPackages = systemPackages;
  environment.variables = {
    _JAVA_AWT_WM_NONREPARENTING = "1";
    # typst otherwise finds no fonts :/
    TYPST_FONT_PATHS = "/run/current-system/sw/share/fonts/truetype/";
  };

  programs.adb.enable = true;
  users.users.florian.extraGroups = ["adbusers"];

  documentation.dev.enable = true;
  # only generate cache when it will be kept for more than the current rebuild
  documentation.man.generateCaches = flakeSelf ? "rev";
  # somehow leads to an error :D
  #documentation.nixos.includeAllModules = true;

  programs.wireshark.enable = true;

  networking.hosts = { 
    "127.0.0.1" = [ "localhorst" ];
  };
}
