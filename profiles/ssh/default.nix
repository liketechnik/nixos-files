{ config, pkgs, lib, ... }:
{
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      PermitRootLogin = "no";
      # taken from https://infosec.mozilla.org/guidelines/openssh#modern-openssh-67
    };
    extraConfig = ''
      # Password based logins are disabled - only public key based logins are allowed.
      AuthenticationMethods publickey
    '';
  };
}

