{ lib, pkgs, config, inputs, ... }:
let
  commonHeaders = ''
    # Add HSTS header with preloading to HTTPS requests.
    # Adding this header to HTTP requests is discouraged
    add_header Strict-Transport-Security $hsts_header always;

    # Enable CSP for your services.
    add_header Content-Security-Policy $cspheader always;

    # Minimize information leaked to other domains
    add_header 'Referrer-Policy' 'origin-when-cross-origin';

    # Disable embedding as a frame
    add_header X-Frame-Options DENY;

    # Prevent injection of code in other mime types (XSS Attacks)
    add_header X-Content-Type-Options nosniff always;

    # Enable XSS protection of the browser.
    # May be unnecessary when CSP is configured properly (see above)
    add_header X-XSS-Protection "1; mode=block" always;
  '';
  woodpeckerPort = "8010";
in {
  sops.secrets."woodpecker/server.env" = {
    format = "binary";
    sopsFile = ../../secrets/woodpecker/server.env.bin;
  };
  sops.secrets."woodpecker/agent.env" = {
    format = "binary";
    sopsFile = ../../secrets/woodpecker/agent.env.bin;
  };

  services.woodpecker-server = {
    enable = true;
    environment = let
      # fixme: having this static is kinda hmm
      knownHosts = pkgs.writeText "ssh_known_hosts" ''
        # liketechnik.name:22 SSH-2.0-OpenSSH_9.1
        liketechnik.name ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDGuXKhdSbcCb9PIZ1v7Q2Z1Bpexiim6LWEpMfBaRXP5C4045ovfr4HOjiyKgQL1Lklciry1gC4P+B0a5QFyq1n40YIZJHEcWiWUQjjSTEOY1sBt2rSa6LyhESbpkN2jnhkm5/GVAL6nVUuGqB8NASFsNoDpx+ut/jruLbNp3JVPkOQ83dlqJC2RUHtgJYeR3yVNJumHuLKdfACLZ8BA+Blo++dBUN7YOsek1EeVdhJDxiaSWCEW/7WE9HKAhF3gvAQm4stdWwABzVygG8tzbOUKk5lU1bls0L6sH5CgCTqhZzz7fsPMSzIkcYV4gQyZqUBt1bBmQ+dSo0Ci/FsgxqVDHyZisAnJAyY4f0TzjfkZ+mEvZI0PZ4TZ23WzWeSuKki/lFik072t+s6kSiTRmTkTKcFGEGH1w4j2V33S9Hc7ojmzA95b+U3DxEplJfy500qzv98XpoQOK+4/tvtVBAfgF6VH0IHyRadE8O6NzMG2H60LeSR7buZmWa0ES8uDKtbDTNjxHNGapMYCGlkHvcb+/czsF5GKKifg+t3E0Nk4T5n5H/H947bGR4L0wBPtyzwnVadEbmz/2/VZk1xzs+WKpjgMzVIZGcS7y0P22atwFtsrtoR/PnYOU0blyg6p3He0MmB8k8bhuot6ry+eDOAuE5ij6+hoQjEEJvglsY3ww==
        # liketechnik.name:22 SSH-2.0-OpenSSH_9.1
        # liketechnik.name:22 SSH-2.0-OpenSSH_9.1
        liketechnik.name ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAJDWQYMAKh5V4T62JVW85SRHsPhqN/HqEmV8YYfyY1G
        # liketechnik.name:22 SSH-2.0-OpenSSH_9.1
        # liketechnik.name:22 SSH-2.0-OpenSSH_9.1
      '';
    in {
      WOODPECKER_HOST = "https://woodpecker.liketechnik.name";
      WOODPECKER_SERVER_ADDR = ":${woodpeckerPort}";
      WOODPECKER_ADMIN = "liketechnik";
      WOODPECKER_GITEA = "true";
      WOODPECKER_GITEA_URL = "https://git.disroot.org";
      WOODPECKER_ENVIRONMENT = "SSH_KNOWN_HOSTS_FILE:${knownHosts}";
      WOODPECKER_LOG_LEVEL = "info";
    };
    environmentFile = config.sops.secrets."woodpecker/server.env".path;
  };

  services.woodpecker-agents.agents = {
    docker = {
      enable=true;
      extraGroups = [ "podman" ];
      environment = {
        DOCKER_HOST = "unix:///run/podman/podman.sock";
        WOODPECKER_BACKEND_DOCKER_VOLUMES = "/nix/store:/nix/store:ro,/nix/var/nix/db:/nix/var/nix/db:ro,/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro";
        WOODPECKER_HEALTHCHECK_ADDR = ":9010";
        WOODPECKER_LOG_LEVEL = "info";
      };
      environmentFile = [ config.sops.secrets."woodpecker/agent.env".path ];
    };
  };

  security.acme.certs."liketechnik.name".extraDomainNames = [ "woodpecker.liketechnik.name" ];

  services.nginx.virtualHosts."woodpecker.liketechnik.name" = {
    forceSSL = true;
    useACMEHost = "liketechnik.name";

    locations."/" = {
      proxyPass = "http://127.0.0.1:${woodpeckerPort}/";

      extraConfig = ''
        proxy_buffering off;

        chunked_transfer_encoding off;
      '';
    };

    extraConfig = commonHeaders;
  };
}
