{ pkgs, lib, virtualisation, config, ... }:
{
  virtualisation.lxd.enable = false;

  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemu = {
    swtpm.enable = true;
    ovmf.packages = with pkgs; [ OVMFFull.fd ];
  };
  # services.qemuGuest.enable = true;

  virtualisation.docker.enable = false;
  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
    dockerSocket.enable = true;
  };
  virtualisation.oci-containers.backend = "podman";

  virtualisation.libvirtd.qemu.runAsRoot = true;
  virtualisation.lxc.lxcfs.enable = true;

  networking.firewall.trustedInterfaces = [ "podman0" "podman1" "podman2" "podman3" ];

  environment.systemPackages = with pkgs; [
    virt-manager
    docker-compose
    buildah
    skopeo
    virtiofsd
  ] ++ lib.optionals (config.services.xserver.enable) [ virt-manager-qt ];

  # can be enabled after upgrading state to at least 22.05
  # (see https://github.com/NixOS/nixpkgs/pull/87268/files)
  boot.enableContainers = false;

  boot.binfmt.emulatedSystems = ["aarch64-linux" "armv7l-linux" "i686-linux"];
}
