{ config, lib, pkgs, inputs, overlay-unstable, nixpkgs, flakeSelf, ... }:
let 
  # overlay with custom packages and overrides
  overlay = self: super: let 
    # do not expose unstable as is, but rather add the packages intented for override here
    unstable_raw = import inputs.nixpkgs-unstable { system = super.system; config.allowUnfree = true; };
    ironbar-flake = import inputs.ironbar { system = super.system; };
  in rec {
    # fenix based rustPlatform for latest rust stable
    rustPlatform_fenixStable = pkgs.makeRustPlatform {
      inherit (inputs.fenix.packages.${super.system}.stable) cargo rustc;
    };

    # sql support needed for configuration
    postfix = super.postfix.override {
      withPgSQL = true;
    };

    # sql support needed for configuration
    dovecot = super.dovecot.override {
      withPgSQL = true;
    };

    xsane = super.xsane.override {
      gimpSupport = true;
    };

    alacritty-ligatures = super.callPackage ../../packages/overrides/applications/terminal-emulators/alacritty-ligatures {
      alacritty = super.alacritty;
      inputs = inputs;
      lib = super.lib;
      stdenv = super.stdenv;
    };

    staticman = super.callPackage ../../packages/custom/servers/staticman {
      pkgs = super;
      nodejs = super.nodejs-14_x;
      writeShellScriptBin = super.writeShellScriptBin;
      symlinkJoin = super.symlinkJoin;
    };

    create-slidev = super.callPackage ../../packages/custom/tools/misc/create-slidev {
      pkgs = super;
      nodejs = super.nodejs-14_x;
    };

    # ledger = super.callPackage ../../packages/overrides/applications/office/ledger {
    #   ledger = super.ledger;
    #   lib = super.lib;
    #   fetchFromGitHub = super.fetchFromGitHub;
    # };

    iosevka-custom-mono = super.callPackage ../../packages/overrides/data/fonts/iosevka-custom-mono {
      iosevka = super.iosevka;
    };

    iosevka-custom-term = super.callPackage ../../packages/overrides/data/fonts/iosevka-custom-term {
      iosevka = super.iosevka;
    };

    iosevka-custom-term-serif = super.callPackage ../../packages/overrides/data/fonts/iosevka-custom-term-serif {
      iosevka = super.iosevka;
    };

    vpn-slice = super.callPackage ../../packages/overrides/tools/networking/vpn-slice {
      vpn-slice = super.vpn-slice;
      iptables = super.iptables;
      iproute2 = super.iproute2;
    };

    mercury-vim = super.callPackage ../../packages/custom/tools/misc/mercury-vim {
      mercury = super.mercury;
    };

    iruby = super.callPackage ../../packages/overrides/applications/editors/jupyter-kernels/iruby {
      lib = super.lib;
      defaultGemConfig = super.defaultGemConfig;
      rake = super.rake;
      bundlerApp = super.bundlerApp;
    };

    # bismuth = super.callPackage ../../packages/overrides/desktops/plasma-5/3rdparty/addons/bismuth {
    #   libsForQt5 = super.libsForQt5;
    #   inputs = inputs;
    # };

    wb32-dfu-updater = super.callPackage ../../packages/custom/development/tools/misc/wb32-dfu-updater {
      lib = super.lib;
      mkDerivation = super.stdenv.mkDerivation;
      fetchFromGitHub = super.fetchFromGitHub;
      cmake = super.cmake;
      libusb1 = super.libusb1;
    };

    # qmk = super.callPackage ../../packages/overrides/tools/misc/qmk {
    #   callPackage = super.callPackage;
    #   inputs = inputs;
    # };

    android-studio = super.callPackage ../../packages/overrides/editors/android-studio {
      callPackage = super.callPackage;
      inputs = inputs;
      makeFontsConf = super.makeFontsConf;
      bash = super.bash;
      writeScript = super.writeScript;
    };

#     prismlauncher = super.qt6Packages.callPackage ../../packages/overrides/games/prismlauncher {
#       callPackage = super.qt6Packages.callPackage;
#       inputs = inputs;
#     };

    # helvum = super.callPackage ../../packages/overrides/applications/audio/helvum {
    #   rustPlatform = rustPlatform_fenixStable;
    #   callPackage = super.callPackage;
    #   inputs = inputs;
    # };

    ironbar = inputs.ironbar.packages.${super.system}.ironbar;

    # fix for watershot
    watershot = (inputs.watershot.packages.${super.system}.default.overrideAttrs (oldAttrs: {
      buildInputs = oldAttrs.buildInputs ++ [
        pkgs.grim
      ];
    }));

    polylux2pdfpc = super.callPackage ../../packages/custom/po/polylux2pdfpc/package.nix {
    };

    base16-spectrum-generator = super.callPackage ../../packages/custom/ba/base16-spectrum-generator/package.nix {
    };

    base16-color-palette-creator = super.callPackage ../../packages/custom/ba/base16-color-palette-creator/package.nix {
    };

  };
in {
  nixpkgs.overlays = [
    overlay

    # inputs.anyrun.overlays.default
    # causes too many issues
    # inputs.neovim-nightly-overlay.overlay

  ];
}
