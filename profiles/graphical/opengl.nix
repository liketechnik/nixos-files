{ pkgs, ... }:
{
  hardware.graphics.enable = true;
  hardware.graphics.enable32Bit = true;

  environment.sessionVariables = {
    MOZ_WEBRENDER = "1";
    MOZ_X11_EGL = "1";
  };

  environment.systemPackages = with pkgs; [
    libdrm
    mesa
    mesa.drivers
  ];
}
