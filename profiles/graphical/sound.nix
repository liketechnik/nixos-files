{ pkgs, inputs, lib, ... }:
{
  environment.etc = {
    "openal/alosoft.conf".text = ''
      [general]
      frequency = 48000
      hrtf = false
      stereo-mode = headphones
      [pulse]
      allow-moves=true
    '';
  };

  hardware.bluetooth.settings = {
    General = {
      Enable = "Source,Sink,Media,Socket";
    };
  };

  environment.systemPackages = with pkgs; [
    pavucontrol
    gst_all_1.gstreamer

    helvum

    # lmms TODO reactivate if build unbroken
    carla

    gnome-sound-recorder

    # xsynth_dssi fails to build
    ladspaPlugins
    AMB-plugins
    nova-filters
    caps
    csa
    autotalent
    zam-plugins
    tap-plugins
    lsp-plugins
    swh_lv2
    mda_lv2
    # ams-lv2 (depends on lvtk, which fails to build)
    rkrlv2
    magnetophonDSP.pluginUtils
    distrho-ports
    bshapr
    bchoppr
    magnetophonDSP.CharacterCompressor
    # tunefish fails to build
    plujain-ramp
    magnetophonDSP.RhythmDelay
    mod-distortion
    x42-plugins
    # infamousPlugins build failure
    mooSpace
    noise-repellent
    eq10q
    magnetophonDSP.LazyLimiter
    talentedhack
    artyFX
    speech-denoiser
    fverb
    fomp
    molot-lite
    # surge build fails currently
    vocproc
    #oxefmsynth
    #magnetophonDSP.ConstantDetuneChorus build fails currently
    magnetophonDSP.faustCompressors
    magnetophonDSP.VoiceOfFaust
  ];

  environment.variables = {
    DSSI_PATH   = "$HOME/.dssi:$HOME/.nix-profile/lib/dssi:/run/current-system/sw/lib/dssi";
    LADSPA_PATH = "$HOME/.ladspa:$HOME/.nix-profile/lib/ladspa:/run/current-system/sw/lib/ladspa";
    LV2_PATH    = "$HOME/.lv2:$HOME/.nix-profile/lib/lv2:/run/current-system/sw/lib/lv2";
    LXVST_PATH  = "$HOME/.lxvst:$HOME/.nix-profile/lib/lxvst:/run/current-system/sw/lib/lxvst";
    VST_PATH    = "$HOME/.vst:$HOME/.nix-profile/lib/vst:/run/current-system/sw/lib/vst";
    VST3_PATH   = "$HOME/.vst3:$HOME/.nix-profile/lib/vst3:/run/current-system/sw/lib/vst3";
  };

  hardware.pulseaudio.enable = false;
  hardware.pulseaudio.support32Bit = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
  hardware.pulseaudio.extraModules = [ pkgs.pulseaudio-modules-bt ];
  hardware.pulseaudio.extraConfig = ''
    load-module module-switch-on-connect
  '';

  services.pipewire = {
    enable = true;
    audio.enable = true;
    package = pkgs.pipewire;
    pulse.enable = true;
    jack.enable = true;
    alsa.enable = false;
    alsa.support32Bit = false;
    wireplumber.enable = true;
  };

  services.pipewire.extraConfig.pipewire = {
    "clock" = {
      "context.properties" = {
        "default.clock.rate" = 48000;
        "default.clock.allowed-rates" = [ 24000 41000 48000 96000 ];
        "clock.power-of-two-quantum" = true;
        "default.clock.min-quantum" = 32;
        "default.clock.max-quantum" = 8192;
        "default.clock.quantum" = 2048;
        "default.clock.quantum-limit" = 2048;
        "vm.overrides" = {
          "default.clock.min-quantum" = 2048;
          "default.clock.rate" = 24000;
          "default.clock.allowed-rates" = [ 24000 ];
        };
      };
    };
  };

  services.pipewire.extraConfig.client = {
    "resample" = {
      "stream.properties" = {
        "resample.quality" = 10;
      };
    };
  };

  services.pipewire.extraConfig.client-rt = {
    "resample" = {
      "stream.properties" = {
        "resample.quality" = 10;
      };
    };
  };

  services.pipewire.extraConfig.pipewire-pulse = {
    "resample" = {
      "stream.properties" = {
        "resample.quality" = 10;
      };
    };
  };

  services.pipewire.wireplumber.configPackages = [
    (pkgs.writeTextDir "share/wireplumber/wireplumber.conf.d/51-bluez.conf" ''
      bluez_monitor.properties = {
        ["bluez5.default.rate"] = 48000
      }

      bluez_monitor.rules = {
        matches = {
          {
            { "device.name", "matches", "bluez.card.*" },
          },
        },
        apply_properties = {
          ["bluez5.a2dp.ldac.quality"] = "hq"
        }
      }
    '')
  ];

}
