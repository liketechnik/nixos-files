{ pkgs, inputs, lib, config, ... }:
let
  inherit (builtins) readFile;
  river-configured = import ../../packages/river-configured.nix { pkgs = pkgs; };
  notifymuch = import ../../packages/notifymuch.nix { pkgs = pkgs; lib = lib; };
  grimflow = import ../../packages/grimflow.nix { pkgs = pkgs; lib = lib; };

  fontPkgs = with pkgs; [
    # default fonts
    stix-two # serif
    recursive # mono/ui text

    xkcd-font
    comic-neue

    # icon fonts
    powerline-fonts
    nerd-fonts.iosevka
    nerd-fonts.victor-mono
    nerd-fonts.symbols-only
    font-awesome
    material-design-icons
    # emojis
    noto-fonts-emoji

    # fonts I used previously
    iosevka-bin
    victor-mono
  ];
in {
  imports = [ ./opengl.nix ./sound.nix ];

  # missing: automate
  # echo 'HDMI-A-1:' | sudo tee /sys/module/drm/parameters/edid_firmware (other screen connected)
  # echo 'HDMI-A-1:edid/P2423.bin' | sudo tee /sys/module/drm/parameters/edid_firmware (P2423 connected)
  # (replug doesn't work, so screen must be manually re-plugged)
  # maybe: boot with edid override enabled and provide shortcut for disabling
  # before connecting to other screens?
  hardware.display.edid.linuxhw = {
    "P2423" = [ "P2423" "1920x1200" ];
  };

  services.greetd = {
    vt = 7;
    enable = true;
    settings = {
      default_session = {
        command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd ${river-configured}/bin/river";
        user = "greeter";
      };
    };
  };

  services.displayManager = {
    sddm = {
      enable = false;
    };

    sessionPackages = [
      river-configured
    ];

    defaultSession = "river";
  };
  services.desktopManager.plasma6 = {
    enable = true;
  };

  services.xserver = {
    enable = true;

    displayManager.gdm = {
      enable = false;
    };

    displayManager.lightdm = {
      enable = false;
    };


    xkb = {
      layout = "us";
      variant = "altgr-intl";
      options = "caps:swapescape";
    };
  };

  services.libinput = {
    enable = true;
  };

  programs.xwayland.enable = true;

  programs.dconf.enable = true;

  # needed to make swaylock work
  security.pam.services.swaylock = {};

  security.pam.services.florian.enableKwallet = true; # auto unlock kwallet with river

  environment.systemPackages = with pkgs; [
    # river stuff
    river-configured

    kanshi # automatic output configuration

    waybar
    # ironbar
    # libappindicator

    # anyrun
    fuzzel # app launcher; dmenu alternative

    swayidle # idle daemon
    swaybg # background :)

    grim # screenshots
    slurp # screen selection for screenshots

    grimflow # screenshots

    # watershot

    swappy # screenshot editing tool

    # changes from #148344
    # for making screen sharing under wayland work :)
    # should be removed when that content has landed in 21.11
    xdg-desktop-portal
    xdg-desktop-portal-wlr
    xdg-desktop-portal-gtk
    xdg-desktop-portal-kde

    # screen sharing for x11 apps (discord, etc)
    xwaylandvideobridge

    # river stuff end

    wlsunset # redshift for wayland

    # copy/paste tools for wayland :)
    wl-clipboard
    # xdotools for wayland
    wtype

    tre # agrep alternative - fuzzy matching
    ponymix # pulseaudio cli tool
    playerctl # media control via cli
    wlr-randr # screen control via cli

    swaylock # screen lock

    dunst # notifications

    notify-desktop

    light # screen brightness

    # be able to enter passwords for anything using polkit
    # (e.g. udisks, tuxedo-control-center, etc)
    kdePackages.polkit-kde-agent-1
    # required dependencies for polkit-kde-agent-1
    # (no, I don't wanna know why I have to add these manually)
    kdePackages.kirigami
    kdePackages.kirigami-addons
    kdePackages.sonnet

    # mail
    notmuch
    notifymuch
    # astroid fails to build
    msmtp
    isync
    aerc

    # password management (kde)
    kdePackages.kwallet
    kwalletcli
    kdePackages.kwalletmanager

    # tiling plugin for kde
    # libsForQt5.bismuth

    # kde programs
    gsettings-qt
    kde-gtk-config
    kdePackages.breeze
    breeze-gtk
    breeze-icons
    kwin-tiling
    plasma-pa
    libsForQt5.qt5.qtgraphicaleffects
    spectacle
    plasma-systemmonitor
    kdePackages.plasma-disks
    krunner-pass
    kmag
    kcharselect
    kcalc
    kdePackages.dolphin-plugins
    ark
    kate
    kdeplasma-addons
    kdialog

    # graphical monitor config
    arandr

    (firefox-wayland.override {
      nativeMessagingHosts = [ passff-host plasma-browser-integration ]; }
    )
    #ungoogled-chromium doesn't build currently

    # mail, addressbook & calendar (kde)
    akregator
    korganizer
    kaddressbook
    kontact
    kmail
    kdePackages.kmail-account-wizard
    kdePackages.akonadi-import-wizard
    kdePackages.accounts-qt
    akonadi
    kdePackages.akonadiconsole
    kdePackages.akonadi-calendar
    kdePackages.akonadi-contacts
    kdePackages.akonadi-mime
    libsForQt5.akonadi-notes
    kdePackages.akonadi-search
    libsForQt5.grantlee
    kdePackages.grantlee-editor
    kdePackages.grantleetheme

    plasma5Packages.kdeconnect-kde

    # graphical gpg management
    seahorse
    kleopatra
    kgpg

    # document & image viewers
    haruna # mpv based video player
    okular
    gwenview
    nomacs
    feh
    zathura
    pdfpc

    # document & image viewers in the terminal
    timg # images, pdf
    epr # eupb (ebooks)

    gparted
    gnome-disk-utility

    gitg
    git-cola

    # music player, cd manager
    strawberry
    elisa
    # transcode fails to build
    # (cf. https://github.com/NixOS/nixpkgs/issues/369573
    # https://github.com/NixOS/nixpkgs/pull/358364 )
    (kdePackages.k3b.override { transcode = null; })

    # audio visualizers (command: vis)
    cli-visualizer


    (libreoffice-qt.overrideAttrs (oldAttrs: rec {
      kdeIntegration = true;
    }))

    obs-studio
    kdePackages.kdenlive
    ardour
    tenacity
    # qlcplus build failure 2024-02-08, not used anymore

    #jami-daemon
    #jami-client-qt

    organicmaps

    # inputs.webcord.packages.${pkgs.system}.default out of date (node eol, does not work)

    # scanning
    kdePackages.skanlite

    # dicom (medical image format) viewer
    weasis
  ] ++ fontPkgs;

  # changes from #148344
  # for making screen sharing under wayland work :)
  # should be removed when that content has landed in 21.11
  environment.pathsToLink = [ "/share/applications" ];

  # possibly working around #41426 (kontact not working)
  environment.sessionVariables = {
    NIX_PROFILES = "${lib.concatStringsSep " " (lib.reverseList config.environment.profiles)}";
  };

  services.flatpak.enable = true;
  xdg = {
    icons.enable = true;
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        # the order seems to matter here?
        xdg-desktop-portal-wlr
        xdg-desktop-portal-kde
        xdg-desktop-portal-gtk
      ];
      # well, why did I use this?
      # it may very well have been needed,
      # but should only be used on a per-application case-by-case basis...
      # see https://github.com/NixOS/nixpkgs/commit/ebde08adf37932ff59c27b5935840aa733965bdb
      # for more info 
      # gtkUsePortal = true;
    };
  };
     
  fonts = {
    enableDefaultPackages = true;

    packages = fontPkgs;

    fontconfig = {
      localConf = ''
        <!-- use a less horrible font substition for pdfs such as https://www.bkent.net/Doc/mdarchiv.pdf -->
        <match target="pattern">
          <test qual="any" name="family"><string>Verdana</string></test>
          <edit name="family" mode="assign" binding="same"><string>DejaVu Sans</string></edit>
        </match>
      '';
      defaultFonts = {
        # mono from nerd font refers only to icons (which get much smaller in the mono version)
        serif = [ "STIX Two Text" ];
        sansSerif = [ "Recursive Lnr St" ];
        monospace = [ "Recursvie Mn Lnr St" ];
      };
    };
  };

  qt.style = "breeze";

  services.geoclue2 = {
    enable = true;
  };
  location.provider = "geoclue2";

  gtk.iconCache.enable = true; 

  programs.gnome-disks.enable = true;
  programs.wireshark.enable = true;
  programs.system-config-printer.enable = true;

  services.gnome.gnome-settings-daemon.enable = true;

  services.gvfs.enable = true;

  security.polkit.enable = true;
  services.udisks2.enable = true;

  # allow kdeconnect to work (see https://userbase.kde.org/KDEConnect#I_have_two_devices_running_KDE_Connect_on_the_same_network.2C_but_they_can.27t_see_each_other)
  networking.firewall.allowedTCPPortRanges = [ { from = 1714; to = 1764; } ];
  networking.firewall.allowedUDPPortRanges = [ { from = 1714; to = 1764; } ];

  programs.gnupg.agent.pinentryPackage = pkgs.pinentry-qt;

  # gnome keyring replacement based on pass :)
  services.passSecretService = {
    enable = true;
    package = pkgs.pass-secret-service.overrideAttrs(super: {
      # I don't want this thing to fiddle with MY passwords though
      postPatch = super.postPatch + ''
        substituteInPlace systemd/dbus-org.freedesktop.secrets.service \
          --replace "pass_secret_service" 'pass_secret_service --path ''${HOME}/.local/share/pass-secret-service'
      '';
    });
  };
}
