{ config, lib, pkgs, ... }:
let
  commonHeaders = ''
    # Add HSTS header with preloading to HTTPS requests.
    # Adding this header to HTTP requests is discouraged
    add_header Strict-Transport-Security $hsts_header always;

    # Enable CSP for your services.
    add_header Content-Security-Policy $cspheader always;

    # Minimize information leaked to other domains
    add_header 'Referrer-Policy' 'origin-when-cross-origin';

    # Disable embedding as a frame
    add_header X-Frame-Options DENY;

    # Prevent injection of code in other mime types (XSS Attacks)
    add_header X-Content-Type-Options nosniff always;

    # Enable XSS protection of the browser.
    # May be unnecessary when CSP is configured properly (see above)
    add_header X-XSS-Protection "1; mode=block" always;
  '';
in {
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "liketechnik@disroot.org";

  # do not create a separate cert for each subdomain:
  # instead, generate one for the main one
  # use that with `useACMEHost` in the subdomain config
  # register the subdomain for the cert via 
  # `security.acme.certs."<main domain>".extraDomainNames = [ "<subdomain 1>" "<subdomain 2>" ]
  security.acme.certs."liketechnik.name".extraDomainNames = [ "staticman.liketechnik.name" "jupyterhub.liketechnik.name" ];

  services.nginx = {
    enable = true;

    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    commonHttpConfig = ''
      map $scheme $hsts_header {
        https   "max-age=31536000; includeSubdomains; preload";
      }
      map $http_host $cspheader {
        hostnames;

        jupyterhub.liketechnik.name "script-src 'self' 'unsafe-eval' 'unsafe-inline'; object-src 'none'; base-uri 'none';";
        ki-lecture-builds.liketechnik.name "script-src 'self' 'unsafe-inline' https://polyfill.io https://cdn.jsdelivr.net; object-src 'none'; base-uri 'none';";
        woodpecker.liketechnik.name "script-src 'self' 'unsafe-inline'; object-src 'none'; base-uri 'none';";

        .rdfcsa-js.liketechnik.name "default-src 'self'; script-src 'self' 'unsafe-inline' blob:; style-src 'self' 'unsafe-inline' blob:; img-src data:; object-src 'none'; base-uri 'none';";

        .liketechnik.name "script-src 'self'; object-src 'none'; base-uri 'none';";

        default "default-src 'self'; object-src 'none'; base-uri 'none';";
      }

      # This might create errors
      proxy_cookie_path / "/; secure; HttpOnly; SameSite=strict";
    '';

    appendHttpConfig = ''
      server {
        listen 8080;
        listen [::]:8080;

        server_name localhost;

        location /nginx_status {
          stub_status on;
          access_log off;
          allow 127.0.0.1;
          allow ::1;
          deny all;
        }
      }
    '';

    virtualHosts = {
      "liketechnik.name" = {
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          root = "/var/www";
        };

        extraConfig = commonHeaders;
      };

      "staticman.liketechnik.name" = {
        forceSSL = true;
        useACMEHost = "liketechnik.name";
        
        locations."/" = {
          proxyPass = "http://127.0.0.1:4000/";
        };

        extraConfig = commonHeaders;
      };

      "jupyterhub.liketechnik.name" = {
        forceSSL = true;
        useACMEHost = "liketechnik.name";

        locations."/" = {
          proxyPass = "http://127.0.0.1:8000/";
          proxyWebsockets = true;

          extraConfig = ''
            proxy_set_header X-Scheme $scheme;

            proxy_buffering off;
          '';
        };

        extraConfig = commonHeaders;
      };
    };
  };
  services.prometheus.exporters.nginx = {
    enable = config.services.prometheus.enable;
    scrapeUri = "http://localhost:8080/nginx_status";
  };
}
