{ pkgs, lib, config, ... }:
let
  service-defaults  = {
    tagList = [ "general-purpose" "docker" "podman" "linux" config.networking.hostName ];
    requestConcurrency = 1;
    runUntagged = true;
    limit = 1;
    dockerImage = "alpine";
    registrationFlags = [
      "--docker-memory 2.5g"
      "--docker-memory-swap 3g"
      "--docker-memory-reservation 2.0g"
      "--docker-cpus 2"
    ];
  };
  privileged-defaults = {
    tagList = [ "privileged" ];
    protected = true;
    dockerPrivileged = true;
    dockerVolumes = [ "/certs" ];
  };
  nix-defaults = (with lib; {
    registrationFlags = [
      "--cache-dir /cache"
    ];
    dockerImage = "alpine";
    dockerVolumes = [
      "/bigdata-root/gitlab-runner-cache:/cache"
      "/nix/store:/nix/store:ro"
      "/nix/var/nix/db:/nix/var/nix/db:ro"
      "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
      "/etc/static:/etc/static:ro"
      "/etc/nix:/etc/nix:ro"
      "/run/wrappers:/run/wrappers:ro"
      "/run/current-system:/run/current-system:ro"
    ];
    dockerDisableCache = true;
    preBuildScript = pkgs.writeScript "setup-container" ''
      mkdir -p -m 0755 /nix/var/log/nix/drvs
      mkdir -p -m 0755 /nix/var/nix/gcroots
      mkdir -p -m 0755 /nix/var/nix/profiles
      mkdir -p -m 0755 /nix/var/nix/temproots
      mkdir -p -m 0755 /nix/var/nix/userpool
      mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
      mkdir -p -m 1777 /nix/var/nix/profiles/per-user
      mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
      mkdir -p -m 0700 "$HOME/.nix-defexpr"

      # breaks sqlite-jdbc libc detection for android builds
      # (https://github.com/xerial/sqlite-jdbc/issues/623)
      rm /etc/os-release
    '';
    environmentVariables = {
      ENV = "/etc/profile";
      USER = "root";
      NIX_REMOTE = "daemon";
      PATH = "/run/wrappers/bin:/run/current-system/sw/bin:/bin:/sbin:/usr/bin:/usr/sbin";
    };
    tagList = [ "nix" ];
  });
in {
  # requires ../virt profile to be activated

  # adapted from https://nixos.wiki/wiki/Gitlab_runner

  boot.kernel.sysctl."net.ipv4.ip_forward" = true; # enabling ip_forward on the host machine is important for the docker container to be able to perform networking tasks (such as cloning the gitlab repo)

  services.gitlab-runner = {
    enable = true;
    settings.check_interval = 10;
    services = {
      general-purpose = lib.mkMerge [ 
        service-defaults
        (with lib; {
          registrationConfigFile = config.sops.secrets."gitlab-runner/general-purpose/registrationConfigFile".path;
        }) 
      ];
      general-privileged = lib.mkMerge [
        service-defaults 
        privileged-defaults 
        (with lib; {
          registrationConfigFile = config.sops.secrets."gitlab-runner/general-privileged/registrationConfigFile".path;
        })
      ];
      docker-cargo-tools = lib.mkMerge [
        service-defaults
        (with lib; {
          registrationConfigFile = config.sops.secrets."gitlab-runner/docker-cargo-tools/registrationConfigFile".path;
        })
      ];
      convertible-structured-text = lib.mkMerge [
        service-defaults
        privileged-defaults
        (with lib; {
          registrationConfigFile = config.sops.secrets."gitlab-runner/convertible-structured-text/registrationConfigFile".path;
        })
      ];
      # TODO: update to new runnr registration method:
      # https://docs.gitlab.com/ee/ci/runners/new_creation_workflow
      # nix-reformat = lib.mkMerge [
      #   service-defaults
      #   nix-defaults
      #   (with lib; {
      #     registrationConfigFile = config.sops.secrets."gitlab-runner/nix-reformat/registrationConfigFile".path;
      #   })
      # ];
    };
  };

  systemd.services.gitlab-runner.serviceConfig.SupplementaryGroups = [ "podman" ]; # docker does not work
}
