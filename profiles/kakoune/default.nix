{ pkgs, inputs, ... }:
  let
    config-file = pkgs.writeText "kakrc.local" ''
      set-option global tabstop 4
      set-option global indentwidth 4

      evaluate-commands %sh{
        bg='rgba:13151326'
        fg='rgb:8ca68c'
        c0='rgb:131513'
        c1='rgb:e6193c'
        c2='rgb:29a329'
        c3='rgb:c3c322'
        c4='rgb:3d62f5'
        c5='rgb:ad2bee'
        c6='rgb:1999b3'
        c7='rgb:8ca68c'
        c9='rgb:e6193c'
        c10='rgb:29a329'
        c11='rgb:c3c322'
        c12='rgb:3d62f5'
        c13='rgb:ad2bee'
        c14='rgb:1999b3'
        c15='rgb:f0fff0'

        echo "
        	face global value $c1
        	face global type $c2
        	face global variable $c3
        	face global module $c3
        	face global function $c6
        	face global string $c5
        	face global keyword $c4
        	face global operator $c2
        	face global attribute $c3
			face global comment $c6
			face global documentation comment
			face global meta $c5
			face global builtin $fg+b

			face global title $c4
			face global header $c6
			face global mono $c3
			face global block $c5
			face global link $c6
			face global bullet $c6
			face global list $c2

			face global Default $fg,$bg
			face global PrimarySelection $fg,$c4+fg
			face global SecondarySelection $bg,$c4+fg
			face global PrimaryCursor $bg,$fg+fg
			face global SecondaryCursor $bg,$fg+fg
			face global PrimaryCursorEol $bg,$c6+fg
			face global SecondaryCursorEol $bg,$c6+fg
			face global LineNumbers $fg,$bg
			face global LineNumbersCursor $fg,$bg+r
			face global MenuForground $fg,$c4
			face global MenuBackground $c4,$fg
			face global MenuInfo $c6
			face global Information $bg,$c3
			face global Error $bg,$c1
			face global DiagnosticError $c1
			face global DiagnosticWarning $c3
			face global StatusLine $c6,$bg
			face global StatusLineMode $c2,$bg
			face global StatusLineInfo $c4,$bg
			face global StatusLineValue $c3,$bg
			face global StatusCursor $bg,$c6
			face global Prompt $c2,$bg
			face global MatchingChar $fg,$bg+b
			face global Whitespace $fg,$bg+fd
			face global BufferPadding $c4,$bg
        "
      }
    '';
    kakoune-with-plugins = pkgs.unstable.kakoune.override {
      kakoune = pkgs.unstable.kakoune-unwrapped;
      plugins = [
      ];
    };
    custom-kakoune = kakoune-with-plugins.overrideAttrs (oldAttrs: rec {
      buildCommand = ''
        # original build command
        ${oldAttrs.buildCommand}

        # custom: link local configuration file into the wrapper
        ln -s "${config-file}" "$out/share/kak/kakrc.local"
      '';
    });
  in {
  environment.systemPackages = [
    custom-kakoune
  ];
}
