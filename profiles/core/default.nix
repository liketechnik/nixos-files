{ config, lib, pkgs, inputs, overlay-unstable, nixpkgs, flakeSelf, ... }:
let 
  inherit (lib) fileContents;
  
in
{
  boot.supportedFilesystems = [ "ntfs" "fat32" "iso9660" "btrfs" "lvm" ];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # services.earlyoom.enable = true;

  systemd.extraConfig = ''
    LogTarget=journal-or-kmsg
  '';
  boot.kernelParams = [
    "console=tty1"
    # disable uas for external seagate expansion hdd
    # (https://www.smartmontools.org/wiki/SAT-with-UAS-Linux)
    # to check/temporarily enable/disable:
    # `cat /sys/module/usb_storage/parameters/quirks `
    # `echo "0x0bc2:0x231a:u" > /sys/module/usb_storage/parameters/quirks `
    # `echo "" > /sys/module/usb_storage/parameters/quirks `
    # (Note: overrides all previously existing quirks!!!!)
    "usb_storage.quirks=0bc2:2038:u"
  ];
  services.journald.console = "/dev/tty1";

  programs.bash = {
    shellAliases = {
      nvis = "nvim -S Session.vim";
      neorg = "nvim +NeorgStart";
      ls = "ls --color=auto";
      disable_webcam = "sudo modprobe -r uvcvideo";
      enable_webcam = "sudo modprobe uvcvideo";
      pbat = "sudo tlp bat";
      pac = "sudo tlp ac";
      eza = "eza --long --header --git --group --tree --level=2";
      ssh = "TERM=xterm ssh";
      "ßh" = "ssh";
      fhlatex = "pdflatex -synctex=1 -interaction=nonstopmode";
      docker-remove-untagged-images = ''sudo docker rmi $(sudo docker images | grep "^<none>" | awk '{print $3}')'';
      docker-remove-stopped-containers = ''sudo docker rm $(sudo docker ps -a -q)'';
      xcd = ''cd "$(xplr --print-pwd-as-result)"'';
    };
    shellInit = ''
      export HISTCONTROL=erasedups

      # function ffmpeg-av1() {
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }

      #   ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $1 -c:v libaom-av1 -crf 20 -b:v 0 -row-mt 1 -c:a libspeex -row-mt 1 $2
      # }
      # function ffmpeg-mkv() {
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }

      #   ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $1 -c:v libvpx-vp9 -crf 10 -b:v 0 -row-mt 1 -c:a libopus -application voip -compression_level 10 $2
      # }
      # function ffmpeg-ogv() {
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }

      #   ffmpeg -hwaccel auto -hwaccel_device /dev/dri/renderD128 -i $1 -c:v theora -qscale:v 6 -c:a libspeex -application voip -compression_level 0 $2
      # }
      # function ffmpeg-h264() {
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }
      #   [[ $1 ]]    || { echo "Missing operand" >&2; return 1; }

      #   ffmpeg -hwaccel vaapi -hwaccel_device /dev/dri/renderD128 -hwaccel_output_format vaapi -i $1 -c:v h264_vaapi -c:a aac $2
      # }
      # function gpg-sign-fh() {
      #   [[ $1 ]]    || { echo "Missing file" >&2; return 1; }

      #   gpg2 --local-user "Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de" --armor --output $1.sig --detach-sign $1
      # }
      # function pandoc-md() {
      #   [[ $1 ]]    || { echo "Missing file name to convert" >&2; return 1; }

      #   pandoc $1.md -o $1.pdf --from markdown --template eisvogel.latex --highlight-style pygments
      # }
      # function nix-dev() {
      #   profile_name="$(echo "$(pwd)" | sed -e 's|/|-|g' | tail -c +2)"
      #   if [ ! -d "/home/florian/.nix-dev-profiles/$profile_name" ]; then
      #     mkdir -vp "/home/florian/.nix-dev-profiles/$profile_name"
      #   fi;
      #   nix develop --profile "/home/florian/.nix-dev-profiles/$profile_name/$profile_name"
      # }
      export OPENER=riverctl-spawn-xdg-open

      # function o() {
      #   riverctl-spawn-xdg-open "$(pwd)/$1"
      # }

      #eval "$(${pkgs.starship}/bin/starship init bash)"
    '';
  };

  programs.less.enable = true;
  programs.less.envVariables = {
    "LESS" = "-R --tabs=4";
  };
  programs.thefuck.enable = true;

  environment = {

    systemPackages = with pkgs; lib.warn "zenith build broken;" [
      binutils
      coreutils
      curl
      direnv
      dnsutils
      dosfstools
      fd
      git
      git-crypt
      gptfdisk
      iputils
      jq
      moreutils
      nix-index
      nmap
      ripgrep
      ripgrep-all
      skim
      tealdeer
      usbutils
      utillinux
      whois
      htop
      bottom
      # zenith
      (pass.withExtensions (ext: with ext; [ pass-otp ]))
      wget
      rsync
      rclone
      neofetch
      lsof
      iotop
      glances
      dvtm
      du-dust
      borgbackup
      bup
      rustic-rs
      bandwhich
      zip unzip
      p7zip
      libcdio
      file
      bat
      eza
      zoxide

      sops

      # build failure
      # onionshare

      tomb
      pinentry-curses

      (import ../../packages/backup-script.nix { pkgs = pkgs; lib = lib; })

      nushell
    ];
  };

  # disable webcam driver by default
  boot.blacklistedKernelModules = [ "uvcvideo" ];

  boot.tmp.useTmpfs = true;

  security.sudo.enable = true;
  security.rtkit.enable = true;

  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;
  hardware.mcelog.enable = true;

  services.accounts-daemon.enable = true;

  services.udev.extraRules = ''
    # set scheduler for NVMe
    ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/scheduler}="kyber"
    # set scheduler for SSD and eMMC
    ACTION=="add|change", KERNEL=="sd[a-z]|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="kyber"
    # set scheduler for rotating disks
    ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
  '';

  services.timesyncd.enable = false;
    services.chrony = {
    enable = true;
    extraConfig = ''
      # Allow the system clock to be stepped in the first three updates
      # if its offset is larger than 1 second
      makestep 1.0 3

      bindcmdaddress 127.0.0.0
      bindcmdaddress ::1
      cmdport 0
    '';
    initstepslew = { enabled = false; };
  };

  users.mutableUsers = true;

  nix = {
    # pin nixpkgs flake
    registry.nixpkgs = {
      exact = true;
      from = { id = "nixpkgs"; type = "indirect"; };
      flake = inputs.nixpkgs;
    };
    # allow to use nixpkgs-unstable with nix shell & co.
    registry.nixpkgs-unstable = {
      exact = true;
      from = { id = "nixpkgs-unstable"; type = "indirect"; };
      flake = inputs.nixpkgs-unstable;
    };
    # make nix-shell & co. use the same nixpkgs as everything else
    nixPath = [ "nixpkgs=flake:nixpkgs" ];
    # allow to use flakes in the built system
    # superseeded by lix: package = pkgs.nixFlakes;
    settings.trusted-binary-caches = [ "https://nix-community.cachix.org" "https://webcord.cachix.org" "https://jakestanger.cachix.org" ];
    settings.binary-cache-public-keys = [ "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=" "webcord.cachix.org-1:l555jqOZGHd2C9+vS8ccdh8FhqnGe8L78QrHNn+EFEs=" "jakestanger.cachix.org-1:VWJE7AWNe5/KOEvCQRxoE8UsI2Xs2nHULJ7TEjYm7mM=" ];
    settings.system-features = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];

    settings.auto-optimise-store = true;

    gc.automatic = true;
    gc.dates = "weekly";
    gc.options = "--delete-older-than 14d";

    optimise.automatic = true;

    settings.sandbox = true;

    settings.allowed-users = [ "@wheel" ];

    settings.trusted-users = [ "root" "@wheel" ];

    extraOptions = ''
      min-free = 536870912
      keep-outputs = true
      keep-derivations = true
      fallback = true

      trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= 
      experimental-features = nix-command flakes

      # stalled-download-timeout = 10
      keep-going = true
    '';

    daemonCPUSchedPolicy = "idle"; # todo only for desktop systems
    daemonIOSchedClass = "idle"; # same
  };

  nixpkgs.config.allowUnfree = true;

  home-manager.useUserPackages = true;
  home-manager.useGlobalPkgs = true;

  system.stateVersion = "20.09"; # let's hope that works :D

  system.configurationRevision = if flakeSelf ? "rev" then flakeSelf.rev else if flakeSelf ? "dirtyRev" then flakeSelf.dirtyRev else lib.warn "no git rev available" null;

  system.activationScripts.diff = {
    supportsDryActivation = true;
    text = ''
      ${pkgs.nvd}/bin/nvd --nix-bin-dir=${pkgs.nix}/bin diff /run/current-system "$systemConfig"
    '';
  };
}
