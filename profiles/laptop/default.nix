{ pkgs, config, ... }:
{
  # power-profiles-daemon is enabled by default,
  # and probably more appropriate
  # services.tlp = {
  #   enable = true;
  #   settings = {
  #     TLP_ENABLE = 1;
  #     TLP_DEFAULT_MODE = "BAT";
  #     TLP_PERSISTEN_DEFAULT = 1;
  #     CPU_SCALING_GOVERNOR_ON_AC = "performance";
  #     CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
  #     CPU_ENERGY_PERF_POLICY_ON_AC = "balance_performance";
  #     CPU_ENERGY_PERF_POLICY_ON_BAT = "power";
  #     CPU_MIN_PERF_ON_AC = 0;
  #     CPU_MAX_PERF_ON_AC = 100;
  #     CPU_MIN_PERF_ON_BAT = 0;
  #     CPU_MAX_PERF_ON_BAT = 30;
  #     CPU_BOOST_ON_AC = 1;
  #     CPU_BOOST_ON_BAT = 0;
  #     SCHEDULER_POWERSAVE_ON_AC = 0;
  #     SCHEDULER_POWERSAVE_ON_BAT = 1;
  #     WIFI_PWR_ON_AC = "off";
  #     WIFI_PWR_ON_BAT = "off";
  #     SOUND_POWER_SAVE_ON_AC = 1;
  #     SOUND_POWER_SAVE_ON_BAT = 1;
  #     USB_AUTOSUSPEND = 0;
  #     RESTORE_DEVICE_STATE_ON_STARTUP = 0;
  #     DEVICES_TO_DISABLE_ON_STARTUP = "bluetooth wwan";
  #     DEVICES_TO_ENABLE_ON_STARTUP = "wifi";
  #   };
  # }; 

  environment.systemPackages = with pkgs; [
    nvme-cli
    iw
  ] ++ lib.optionals (config.services.xserver.enable) [ pkgs.cheese ];

  hardware.acpilight.enable = true;
  hardware.bluetooth.enable = true;

  services.acpid.enable = true;

  services.upower = {
    enable = true;
    percentageCritical = 20;
    percentageLow = 35;
  };

  networking.networkmanager = {
    enable = true;
    # wifi.macAddress = "stable";
    wifi.powersave = false;
    wifi.backend = "iwd";
    ethernet.macAddress = "stable";
    plugins = with pkgs; [
      networkmanager-openvpn
      networkmanager-openconnect
    ];
  };
  networking.wireless.userControlled.enable = true;

  environment.variables = {
    COUNTRY = "DE";
  };

  networking.wireless.iwd.settings = {
    Network = {
      EnableIPv6 = true;
      NameResolvingService = "resolvconf";
    };
    Settings = {
      AutoConnect = true;
    };
    Rank = {
      # increased preference for 5ghz networks
      BandModifier5Ghz = 1.5;
    };
  };

  # Arch wiki:
  # Setting RTS and fragmentation thresholds

# Wireless hardware disables RTS and fragmentation by default. These are two different methods of increasing throughput at the expense of bandwidth (i.e. reliability at the expense of speed). These are useful in environments with wireless noise or many adjacent access points, which may create interference leading to timeouts or failing connections.

# Packet fragmentation improves throughput by splitting up packets with size exceeding the fragmentation threshold. The maximum value (2346) effectively disables fragmentation since no packet can exceed it. The minimum value (256) maximizes throughput, but may carry a significant bandwidth cost.

# # iw phy0 set frag 512

# RTS improves throughput by performing a handshake with the access point before transmitting packets with size exceeding the RTS threshold. The maximum threshold (2347) effectively disables RTS since no packet can exceed it. The minimum threshold (0) enables RTS for all packets, which is probably excessive for most situations.

# # iw phy0 set rts 500

# Note: phy0 is the name of the wireless device as listed by iw phy.

  # only swcrypto=1 11n_disable=8: poor link quality (arch wiki)/problems with bluetooth (debian wiki)
  # power_scheme=1: disable pcie power saving (local itnerference, debian wiki)
  # cfg80211_disable_40mhz_24ghz: same as above (expect for the disable bit)
  # boot.extraModprobeConfig = ''
  #   options iwlwifi swcrypto=1 11n_disable=8 power_scheme=1 cfg80211_disable_40mhz_24ghz
  # '';
  boot.extraModprobeConfig = ''
    options iwlmvm power_scheme=1
  '';
}
