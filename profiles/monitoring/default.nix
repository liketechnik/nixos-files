{ pkgs, lib, inputs, ... }:
{

  # currently broken (it's unstable after all)
  # hardware.sensor.hddtemp.enable = true;
  # hardware.sensor.hddtemp.drives = [ "/dev/sda" "/dev/nvme0n1" ];
  environment.systemPackages = with pkgs; [
    # unstable.hddtemp disable for now
    lm_sensors
    smartmontools
    nvme-cli
  ];

  users.users."telegraf".extraGroups = [ "docker" ];
  liketechnik.postgresql = {
    enable = true;
    fullAccessDbs = [
      {
        dbName = "telegraf";
        schemaName = "telegraf";
        user = {
          name = "telegraf";
          ensurePermissions = {
            "\"pg_stat_database\"" = "SELECT";
            "\"pg_stat_bgwriter\"" = "SELECT";
            # "pg_monitor" = "";
            "DATABASE \"telegraf\"" = "CONNECT";
          };
        };
      }
    ];
  };
  services.telegraf = {
    enable = true;
    extraConfig = {
      inputs = {
        mem = {
        };
        disk = {
          ignore_fs = [ "tmpfs" "devtmpfs" "devfs" "iso9660" "overlay" "aufs" "squashfs" "bind" ];
        };
        cpu = {
          percpu = true;
          totalcpu = true;
          collect_cpu_time = true;
          report_active = true;
        };
        system = {
        };
        diskio = {
        };
        net = {
        };
        processes = {
        };
        swap = {
        };
        docker = {
          endpoint = "unix:///var/run/docker.sock";
        };
        diskio = {
        };
        influxdb = {
        };
        postgresql = {
          address = "postgres://telegraf@///telegraf?host=/run/postgresql/.s.PGSQL.5432";
        };
        # see above
        # hddtemp = { 
        # };
        interrupts = {
          cpu_as_tag = true;
        };
        kernel = {
        };
        nstat = {
        };
        sensors = {
        };
        #smart = {
        #  #path_smartcl = "${pkgs.smartmontools}/bin/smartctl";
        #  #path_nvme = "${pkgs.nvme-cli}/bin/nvme-cli";
        #};
        systemd_units = {
        };
        wireless = {
        };
        exec = {
#          commands = [ ''${pkgs.bash}/bin/bash -c echo -n "zswap $(sudo grep -R . /sys/kernel/debug/zswap/ |  awk -F":" '{printf $1"="$2","}' | sed 's/,$/\n/')" '' ]; # taken from https://github.com/dkruyt/telegraf-zswap
          commands = [
            "${pkgs.writeShellScriptBin "zswap.sh" 
              ''
                ${pkgs.coreutils}/bin/echo -n zswap $(/run/wrappers/bin/sudo ${pkgs.gnugrep}/bin/grep -R . /sys/kernel/debug/zswap/ |  ${pkgs.gawk}/bin/awk -F":" '{printf $1"="$2","}' | ${pkgs.gnused}/bin/sed 's/,$/\n/')
              ''
            }/bin/zswap.sh"
          ];

          name_suffix = "_zswap";

          data_format = "influx";
        };
        ping = {
          interval = "30s";

          urls = [ "cache.nixos.org" "hsbi.de" ];

          method = "native";

          count = 5;
        };
        net_response = {
          interval = "1m";

          protocol = "tcp";
          address = "search.nixos.org:80";
        };
      };
      outputs = {
        influxdb = {
          database = "telegraf";
          urls = [
            "http://localhost:8086"
          ];
        };
      };
    };
  };
  systemd.services."telegraf".path = [ pkgs.lm_sensors ];
  security.sudo.extraRules = [
    { 
      users = [ "telegraf" ]; 
      commands = [ { command = "${pkgs.gnugrep}/bin/grep -R . /sys/kernel/debug/zswap/"; options = [ "SETENV" "NOPASSWD" ]; } ];
    }
  ];
  services.influxdb = {
    enable = true;
  };
  services.kapacitor = {
    enable = true;
  };
  users.users.kapacitor.group = "kapacitor"; # default was nobody, but that is unsafe, so...
  virtualisation.oci-containers.containers."chronograf" = {
    image = "chronograf:1.8";
    imageFile = pkgs.dockerTools.pullImage {
      imageName = "chronograf";
      imageDigest = "sha256:28d78f7ebfd70cabe87f092c5711214b6430d593fa97ae0d07b4ca085eac0089";
      finalImageName = "chronograf";
      finalImageTag = "1.8";
      sha256 = "sha256-Y8WeULr2p2+WmzlvpJh3iHG/NOVFs1V1lL+/TvErwWQ=";
      os = "linux";
      arch = "x86_64";
    };
    autoStart = true;
    #user = "chronograf:chronograf";
    extraOptions = [ "--network=host" ];
    volumes = [ "/var/lib/chronograf:/var/lib/chronograf" ];
  };
}
