{ pkgs, ... }:
{
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.gutenprint pkgs.gutenprintBin pkgs.epson-escpr pkgs.epson-escpr2 ];
  hardware.sane.enable = true;
  hardware.sane.extraBackends = [ pkgs.sane-airscan ];

  services.saned.enable = true;

  environment.systemPackages = with pkgs; [
    cups-filters
  ];

  environment.etc = {
    # maybe unnecessary, try commenting out at some point
    # "imagescan/imagescan.conf".text = ''
    #   [devices]
    #   net.udi = networkscan:esci://192.168.178.55:1865
    #   net.vendor = Epson
    #   net.model = ET-2750
    #   net.name = Network Scanner
    # '';

    # "sane.d/epkowa.conf".text = ''
    #   net 192.168.178.55
    # '';
    # "sane.d/epson2.conf".text = ''
    #   usb
    #   net autodiscovery
    # '';
    "sane.d/airscan.conf".text = ''
      [devices]
      "Epson@Home" = http://192.168.178.55:443/eSCL/, eSCL
      "Epson@Minden" = http://192.168.178.28:443/eSCL/, eSCL
    '';
    # "utsushi/utsushi.conf".text = ''
    #   [devices]
    #   myscanner.udi = esci:networkscan://192.168.178.55:1865
    #   myscanner.vendor = Epson
    #   myscanner.model = ET-2750
    # '';
  };
}
