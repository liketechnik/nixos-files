{ config, lib, pkgs, ... }:

{
  services.vdirsyncer = {
    enable = true;
    jobs = {
      "chameleon" = {
        enable = true;

        user = "florian";
        group = "users";

        timerConfig = {
          OnBootSec = "15min";
          OnUnitActiveSec = "2h";
        };

        forceDiscover = true;

        config = {
          pairs = {
            chameleon_sync = {
              a = "chameleon_remote";
              b = "chameleon_local";
              collections = [ "from a" "from b" ];
              metadata = [ "color" "displayname" ];
            };
          };
          storages = {
            chameleon_remote = {
              type = "caldav";
              url = "http://chameleon.liketechnik:8008/user/calendars";
            };
            chameleon_local = {
              type = "filesystem";
              path = "~/.calendars/chameleon/";
              fileext = ".ics";
            };
          };
        };
      };
    };
  };
  # allow the service to access the calendar directory,
  # while still disallowing access to anything else
  systemd.services."vdirsyncer@chameleon".serviceConfig = {
    ProtectHome = lib.mkForce "tmpfs";
    BindPaths = "/home/florian/.calendars/chameleon";
  };
}
