{ pkgs, lib, config, ... }:
let
  konsole-base16 = pkgs.stdenv.mkDerivation {
    name = "konsole-base16";
    version = "2021-11-07";

    src = pkgs.fetchFromGitHub {
      owner = "cskeeters";
      repo = "base16-konsole";
      rev = "b30e0b26e766cf8a3d12afb18ac2773f53af5a87";
      sha256 = "sha256-SUpGLJmxkL1zO+sfLYogTdY/inS7prKGtDVOG6CSFyk=";
    };

    installPhase = ''
      install -m 444 -D -t $out/share/konsole/ colorscheme/*.colorscheme
    '';
  };
  # color scheme adapted from https://github.com/chriskempson/base16-tomorrow-scheme
  kde-base16-color-schemes = pkgs.runCommand "kde-base16-color-schemes" {} ''
    install -m 444 -D ${./TomorrowNight.colors} \
        $out/share/color-schemes/TomorrowNight.colors
    install -m 444 -D ${./atelier-savanna.colors} \
        $out/share/color-schemes/atelier-savanna.colors
    install -m 444 -D ${./atelier-savanna.green.colors} \
        $out/share/color-schemes/atelier-savanna-green.colors
  '';
in {
  environment.systemPackages = lib.optionals (config.services.xserver.enable) [
    konsole-base16
    kde-base16-color-schemes
  ];
}
