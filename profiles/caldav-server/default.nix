{ config, lib, pkgs, ... }:

{
  services.xandikos = {
    enable = true;

    port = 8008;

    address = "::0";

    extraOptions = [
      "--autocreate"
    ];
  };
}
