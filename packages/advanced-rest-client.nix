{ pkgs }:
let
  appimageTools = pkgs.appimageTools;
  pname = "advanced-rest-client";
  version = "16.0.1";
  name = "${pname}-${version}";

  src = pkgs.fetchurl {
    url = "https://github.com/advanced-rest-client/arc-electron/releases/download/v${version}/arc-linux-${version}-x86_64.AppImage";
    name="${pname}-${version}.AppImage";
    sha256 = "sha256-vITnJiWcEGuukNSoHM0mxXGBqaiZRF2EvddEMgFkHUY=";
  };

  appimageContents = appimageTools.extractType2 {
    inherit src pname version;
  };
in appimageTools.wrapType2 {
  inherit src pname version;

  multiPkgs = null; # no 32bit needed
  extraPkgs = pkgs: appimageTools.defaultFhsEnvArgs.multiPkgs pkgs ++ [ pkgs.bash ];

  extraInstallCommands = ''
    install -m 444 -D ${appimageContents}/${pname}.desktop $out/share/applications/${pname}.desktop
    install -m 444 -D ${appimageContents}/${pname}.png \
    $out/share/icons/hicolor/512x512/apps/${pname}.png
    substituteInPlace $out/share/applications/${pname}.desktop \
    --replace 'Exec=AppRun --no-sandbox' 'Exec=${pname}'
  '';
}

