{ pkgs, lib }:
let
in pkgs.writeShellScriptBin "backup-btrfs" ''
set -eux -o pipefail

if [[ $(id -u) -ne 0 ]]
then
    echo "Not running as root, re-executing with sudo"
    exec sudo --preserve-env=DBUS_SESSION_BUS_ADDRESS $0 "$@"
fi

BTRFS_ROOT_MOUNT=/btrfs-root

TARGET_SUBVOL=$1

pushd $BTRFS_ROOT_MOUNT

if [[ $# -ne 1 ]]
then
    echo "Target subvolume missing, quit"
    exit 1
fi
if [ ! -d $TARGET_SUBVOL ]
then
    echo $TARGET_SUBVOL does not exist, quit
    exit 1
fi

echo "Is the backup disk mounted?"
read

if [ -d ''${TARGET_SUBVOL}_backup ]
then
    echo "[WARN] snapshot already exists, skipping creation"
else
    btrfs subvolume snapshot -r $TARGET_SUBVOL ''${TARGET_SUBVOL}_backup
fi

pushd ''${TARGET_SUBVOL}_backup

archive_name=$(echo $TARGET_SUBVOL | sed -e 's|/|_|')
# well, I guess this is exaclty the opposite of what you should do,
# but I don't wanna wait for my backups :P
# so: 
# - taskset: pin borg (single threaded) to a single core
# - ionice: realtime io prio with highest prio
# - chrt: scheduling policy + priority - realtime and highest prio
taskset --cpu-list 3 ionice --class 1 --classdata 0 -- chrt --rr 99 borg create --verbose --progress --stats --show-rc --compression zstd,19 --noatime --exclude "dev" --exclude "proc" --exclude "tmp" --exclude "run" --exclude "root/.cache" --exclude ".cache" --exclude "var/tmp" --exclude "var/run" --exclude "var/lock" --exclude "media" --exclude "mnt" --exclude "home/florian/.cache" --exclude "florian/.cache" /run/media/florian/Backups_Qubes_ne/borg/voidroot::florian-p15-''${archive_name}-{now:%Y-%m-%d} .

popd

if [[ ! -z $DBUS_SESSION_BUS_ADDRESS ]]
then
    sudo --preserve-env=DBUS_SESSION_BUS_ADDRESS -u florian notify-desktop -u normal -t 5000 -i drive-harddisk-encrypted -a backup_btrfs "Backup finished"
fi

echo "Delete snapshot with command 'btrfs subvolume delete ''${TARGET_SUBVOL}_backup'?"
read
btrfs subvolume delete ''${TARGET_SUBVOL}_backup

popd

''

