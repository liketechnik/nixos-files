{ lib
, mkDerivation
, fetchFromGitHub
, cmake
, libusb1
}:

mkDerivation rec {
  pname = "wb32-dfu-updater";
  version = "1.0.0";

  src = fetchFromGitHub {
    owner = "WestberryTech";
    repo = pname;
    rev = version;
    sha256 = "sha256-DKsDVO00JFhR9hIZksFVJLRwC6PF9LCRpf++QywFO2w=";
  };

  nativeBuildInputs = [ cmake ];
  buildInputs = [ libusb1 ];
}
