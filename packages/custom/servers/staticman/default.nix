{ 
  pkgs, 
  nodejs ? pkgs.nodejs-14_x,
  writeShellScriptBin,
  symlinkJoin
}:
let
    node-pkgs = import ./node-pkgs { pkgs = pkgs; nodejs = nodejs; };
    staticman = node-pkgs."staticman-git+https://github.com/eduardoboucas/staticman.git";
    wrapped = writeShellScriptBin "staticman" ''
      cd ${staticman}/lib/node_modules/staticman
      exec ${nodejs}/bin/npm start
    '';
in symlinkJoin {
  name = "staticman";
  paths = [
    wrapped
    nodejs
    staticman
  ];
}
