{ lib
, pkgs
, fetchFromGitHub
}:
let
in pkgs.rustPlatform.buildRustPackage rec {
  pname = "polylux2pdfpc";
  version = "0.3.1";

  src = fetchFromGitHub {
    owner = "andreasKroepelin";
    repo = "polylux";
    rev = "v${version}";
    hash = "sha256-jErxl2s2xez2huUwpsT6N1pZANvuZMdIt4taFOurCtU=";
  };
  cargoHash = "sha256-vmCaQxPkzz1ZVmtX7L3VeQb3kWhVqyPoQ1NrTSiJN9Y=";

  sourceRoot = "${src.name}/pdfpc-extractor";
}
