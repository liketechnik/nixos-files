{ lib
, python3
, fetchFromGitHub
}:
let
in python3.pkgs.buildPythonApplication rec {
  pname = "base16-color-palette-creator";
  version = "2023-12-30";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "liketechnik";
    repo = pname;
    rev = "a863b91d4324688714188aebc62fe53ebaf5e733";
    hash = "sha256-4/f2hkN+IkpZynl4SKo392mXn4/ufNCJmZHO89sRr7w=";
  };

  postPatch = ''
    sed -i '1s|^|#!/usr/bin/env python3\n|' creator.py
    chmod +x creator.py
  '';

  postInstall = ''
    mv $out/bin/creator.py $out/bin/base16-color-palette-creator
  '';

  propagatedBuildInputs = with python3.pkgs; [
    pillow
    pyyaml
  ];
}
