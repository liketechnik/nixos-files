{ lib
, python3
, fetchFromGitHub
}:
let
in python3.pkgs.buildPythonApplication rec {
  pname = "base16-spectrum-generator";
  version = "2023-12-17";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "alexmirrington";
    repo = pname;
    rev = "91d69c7238d39296c0f4339c10c469e7fc7329b2";
    hash = "sha256-y3pxFL7WoSPgGB5kTC3IiAGP11Z/CAcUl+v+0KLhhjI=";
  };

  postPatch = ''
    cp ${./setup.py} setup.py
    mv main.py base16-spectrum-generator.py
    echo pyyaml >> requirements.txt
  '';

  postInstall = ''
    mv $out/bin/base16-spectrum-generator.py $out/bin/base16-spectrum-generator
  '';

  propagatedBuildInputs = with python3.pkgs; [
    numpy
    pillow
    pyyaml
  ];
}
