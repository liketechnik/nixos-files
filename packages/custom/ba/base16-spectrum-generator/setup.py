from setuptools import setup

with open('requirements.txt') as f:
    install_requires = f.read().splitlines()

setup(
  name='base16-spectrum-generator',
  install_requires=install_requires,
  scripts=[
    'base16-spectrum-generator.py',
  ],
)
