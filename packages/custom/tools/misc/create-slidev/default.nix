{
  pkgs,
  nodejs ? pkgs.nodejs-14_x,
}:
let
  node-pkgs = import ./node-pkgs { pkgs = pkgs; nodejs = nodejs; };
  create-slidev = node-pkgs."create-slidev-0.32.3";
in create-slidev
