{
  mercury
}:
let
in mercury.overrideAttrs (oldAttr: rec {
  pname = "mercury-vim-syntax";
  nativeBuildInputs = [ ];
  buildInputs = [ ];
  phases = ["unpackPhase" "installPhase"];

  installPhase = ''
    mkdir -p $out/vim
    cp -r vim $out/
  '';
})
