{ pkgs, lib }:
let
in pkgs.writeShellScriptBin "grimflow" ''
# Based on https://github.com/swaywm/sway/blob/master/contrib/grimshot
# Adpated for usage in river (by cutting out most functionality :D)

## Grimflow: a helper for screenshots within river
## Requirements:
##  - `grim`: screenshot utility for wayland
##  - `slurp`: to select an area
##  - `wl-copy`: clipboard utility
##  - `notify-desktop`: to show notifications
## Those are needed to be installed, if unsure, run `grimflow check`
##

getTargetDirectory() {
  test -f ''${XDG_CONFIG_HOME:-~/.config}/user-dirs.dirs && \
    . ''${XDG_CONFIG_HOME:-~/.config}/user-dirs.dirs

  echo ''${XDG_SCREENSHOTS_DIR:-''${XDG_PICTURES_DIR:-$HOME}}
}

if [ "$1" = "--notify" ]; then
  NOTIFY=yes
  shift 1
else
  NOTIFY=no
fi

ACTION=''${1:-usage}
SUBJECT=''${2:-area}
FILE=''${3:-$(getTargetDirectory)/$(date -Iseconds).png}

if [ "$ACTION" != "save" ] && [ "$ACTION" != "copy" ] && [ "$ACTION" != "check" ]; then
  echo "Usage:"
  echo "  $0 [--notify] (copy|save) [area] [FILE|-]"
  echo "  $0 check"
  echo "  $0 usage"
  echo ""
  echo "Commands:"
  echo "  copy: Copy the screenshot data into the clipboard."
  echo "  save: Save the screenshot to a regular file or '-' to pipe to STDOUT."
  echo "  check: Verify if required tools are installed and exit."
  echo "  usage: Show this message and exit."
  echo ""
  echo "Targets:"
  echo "  area: Manually select a region."
  exit
fi

notify() {
  notify-desktop -t 3000 -a grimflow "$@"
}
notifyOk() {
  [ "$NOTIFY" = "no" ] && return

  TITLE=''${2:-"Screenshot"}
  MESSAGE=''${1:-"OK"}
  notify "$TITLE" "$MESSAGE"
}
notifyError() {
  if [ $NOTIFY = "yes" ]; then
    TITLE=''${2:-"Screenshot"}
    MESSAGE=''${1:-"Error taking screenshot with grim"}
    notify -u critical "$TITLE" "$MESSAGE"
  else
    echo $1
  fi
}

die() {
  MSG=''${1:-Bye}
  notifyError "Error: $MSG"
  exit 2
}

check() {
  COMMAND=$1
  if command -v "$COMMAND" > /dev/null 2>&1; then
    RESULT="OK"
  else
    RESULT="NOT FOUND"
  fi
  echo "   $COMMAND: $RESULT"
}

takeScreenshot() {
  FILE=$1
  GEOM=$2
  OUTPUT=$3
  if [ ! -z "$OUTPUT" ]; then
    grim -o "$OUTPUT" "$FILE" || die "Unable to invoke grim"
  elif [ -z "$GEOM" ]; then
    grim "$FILE" || die "Unable to invoke grim"
  else
    grim -g "$GEOM" "$FILE" || die "Unable to invoke grim"
  fi
}

if [ "$ACTION" = "check" ] ; then
  echo "Checking if required tools are installed. If something is missing, install it to your system and make it available in PATH..."
  check grim
  check slurp
  check wl-copy
  check notify-desktop
  exit
elif [ "$SUBJECT" = "area" ] ; then
  GEOM=$(slurp -d)
  # Check if user exited slurp without selecting the area
  if [ -z "$GEOM" ]; then
    exit 1
  fi
  WHAT="Area"
elif [ "$SUBJECT" = "active" ] ; then
  die "Unsupported"
  FOCUSED=$(swaymsg -t get_tree | jq -r 'recurse(.nodes[]?, .floating_nodes[]?) | select(.focused)')
  GEOM=$(echo "$FOCUSED" | jq -r '.rect | "\(.x),\(.y) \(.width)x\(.height)"')
  APP_ID=$(echo "$FOCUSED" | jq -r '.app_id')
  WHAT="$APP_ID window"
elif [ "$SUBJECT" = "screen" ] ; then
  die "Unsupported"
  GEOM=""
  WHAT="Screen"
elif [ "$SUBJECT" = "output" ] ; then
  die "Unsupported"
  GEOM=""
  OUTPUT=$(swaymsg -t get_outputs | jq -r '.[] | select(.focused)' | jq -r '.name')
  WHAT="$OUTPUT"
elif [ "$SUBJECT" = "window" ] ; then
  die "Unsupported"
  GEOM=$(swaymsg -t get_tree | jq -r '.. | select(.pid? and .visible?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | slurp)
  # Check if user exited slurp without selecting the area
  if [ -z "$GEOM" ]; then
   exit 1
  fi
  WHAT="Window"
else
  die "Unknown subject to take a screen shot from" "$SUBJECT"
fi

if [ "$ACTION" = "copy" ] ; then
  takeScreenshot - "$GEOM" "$OUTPUT" | wl-copy --type image/png || die "Clipboard error"
  notifyOk "$WHAT copied to buffer"
else
  if takeScreenshot "$FILE" "$GEOM" "$OUTPUT"; then
    TITLE="Screenshot of $SUBJECT"
    MESSAGE=$(basename "$FILE")
    notifyOk "$MESSAGE" "$TITLE"
    echo $FILE
  else
    notifyError "Error taking screenshot with grim"
  fi
fi
''
