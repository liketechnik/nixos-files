# adapted from the sway module in nixpkgs
{ pkgs }:
let
  riverBaseWrapper = pkgs.writeShellScriptBin "river" ''
    set -o errexit
    if [ ! "$_RIVER_WRAPPER_ALREADY_EXECUTED" ]; then
      export XDG_CURRENT_DESKTOP=river
      export XDG_SESSION_TYPE=wayland

      # set keyboard layout
      export XKB_DEFAULT_OPTIONS=caps:escape
      export XKB_DEFAULT_LAYOUT="us(altgr-intl)"

      # 'fix' qt apps
      export QT_QPA_PLATFORMTHEME=kde 
      export QT_STYLE_OVERRIDE=breeze-dark

      export _RIVER_WRAPPER_ALREADY_EXECUTED=1
    fi
    if [ "DBUS_SESSION_BUS_ADDRESS" ]; then
      export DBUS_SESSION_BUS_ADDRESS
      exec ${pkgs.river}/bin/river "$@"
    else
      exec ${pkgs.dbus}/bin/dbus-run-session ${pkgs.river}/bin/river "$@"
    fi
  '';
  riverSessionFile = pkgs.writeTextFile {
    name = "river.desktop"; 
    text = ''
      [Desktop Entry]
      Name=River
      Comment=A dynamic tiling Wayland compositor
      Exec=river
      Type=Application
    ''; 
    executable = true; 
    destination = "/share/wayland-sessions/river.desktop";
  };
  riverctl-xdg-open-wrapper = pkgs.writeShellScriptBin "riverctl-spawn-xdg-open" ''
    riverctl spawn "xdg-open '$1'"
  '';
in  pkgs.symlinkJoin {
  name = "river-${pkgs.river.version}";

  paths = [ riverBaseWrapper riverSessionFile pkgs.river riverctl-xdg-open-wrapper ];

  nativeBuildInputs = [ pkgs.makeWrapper pkgs.wrapGAppsHook ];

  buildInputs = [ pkgs.gdk-pixbuf pkgs.glib pkgs.gtk3 ];

  dontWrapGApps = true;

  postBuild = ''
      gappsWrapperArgsHook

      wrapProgram $out/bin/river \
        "''${gappsWrapperArgs[@]}" \
  '';

  passthru = {
    inherit (pkgs.river.passthru) tests;

    providedSessions = [ "river" ];
  };

  meta = builtins.removeAttrs pkgs.river.meta [ "outputsToInstall" ];
}

