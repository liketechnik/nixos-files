{
  callPackage,
  inputs,
  rustPlatform
}:
let
in callPackage "${inputs.nixpkgs-unstable}/pkgs/applications/audio/helvum" {
  inherit rustPlatform;
}
