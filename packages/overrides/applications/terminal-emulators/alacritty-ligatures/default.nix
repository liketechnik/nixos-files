{ 
  alacritty,
  inputs,
  lib,
  stdenv
}:
let 
in alacritty.overrideAttrs (oldAttr: rec {
  src = inputs.alacritty;

  pname = "alacritty-ligature";
  version = "0.8.0-dev";

  # src = pkgs.fetchFromGitHub {
  #   owner = "zenixls2";
  #   repo = "alacritty";
  #   rev = "3ed043046fc74f288d4c8fa7e4463dc201213500";
  #   sha256 = "sha256-1dGk4ORzMSUQhuKSt5Yo7rOJCJ5/folwPX2tLiu0suA=";
  # };

  cargoDeps = oldAttr.cargoDeps.overrideAttrs (lib.const {
    name = "alacritty-vendor.tar.gz";
    inherit src;
    # gonna need that for the next update: 
    #outputHash = "0000000000000000000000000000000000000000000000000000";
    outputHash = "sha256-5zMDPPhk5Qr8a49/Reg9k8fJO00+8iWFVV1qC5YLhI8=";
  });

  postInstall = oldAttr.postInstall + ''
    patchelf --set-rpath $(patchelf --print-rpath $out/bin/alacritty):"${lib.makeLibraryPath [ stdenv.cc.cc.lib ]}" $out/bin/alacritty
  '';
})
