{
  ledger,
  lib,
  fetchFromGitHub,
}:
let
  version = "3.2.1";
  outdated = ledger.version <= "3.2.1";
in if outdated then (ledger.overrideAttrs(old: rec {
      version = "2022-08-25";
      src = fetchFromGitHub {
        owner = "ledger";
        repo = "ledger";
        rev = "cccb827886773066665e99df4a4ecec4ab5bda14";
        sha256 = "sha256-yA5fgOAjXs4WnKOYarFK7xzPwwf4+JMATmItn64TVlo=";
      };
      patches = [];
})) else lib.warn "ignoring ledger override since nixpkgs version is more recent" ledger
