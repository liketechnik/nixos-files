{
  callPackage,
  inputs,
  makeFontsConf,
  bash,
  writeScript
}:
let 
  android-studio = import "${inputs.nixpkgs}/pkgs/applications/editors/android-studio/common.nix" {
      channel = "stable";
      pname = "android-studio";

      version = "2021.1.1.23";
      sha256Hash = "sha256-s3UGzYrHqA/jDNFyTjvlwtlwp6pqo/ycp0Wv43AKq88=";
    };
    waylandStartScript = originalStartScript: ''
      #!${bash}/bin/bash
      export QT_QPA_PLATFORM=xcb
      # hack to make this thing source /etc/profile correctly
      exec ${bash}/bin/bash -l -c ${originalStartScript}
    '';
in (callPackage android-studio { 
      fontsConf = makeFontsConf {
        fontDirectories = [];
      };
      tiling_wm = true;
    }).overrideAttrs (oldAttrs: let 
        originalStartScript = writeScript "android-studio" oldAttrs.startScript;
      in rec {
        startScript = waylandStartScript originalStartScript;
    })
