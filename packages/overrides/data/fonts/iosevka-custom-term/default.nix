{
  iosevka
}:
let
in iosevka.override {
  privateBuildPlan = {
    family = "Iosevka Custom Term";
    spacing = "term";
    serifs = "sans";
  };
  set = "custom-term";
}
