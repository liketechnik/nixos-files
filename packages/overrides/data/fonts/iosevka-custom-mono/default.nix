{
  iosevka
}:
let
in (iosevka.override {
  privateBuildPlan = {
    family = "Iosevka Custom Mono";
    spacing = "fontconfig-mono";
    serifs = "sans";
  };
  set = "custom-mono";
}).overrideAttrs (oldAttr: rec {
  buildPhase = ''
    runHook preBuild
    npm run build --no-update-notifier -- --jCmd=2 ttf::$pname >/dev/null
    runHook postBuild
  '';
})

