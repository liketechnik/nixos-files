{
  iosevka
}:
let
in iosevka.override {
  privateBuildPlan = {
    family = "Iosevka Custom Term Serif";
    spacing = "term";
    serifs = "slab";
  };
  set = "custom-term-serif";
}
