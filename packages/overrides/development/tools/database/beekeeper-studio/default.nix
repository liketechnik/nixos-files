{
  callPackage,
  inputs
}:
let
in callPackage "${inputs.nixpkgs-unstable}/pkgs/development/tools/database/beekeeper-studio" { }

