{ 
  vpn-slice,
  iptables,
  iproute2,
  dig
}:
let
in vpn-slice.overrideAttrs (oldAttr: rec { 
  propagatedBuildInputs = oldAttr.propagatedBuildInputs ++ [
    iptables
    iproute2
  ];
  # postPatch = ''
  #   sed -i "s|'/sbin/ip'|'${iproute2}/bin/ip'|" vpn_slice/linux.py
  #   sed -i "s|'/sbin/iptables'|'${iptables}/bin/iptables'|" vpn_slice/linux.py
  # '';
})
