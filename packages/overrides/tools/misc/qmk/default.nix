{ 
  callPackage,
  inputs
}:
let
in callPackage "${inputs.nixpkgs-unstable}/pkgs/tools/misc/qmk" {}
