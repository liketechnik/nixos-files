# copied from https://github.com/NixOS/nixpkgs/pull/92797
{ pkgs, lib }:
let 
in pkgs.python3Packages.buildPythonApplication rec {
  pname = "notifymuch";
  version = "0.1";

  src = pkgs.fetchFromGitHub {
    owner = "kspi";
    repo = "notifymuch";
    rev = "9d4aaf54599282ce80643b38195ff501120807f0";
    sha256 = "1lssr7iv43mp5v6nzrfbqlfzx8jcc7m636wlfyhhnd8ydd39n6k4";
  };

  propagatedBuildInputs = with pkgs; [
    python3Packages.notmuch
    python3Packages.pygobject3
    libnotify
    gtk3
  ];

  nativeBuildInputs = with pkgs; [
    gobject-introspection
    wrapGAppsHook
  ];

  dontWrapGApps = true;

  preFixup = ''
    echo "wrapper args"
    echo "''${makeWrapperArgs[@]}"
    makeWrapperArgs+=("''${gappsWrapperArgs[@]}")
    echo "wrapper args again"
    echo "''${makeWrapperArgs[@]}"
  '';

  strictDeps = false;

  meta = with lib; {
    description = "Display desktop notifications for unread mail in a notmuch database";
    homepage = "https://github.com/kspi/notifymuch";
    maintainers = with maintainers; [ glittershark ];
    license = licenses.gpl3;
  };
}

