{ pkgs }:
let 
in pkgs.rustPlatform.buildRustPackage rec {
    pname = "bartib";
    version = "1.0.0";

    src = pkgs.fetchFromGitHub {
      owner = "nikolassv";
      repo = pname;
      rev = "v${version}";
      sha256 = "sha256-OPtEKe7WSPiXFmrm4TVEV9CAq1BanN4FYHJ9fhf4Q90=";
    };
    cargoHash = "sha256-4jz1UrBRC51r03xBXAVGk7snvld9EdVK+V2VucKNZ7c=";

    nativeBuildInputs = with pkgs; [ installShellFiles makeWrapper ];

    postInstall = ''
      mv misc/bartibCompletion.sh misc/bartibCompletion.bash
      installShellCompletion misc/bartibCompletion.bash
    '';
  }
