test:
{ pkgs, self }:
(self.inputs.nixpkgs.lib.nixos.runTest {
  hostPkgs = pkgs;
  # speed up evaluation by skipping evaluating documentation
  defaults.documentation.enable = pkgs.lib.mkDefault false;
  node.specialArgs = {
    inherit self;
  };
  imports = [ test ];
}).config.result

