(import ./lib.nix) {
  name = "btrfs-boot-snapshots";
  nodes = {
    machine =
      { self, pkgs, lib, ...}:
      {
        # Use systemd-boot
        virtualisation = {
          emptyDiskImages = [ 512 ];
          useBootLoader = true;
          useEFIBoot = true;
        };
        boot.loader.systemd-boot.enable = true;
        boot.loader.efi.canTouchEfiVariables = true;

        environment.systemPackages = with pkgs; [ btrfs-progs ];

        specialisation.boot-btrfs-mounts.configuration = {
          boot.initrd.postMountCommands = let
            btrfs-initrd-snappshot = pkgs.callPackage ../lib/btrfs-initrd-snapshot.nix {};
          in lib.mkVMOverride ''
            echo "Creating boot-time snapshots..."
            ${btrfs-initrd-snappshot}/bin/btrfs-initrd-snapshot root root bootsnapshots
            ${btrfs-initrd-snappshot}/bin/btrfs-initrd-snapshot root home bootsnapshots
            ${btrfs-initrd-snappshot}/bin/btrfs-initrd-snapshot root var-lib bootsnapshots
          '';
          fileSystems = lib.mkVMOverride {
            "/root" = {
              label = "root";
              fsType = "btrfs";
              options = [
                "subvol=root"
              ];
              neededForBoot = true;
            };
            "/home" = {
              label = "root";
              fsType = "btrfs";
              options = [
                "subvol=home"
              ];
              neededForBoot = true;
            };
            "/var-lib" = {
              label = "root";
              fsType = "btrfs";
              options = [
                "subvol=var-lib"
              ];
              neededForBoot = true;
            };
            "/btrfs-root" = {
              label = "root";
              fsType = "btrfs";
              neededForBoot = true;
            };
        };
      };
    };
  };

  testScript = ''
    machine.start(allow_reboot=True)

    # setup btrfs fs
    machine.succeed("mkfs.btrfs -L root /dev/vdb")
    machine.succeed("mkdir -p /mnt && mount /dev/vdb /mnt && btrfs subvolume create /mnt/root /mnt/home /mnt/var-lib")
    machine.succeed("echo init > /mnt/var-lib/init")
    machine.succeed("bootctl set-default nixos-generation-1-specialisation-boot-btrfs-mounts.conf")
    machine.succeed("sync")

    # creation of first snapshot set (latest-0)
    machine.reboot()
    machine.wait_for_unit("multi-user.target")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_root/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-0"))
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest-1")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_home/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-0"))
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest-1")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_var-lib/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-0"))
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest-1")

    "init" in machine.succeed("cat /var-lib/init")
    machine.succeed("echo first > /var-lib/first")

    # creation of second snapshot set (latest-1)
    machine.reboot()
    machine.wait_for_unit("multi-user.target")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_root/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-1"))
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest-2")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_home/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-1"))
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest-2")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_var-lib/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-1"))
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest-2")

    "init" in machine.succeed("cat /var-lib/init")
    "first" in machine.succeed("cat /var-lib/first")
    machine.succeed("echo second > /var-lib/second")

    # creation of third snapshot set (latest-2)
    machine.reboot()
    machine.wait_for_unit("multi-user.target")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_root/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-2"))
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest-3")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_home/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-2"))
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest-3")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_var-lib/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-2"))
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest-3")

    "init" in machine.succeed("cat /var-lib/init")
    "first" in machine.succeed("cat /var-lib/first")
    "second" in machine.succeed("cat /var-lib/second")
    machine.succeed("echo third > /var-lib/third")

    # creation of fourth snapshot set (latest-3)
    machine.reboot()
    machine.wait_for_unit("multi-user.target")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_root/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-3"))
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest-4")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_home/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-3"))
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest-4")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_var-lib/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-3"))
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest")
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest-4")

    "init" in machine.succeed("cat /var-lib/init")
    "first" in machine.succeed("cat /var-lib/first")
    "second" in machine.succeed("cat /var-lib/second")
    "third" in machine.succeed("cat /var-lib/third")
    machine.succeed("echo fourth > /var-lib/fourth")

    # creation of fifth snapshot set (latest-4)
    machine.reboot()
    machine.wait_for_unit("multi-user.target")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_root/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-3"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-4"))
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_home/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-3"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-4"))
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_var-lib/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-3"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-4"))
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest")

    "init" in machine.succeed("cat /var-lib/init")
    "first" in machine.succeed("cat /var-lib/first")
    "second" in machine.succeed("cat /var-lib/second")
    "third" in machine.succeed("cat /var-lib/third")
    "fourth" in machine.succeed("cat /var-lib/fourth")
    machine.succeed("echo fifth > /var-lib/fifth")

    # deletion of oldest and creation new latest snapshot
    machine.reboot()
    machine.wait_for_unit("multi-user.target")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_root/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-3"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_root/latest-4"))
    machine.fail("ls /btrfs-root/bootsnapshots_root/latest")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_home/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-3"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_home/latest-4"))
    machine.fail("ls /btrfs-root/bootsnapshots_home/latest")

    print(machine.succeed("ls /btrfs-root/bootsnapshots_var-lib/"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-0"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-1"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-2"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-3"))
    print(machine.succeed("btrfs subvolume show /btrfs-root/bootsnapshots_var-lib/latest-4"))
    machine.fail("ls /btrfs-root/bootsnapshots_var-lib/latest")

    "init" in machine.succeed("cat /var-lib/init")
    "first" in machine.succeed("cat /var-lib/first")
    "second" in machine.succeed("cat /var-lib/second")
    "third" in machine.succeed("cat /var-lib/third")
    "fourth" in machine.succeed("cat /var-lib/fourth")
    "fifth" in machine.succeed("cat /var-lib/fifth")
  '';
}
