fsLabel="$1"
subvol="$2"
snapshotDirPrefix="$3"
if [ -z "$fsLabel" ]
then
    echo "argument 1 (fsLabel) missing"
    exit 2
fi
if [ -z "$subvol" ]
then
    echo "argument 2 (subvol) missing"
    exit 2
fi
if [ -z "$snapshotDirPrefix" ]
then
    echo "argument 3 (snapshotDirPrefix) missing"
    exit 2
fi
fsRoot="$(findmnt --noheadings --source LABEL="${fsLabel}" --output TARGET --options subvolid=5 --first-only)"
if [ -z "$fsRoot" ]
then
    echo "unable to find fsroot for label '${fsLabel}'"
    exit 1
fi

subvolPath="${fsRoot}/${subvol}"
if [ ! -d "${subvolPath}" ]
then
    echo "Subvolume '${subvolPath}' does not exist!"
    exit 1
fi

snapshotDirPath="${fsRoot}/${snapshotDirPrefix}_${subvol}"
if [ ! -d "${snapshotDirPath}" ]
then
    mkdir "${snapshotDirPath}"
fi

if [ -d "${snapshotDirPath}/latest" ]
then
    echo "Snapshot dir '${snapshotDirPath}' in invalid state (latest exists; but shouldn't)"
    exit 1
fi

btrfs subvolume snapshot -r "${fsRoot}/${subvol}" "${snapshotDirPath}/latest"

if [ -d "${snapshotDirPath}/latest-4" ]
then
    btrfs subvolume delete "${snapshotDirPath}/latest-4"
fi

if [ -d "${snapshotDirPath}/latest-3" ]
then
mv "${snapshotDirPath}/latest-3" "${snapshotDirPath}/latest-4"
fi

if [ -d "${snapshotDirPath}/latest-2" ]
then
mv "${snapshotDirPath}/latest-2" "${snapshotDirPath}/latest-3"
fi

if [ -d "${snapshotDirPath}/latest-1" ]
then
mv "${snapshotDirPath}/latest-1" "${snapshotDirPath}/latest-2"
fi

if [ -d "${snapshotDirPath}/latest-0" ]
then
mv "${snapshotDirPath}/latest-0" "${snapshotDirPath}/latest-1"
fi

mv "${snapshotDirPath}/latest" "${snapshotDirPath}/latest-0"
