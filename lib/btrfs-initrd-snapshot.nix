{ pkgs, ... }:
# I'd have loved to make this parameterized in nix,
# but writeShellApplication has no direct interface for string replacement.
# Alas, pass the 3 args when calling this script.
pkgs.writeShellApplication {
  name = "btrfs-initrd-snapshot";
  runtimeInputs = with pkgs; [ util-linux btrfs-progs ];
  text = builtins.readFile ./btrfs-initrd-snapshot.sh;
}

