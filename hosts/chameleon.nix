{ config, pkgs, lib, sops, ... }:
{
  sops.secrets."staticman".sopsFile = ../secrets/staticman/chameleon.yaml;
  sops.secrets."gitlab-runner/general-purpose/registrationConfigFile".sopsFile = ../secrets/gitlab-runner/chameleon.yaml;
  sops.secrets."gitlab-runner/general-privileged/registrationConfigFile".sopsFile = ../secrets/gitlab-runner/chameleon.yaml;
  sops.secrets."gitlab-runner/docker-cargo-tools/registrationConfigFile".sopsFile = ../secrets/gitlab-runner/chameleon.yaml;
  sops.secrets."gitlab-runner/convertible-structured-text/registrationConfigFile".sopsFile = ../secrets/gitlab-runner/chameleon.yaml;
  sops.secrets."gitlab-runner/nix-reformat/registrationConfigFile".sopsFile = ../secrets/gitlab-runner/chameleon.yaml;

  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "de_DE.UTF-8";

  boot.loader.grub.devices = [ "/dev/sda" ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "sd_mod" "rtsx_pci_sdmmc" "virtio_pci" "sr_mod" "virtio_scsi" ];
  boot.initrd.kernelModules = [ "dm-snapshot" "dm-mod" "zstd" "z3fold" ];

  boot.kernelModules = [ "kvm-intel" "kvm-amd" ];

  nix.settings.max-jobs = 1;
  nix.settings.cores = 1;

  networking.hostName = "chameleon";
  networking.domain = "liketechnik.name";
  networking.defaultGateway6 = {
    interface = "enp1s0";
    address = "fe80::1";
  };
  networking.dhcpcd.extraConfig = ''
    interface enp1s0
    static ip6_address=2a01:4f9:c011:75c6::1/64
  '';
  # prevent Error: Nexthop device is not up from occurring
  systemd.services.network-setup = {
    bindsTo = [ "sys-subsystem-net-devices-enp1s0.device" ];
    after = [ "sys-subsystem-net-devices-enp1s0.device" ];
    serviceConfig = {
      Restart = "on-failure";
      RestartSec = "1s";
    };
  };

  services.beesd.filesystems.root = {
    spec = "/btrfs-root";
    extraOptions = [ "--loadavg-target" "1.0" "--thread-min" "0" ];
    hashTableSizeMB = 32; # recommended for uncompressed data: 128mb/TB; 2-4x for compressed data; estimate: 50GB disk space->128/20 = 6.4->6.4*4 = 25.6
  };
  services.beesd.filesystems.bigdata = {
    spec = "/bigdata-root";
    extraOptions = [ "--loadavg-target" "1.0" "--thread-min" "0" ];
    hashTableSizeMB = 32; # recommended for uncompressed data: 128mb/TB; 2-4x for compressed data; estimate: 50GB disk space->128/20 = 6.4->6.4*4 = 25.6
  };

  services.btrfs.autoScrub = {
    enable = true;
    interval = "weekly";
    fileSystems = [ "/btrfs-root" "/bigdata-root" ];
  };

  # we have btrfs (e. g. fs level compression + dedup) for that
  nix.settings.auto-optimise-store = lib.mkForce false;
  nix.optimise.automatic = lib.mkForce false;

  boot.kernelParams = [
  ];

  # instead of setting the following kernel params
  # "zswap.enabled=1"
  # "zswap.compressor=zstd"
  # "zswap.zpool=z3fold"
  # do this here so that the modules have already been loaded
  boot.postBootCommands = ''
    echo zstd > /sys/module/zswap/parameters/compressor
    echo z3fold > /sys/module/zswap/parameters/zpool
    echo 1 > /sys/module/zswap/parameters/enabled
  '';

  boot.kernel.sysctl = {
    "vm.swappiness" = 90;
  };

  fileSystems."/" = {
    label = "flearoot";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=root"
    ];
    neededForBoot = true;
  };

  fileSystems."/btrfs-root" = {
    label = "flearoot";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
    ];
    neededForBoot = true;
  };

  fileSystems."/bigdata-root" = {
    label = "bigdata";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
    ];
    neededForBoot = true;
  };

  fileSystems."/home" = {
    label = "flearoot";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=home"
    ];
    neededForBoot = true;
  };

  fileSystems."/nix" = {
    label = "bigdata";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=nix"
    ];
    neededForBoot = true;
  };

  fileSystems."/var/lib" = {
    label = "bigdata";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=var-lib"
    ];
    neededForBoot = true;
  };

  fileSystems."/var/db" = {
    label = "flearoot";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=var-db"
    ];
    neededForBoot = true;
  };

  fileSystems."/boot" = {
    label = "boot";
  };

}
