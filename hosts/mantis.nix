{ config, pkgs, lib, inputs, ... }:
let
  btrfs-initrd-snappshot = pkgs.callPackage ../lib/btrfs-initrd-snapshot.nix {};
  initrd-snapshot = subvol: fsLabel: "${btrfs-initrd-snappshot}/bin/btrfs-initrd-snapshot ${fsLabel} ${subvol} bootsnapshots";

   rocmEnv = pkgs.symlinkJoin {
      name = "rocm-combined";
      paths = with pkgs.rocmPackages; [
        rocblas
        hipblas
        clr
      ];
    };
in {
  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "de_DE.UTF-8";
  i18n.inputMethod.enabled = null;

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "sd_mod" "rtsx_pci_sdmmc" "z3fold" "zstd" ];

  boot.initrd.kernelModules = [ "dm-snapshot" "z3fold" "zstd" "tcp_veno" ];
  boot.kernelModules = [ "kvm-amd" "amdgpu" ];
  boot.extraModulePackages = [ ];

  hardware.cpu.amd.updateMicrocode = true;
  hardware.tuxedo-keyboard.enable = true;

  hardware.logitech = {
    wireless = {
      enable = true;
      enableGraphical = true;
    };
  };

  services.xserver.videoDrivers = [ "amdgpu" ];

  # this version was used to create the btrfs filesystem 
  # on this host (which was latest at creation). 
  # On a second thought,
  # I don't think I want this to be latest,
  # I'd much rather prefer this one to be the default (e. g. linuxPackages),
  # but on the otherhand don't want to downgrade the kernel,
  # as btrfs seems to work fine in this version.
  # Therefore:
  # FIXME remove the pin as soon as as pkgs.linuxPackages is at the next lts >= 5.14
  boot.kernelPackages = pkgs.linuxKernel.packages.linux_xanmod;

  # interestingly this service fails, due to no such device being available
  # as I did *not* enable this service in any way, it *should* be fine to just disable it :)
  # systemd.suppressedSystemUnits = [ "systemd-backlight@backlight:acpi_video0.service" ];

  hardware.graphics.extraPackages = with pkgs; [
    rocmPackages.clr.icd

    libvdpau-va-gl
  ];
  hardware.graphics.extraPackages32 = with pkgs; [
    rocmPackages.clr.icd

    libvdpau-va-gl
  ];

  systemd.tmpfiles.rules = [
    "L+    /opt/rocm/   -    -    -     -    ${rocmEnv}"
  ];

  hardware.firmware = with pkgs; [
    wireless-regdb
  ];

  services.tlp.enable = lib.mkForce false; # we have tcc for that

  hardware.tuxedo-rs = {
    enable = true;
    tailor-gui.enable = true;
  };

  networking.networkmanager.wifi.macAddress = "stable";

  services.thermald.enable = true;

  nix.settings.max-jobs = 8;
  nix.settings.cores = 8;

  networking.hostName = "mantis";

  services.beesd.filesystems.root = {
    verbosity = "err";
    spec = "/btrfs-root";
    extraOptions = [ "--loadavg-target" "10.0" "--thread-min" "1" "--thread-factor" "0.5" ]; # if more than half of cpu's are busy, beesd shouldn't be active
    hashTableSizeMB = 1024; # recommended for uncompressed data: 128mb/TB; 2-4x for compressed data; estimate: 2TB disk space->128*2= 265->256*4 = 1024
  };

  services.btrfs.autoScrub = {
    enable = true;
    interval = "weekly";
    fileSystems = [ "/" ];
  };

  # we have btrfs (e. g. fs level compression + dedup) for that
  nix.settings.auto-optimise-store = lib.mkForce false;
  nix.optimise.automatic = lib.mkForce false;

  boot.kernelParams = [
    # force deep (suspend-to-ram) suspend/sleep mode
    # instead of s2idle, which is not supported
    # on this hardware (well, support is advertised by bios
    # and detected by the kernel, but the amdgpu driver
    # complains when actually trying to use it,
    # and is unable to correctly put the gpu to sleep)
    "mem_sleep_default=deep"
    "ieee80211_regdom=DE"
    # "acpi_backlight=video"
    "amd_pstate=active"
  ];

  # instead of setting the following kernel params
  # "zswap.enabled=1"
  # "zswap.compressor=zstd"
  # "zswap.zpool=z3fold"
  # do this here so that the modules have already been loaded
  boot.postBootCommands = ''
    echo zstd > /sys/module/zswap/parameters/compressor
    echo z3fold > /sys/module/zswap/parameters/zpool
    echo 1 > /sys/module/zswap/parameters/enabled
    # use the powersave governor so that epp is available
    echo "setting up scaling governor powersave"
    ${pkgs.bash}/bin/bash -c 'for cpu in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do echo powersave > $cpu; done'
    echo "setting up epp balance_power"
    ${pkgs.bash}/bin/bash -c 'for cpu in /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference; do echo balance_power > $cpu; done'
    echo "setting up max freq 2.9GHz"
    ${pkgs.bash}/bin/bash -c 'for cpu in /sys/devices/system/cpu/cpu*/cpufreq/scaling_max_freq; do echo 2900000 > $cpu; done'
  '';

  powerManagement.resumeCommands = ''
    # use the powersave governor so that epp is available
    echo "setting up scaling governor powersave"
    ${pkgs.bash}/bin/bash -c 'for cpu in /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor; do echo powersave > $cpu; done'
    echo "setting up epp balance_power"
    ${pkgs.bash}/bin/bash -c 'for cpu in /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference; do echo balance_power > $cpu; done'
    echo "setting up max freq 2.9GHz"
    ${pkgs.bash}/bin/bash -c 'for cpu in /sys/devices/system/cpu/cpu*/cpufreq/scaling_max_freq; do echo 2900000 > $cpu; done'
  '';

  boot.kernel.sysctl = {
    "vm.swappiness" = 90;
    # limit for intellij
    "fs.notify.max_user_watches" = 524288;
    # different tcp congestion control
    "net.ipv4.tcp_congestion_control" = "veno";
  };

  boot.initrd.luks.devices = {
    cryptroot = {
      device = "/dev/disk/by-label/cryptroot";
    };
    cryptswap = {
      device = "/dev/disk/by-label/cryptswap";
    };
  };

  boot.initrd.postMountCommands = ''
    echo "Creating boot-time snapshots..."
    ${initrd-snapshot "root" "root"}
    ${initrd-snapshot "home" "root"}
    ${initrd-snapshot "nix" "root"}
    ${initrd-snapshot "var-db" "root"}
    ${initrd-snapshot "var-lib" "root"}
    # initrd-snapshot "srv" "/btrfs-root/root"}
    # initrd-snapshot "machines" "/btrfs-root/var-lib"}
    echo "Finished creating boot-time snapshots..."
  '';

  fileSystems."/" = {
    label = "root";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=root"
    ];
    neededForBoot = true;
  };

  fileSystems."/btrfs-root" = {
    label = "root";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
    ];
    neededForBoot = true;
  };

  fileSystems."/home" = {
    label = "root";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=home"
    ];
    neededForBoot = true;
  };

  fileSystems."/nix" = {
    label = "root";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=nix"
    ];
    neededForBoot = true;
  };

  fileSystems."/var/db" = {
    label = "root";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=var-db"
    ];
    neededForBoot = true;
  };

  fileSystems."/var/lib" = {
    label = "root";
    fsType = "btrfs";
    options = [
      "defaults"
      "noatime"
      "compress=zstd:4"
      "subvol=var-lib"
    ];
    neededForBoot = true;
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/BOOT";
  };

  swapDevices = [ 
    { 
      device = "/dev/disk/by-label/swap";
    }
  ];

  fileSystems."/srv/nfs/meetings" = {
    device = "/home/florian/Documents/meetings";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/windoof" = {
    device = "/home/florian/Documents/windoof";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/fh" = {
    device = "/home/florian/Documents/fh-dblab";
    options = [ "bind" ];
  }; 

  services.openssh.openFirewall = false;

  networking.hosts = {
    "10.200.7.250" = [ "localhost.qemu-meeting" "qemu-meeting" ];
    #10.200.7.189               localhost.windoof       windoof
    "10.200.7.108" = [ "localhost.kde-dev" "kde-dev" ];
    "10.200.7.87" = [ "localhost.fh-dblab" "fh-dblab" ];
    "192.168.178.93" = [ "fritz.box.flos-pi3" "flos-pi3" ];
    "192.168.178.94" = [ "fritz.box.flos-pi4" "flos-pi4" ];
  }; 

}
