{ config, pkgs, lib, ... }:
{
  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "de_DE.UTF-8";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "sd_mod" "rtsx_pci_sdmmc" "z3fold" "zstd" ];
  boot.initrd.kernelModules = [ "dm-snapshot" "z3fold" "zstd" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  hardware.cpu.intel.updateMicrocode = true;

  security.allowSimultaneousMultithreading = false;
  # default cpu speed of kudu5
  powerManagement.cpufreq.max = 2200000;

  hardware.graphics.extraPackages = with pkgs; [
    intel-compute-runtime
    vaapiVdpau
    #vaapiIntel
    #vaapi-intel-hybrid
    intel-media-driver
    #libvdpau-va-gl
  ];
  hardware.graphics.extraPackages32 = with pkgs; [
    intel-compute-runtime
    vaapiVdpau
    #vaapiIntel
    #vaapi-intel-hybrid
    intel-media-driver
    #libvdpau-va-gl
  ];
  
  hardware.system76.enableAll = true;

  hardware.firmware = with pkgs; [
    wireless-regdb
  ];

  environment.systemPackages = [
    pkgs.system76-firmware

    # breaks vulkan for non-intel hardware, therefore must go here
    (pkgs.steam.override { extraProfile = ''
      unset VK_ICD_FILENAMES
      export VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/intel_icd.i686.json:/usr/share/vulkan/icd.d/intel_icd.x86_64.json
      '';
    })
  ];

  services.thermald.enable = true;

  nix.settings.max-jobs = 8;
  nix.settings.cores = 8;

  networking.hostName = "florian-76k";

  liketechnik.wireguard.enable = true;

  boot.kernelParams = [
    "ieee80211_regdom=DE"
  ];

  boot.postBootCommands = ''
    echo zstd > /sys/module/zswap/parameters/compressor
    echo z3fold > /sys/module/zswap/parameters/zpool
    echo 1 > /sys/module/zswap/parameters/enabled

  '';

  boot.kernel.sysctl = {
    "vm.swappiness" = 90;
    # limit for intellij
    "fs.notify.max_user_watches" = 524288;
    # mesa_intel driver complains if this isn't set
    # (allows access to system-wide metrics for non-root users,
    # according to https://www.kernel.org/doc/html/v5.1/gpu/i915.html#overview)
    "dev.i915.perf_stream_paranoid" = 0;
  };

  boot.initrd.luks.devices = {
    cryptroot = {
      device = "/dev/disk/by-uuid/5eb648a6-0dc3-46e1-9f25-2e3fa7e058cd";
    };
    cryptbigdata = {
      device = "/dev/disk/by-uuid/4dd63921-0b50-409b-9617-ff99d981fb5e";
    };
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/a042e487-ba2c-44cb-93d2-1efc4d086d9b";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/8422-CCC1";
  };

  fileSystems."/bigdata" = {
    device = "/dev/disk/by-uuid/4e279a66-8263-4f2a-acd5-3efb3b359e99";
  };

    fileSystems."/var/lib/libvirt" =
    { device = "/bigdata/var/lib/libvirt";
      fsType = "none";
      options = [ "bind" ];
    };

    fileSystems."/var/lib/docker" =
    { device = "/bigdata/var/lib/docker";
      fsType = "none";
      options = [ "bind" ];
    };
    fileSystems."/var/lib/containers" =
    { device = "/bigdata/var/lib/containers";
      fsType = "none";
      options = [ "bind" ];
    };
    fileSystems."/home/florian/.local/share/containers" =
    { device = "/bigdata/documents-hidden/local-share-containers";
      fsType = "none";
      options = [ "bind" ];
    };

    swapDevices =
    [ { device = "/dev/disk/by-uuid/3369167c-9c76-4b30-ada3-e353e2e5a29e"; }
    ];

  fileSystems."/home/florian/Documents/linux_iamges" = {
    device = "/bigdata/documents/linux_images";
    options = [ "bind" ];
  };

  fileSystems."/home/florian/.var/app/com.valvesoftware.Steam" = {
    device = "/bigdata/documents/com.valvesoftware.Steam";
    options = [ "bind" ];
  };

  fileSystems."/home/florian/.var/app/com.spotify.Client" = {
    device = "/bigdata/documents/com.spotify.Client";
    options = [ "bind" ];
  };

  fileSystems."/home/florian/Documents/programming/rust/rust" = {
    device = "/bigdata/documents/programming/rust/rust";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/meetings" = {
    device = "/home/florian/Documents/meetings";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/windoof" = {
    device = "/home/florian/Documents/windoof";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/fh" = {
    device = "/home/florian/Documents/fh-dblab";
    options = [ "bind" ];
  }; 

  services.openssh.openFirewall = false;
  networking.firewall.trustedInterfaces = [ "enp3s0f1" ];

  #services.openssh = {
  #  enable = false;
  #  listenAddresses = [
  #    # Mi
  #    {
  #      addr = "192.168.178.108";
  #      port = 22;
  #    }
  #    # We
  #    #{
  #    #  addr = "192.168.178.48";
  #    #  port = 22;
  #    #}
  #  ];
  #};

  networking.hosts = {
    "10.200.7.250" = [ "localhost.qemu-meeting" "qemu-meeting" ];
    #10.200.7.189               localhost.windoof       windoof
    "10.200.7.108" = [ "localhost.kde-dev" "kde-dev" ];
    "10.200.7.87" = [ "localhost.fh-dblab" "fh-dblab" ];
    "192.168.178.93" = [ "fritz.box.flos-pi3" "flos-pi3" ];
    "192.168.178.94" = [ "fritz.box.flos-pi4" "flos-pi4" ];
  }; 
}
