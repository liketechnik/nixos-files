{ suites, ... }:
{
  imports = suites.base;

  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "de_DE.UTF-8";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.availableKernelModules = [ "ahci" "xhci_pci" "virtio_pci" "sr_mod" "virtio_blk" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-intel" ];

  services.spice-vdagentd.enable = true;
  fileSystems."/mnt/host" = {
    device = "10.200.7.1:/srv/nfs/kde-dev";
    fsType = "nfs";
  };

  boot.initrd.luks.devices = {
    root = {
      device = "/dev/disk/by-uuid/138230b9-ab08-4b87-85b7-5f551af26241";
    };
  };

  fileSystems."/" = { 
    device = "/dev/disk/by-uuid/8f3bfe25-006a-4aa1-886c-8810ec445a27";
    fsType = "ext4";
  };

  fileSystems."/boot" = { 
    device = "/dev/disk/by-uuid/091E-F617";
    fsType = "vfat";
  };

  swapDevices = [ 
    { 
      device = "/dev/disk/by-uuid/d6a9f9bb-5634-4ed5-a90d-8b91f686e6e3"; 
    }
  ];

  fileSystems."/home/florian/Documents/linux_iamges" = {
    device = "/bigdata/documents/linux_images";
    options = [ "bind" ];
  };

  fileSystems."/home/florian/.var/app/com.valvesoftware.Steam" = {
    device = "/bigdata/documents/com.valvesoftware.Steam";
    options = [ "bind" ];
  };

  fileSystems."/home/florian/.var/app/com.spotify.Client" = {
    device = "/bigdata/documents/com.spotify.Client";
    options = [ "bind" ];
  };

  fileSystems."/home/florian/Documents/programming/rust/rust" = {
    device = "/bigdata/documents/rust/rust";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/meetings" = {
    device = "/home/florian/Documents/meetings";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/windoof" = {
    device = "/home/florian/Documents/windoof";
    options = [ "bind" ];
  };

  fileSystems."/srv/nfs/fh" = {
    device = "/home/florian/Documents/fh-dblab";
    options = [ "bind" ];
  }; 

}
