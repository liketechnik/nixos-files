{
  description = "Florians system configuration";


  inputs.nixpkgs.url = "github:NixOs/nixpkgs/nixos-unstable";
  inputs.nixpkgs-unstable.url = "github:NixOs/nixpkgs/nixos-unstable";
  inputs.nixpkgs-eighteen.url = "github:NixOs/nixpkgs/nixos-18.09";
  inputs.nixpkgs-eighteen.flake = false;

  inputs.home-manager.url = "github:nix-community/home-manager/master"; # keep in sync with nixpkgs
  inputs.home-manager.inputs = { nixpkgs.follows = "nixpkgs"; };

  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  inputs.alacritty = {
    url = "github:zenixls2/alacritty/ligature";
    flake = false;
  };

  inputs.neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
  inputs.neovim-nightly-overlay.inputs = { nixpkgs.follows = "nixpkgs"; };

  inputs.sops-nix.url = "github:Mic92/sops-nix";
  inputs.sops-nix.inputs.nixpkgs.follows = "nixpkgs";

  inputs.fenix.url = "github:nix-community/fenix";
  inputs.fenix.inputs.nixpkgs.follows = "nixpkgs";

  inputs.webcord.url = "github:fufexan/webcord-flake";
  inputs.webcord.inputs.nixpkgs.follows = "nixpkgs";

  inputs.ironbar = {
    url = "github:JakeStanger/ironbar";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  inputs.anyrun = {
    url = "github:Kirottu/anyrun";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  inputs.watershot = {
    url = "github:Kirottu/watershot";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  inputs.lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.91.1-1.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  outputs = { self, nixpkgs, sops-nix, flake-compat, webcord, lix-module, ... }@inputs: let
    inherit (nixpkgs) lib;

    systems = [ "x86_64-linux" ];

    forSystems = lib.genAttrs systems;

    defaultModules = [
        # modules that should always be available
        sops-nix.nixosModules.sops
        ./modules/sops-expand
        ./modules/wireguard
        ./modules/postgresql

        # core config (e. g. make flakes work in the built system, import modules, etc.)
        "${inputs.home-manager}/nixos"
        ./profiles/core
        ./profiles/overlay

        # system wide profiles
        # ./profiles/kakoune
        ./profiles/neovim
        ./profiles/wezterm
        ./profiles/kdecolors

        # users
        ./users/florian
        ./users/root

        ({pkgs, sops, ... }: {
          sops.defaultSopsFile = ./secrets/common.yaml;
          sops.age.keyFile = "/var/lib/sops-nix/key.txt";
          # generate a new key if none exists
          sops.age.generateKey = true;
          sops.gnupg.sshKeyPaths = [];
        })

        lix-module.nixosModules.default
        ({pkgs, sops, ...}: {
          nix.settings.extra-substituters = [
            "https://cache.lix.systems"
          ];

          nix.settings.trusted-public-keys = [
            "cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o="
          ];
        })
    ];
  in {
    nixosConfigurations.florian-76k = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";

      specialArgs.inputs = inputs;
      specialArgs.flakeSelf = self;

      modules = defaultModules ++ [ 
        # host config
        ./hosts/florian-76k.nix

        # system wide profiles
        ./profiles/develop
        ./profiles/laptop
        ./profiles/networking
        ./profiles/printing
        ./profiles/tools
        ./profiles/virt
        # ./profiles/monitoring
        ./profiles/prometheus
        ./profiles/ssh

        ./profiles/postgresql
      ];
    };
    nixosConfigurations.mantis = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";

      specialArgs.inputs = inputs;
      specialArgs.flakeSelf = self;

      modules = defaultModules ++ [ 
        # host config
        ./hosts/mantis.nix

        # system wide profiles
        ./profiles/develop
        ./profiles/fh
        ./profiles/gaming
        ./profiles/graphical 
        ./profiles/laptop
        ./profiles/networking
        ./profiles/printing
        ./profiles/tools
        ./profiles/virt
        # ./profiles/monitoring
        ./profiles/prometheus
        ./profiles/ssh

        ./profiles/caldav-client

        ./profiles/postgresql
      ];
    };
    nixosConfigurations.chameleon = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";

      specialArgs.inputs = inputs;
      specialArgs.flakeSelf = self;

      modules = defaultModules ++ [
        # host config
        ./hosts/chameleon.nix

        # system wide profiles
        ./profiles/ssh
        ./profiles/virt
        ./profiles/networking
        ./profiles/gitlab-runner
        ./profiles/prometheus
        ./profiles/postgresql

        ./profiles/staticman
        ./profiles/nginx
        ./profiles/jupyterhub
        ./profiles/mail-server
        ./profiles/rss2email
        ./profiles/grocy
        ./profiles/taskserver
        ./profiles/caldav-server
        ./profiles/woodpecker

        ./profiles/fh-server
      ];
    };

    packages = forSystems(system: let
      pkgs = import nixpkgs { inherit system; };
    in {
      test-btrfs-boot-snapshots = import ./tests/btrfs-boot-snapshots.nix { inherit pkgs self; };
    });
  };
}
