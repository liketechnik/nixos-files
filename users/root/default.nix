{ ... }:
{
  users.users.root.initialPassword = "nixos";

  home-manager.users.root = { pkgs, home, ... }: {
    imports = [
      ../profiles/direnv
      ../profiles/git
      ../profiles/inputrc
      ../profiles/starship
      # ../profiles/tab
      ../profiles/zoxide
      ../profiles/bat
      ../profiles/zathura
    ];

    home.homeDirectory = "/root";
    home.stateVersion = "18.09";
  };
}
