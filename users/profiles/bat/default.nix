{ pkgs, ... }:
let
in {
  xdg.configFile."bat/config".text = ''
    --theme="ansi"
    --tabs=4
  '';
}

