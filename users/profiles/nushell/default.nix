{ config, pkgs, lib, ... }:

{
  xdg.configFile."nushell/config.nu".source = ./config.nu;
  xdg.configFile."nushell/env.nu".source = ./env.nu;

  home.activation.zoxideNushellInit = lib.warn "zoxide nushell init has nu deprectation warnings"
    lib.hm.dag.entryAfter ["writeBoundary"] ''
      $DRY_RUN_CMD zoxide init nushell --hook prompt > $HOME/.cache/zoxide-init.nu
      # this fixes the deprecation warnings for now :)
      $DRY_RUN_CMD sed -i -e 's|def-env|def --env|' $HOME/.cache/zoxide-init.nu
  '';
}
