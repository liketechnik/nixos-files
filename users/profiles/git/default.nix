{ pkgs, ... }:
let
  # this should be a safer way for git aliases with commands...
  git-context = name: email: msmtp-acc: pkgs.writeShellScriptBin "git-${name}" ''
    git config user.email '${email}'
    git config sendemail.sendmailCmd 'msmtp --account ${msmtp-acc}'
    git config sendemail.bcc '${email}'
  '';
  git-cefh = git-context "cefh" "florian_filip.warzecha@hsbi.de" "hsbi";
  git-cep = git-context "cep" "liketechnik@disroot.org" "disroot";
in {
  home.packages = [
    git-cefh
    git-cep

    pkgs.jujutsu
  ];

  xdg.configFile."jj/config.toml".source = ./jj.toml;

  programs.git = {
    enable = true;

    package = pkgs.gitAndTools.gitFull;

    extraConfig = {
      sendemail.annotate = "yes";
      sendemail.multiEdit = true;
      sendemail.confirm = "always";
      pull.rebase = false;
      pull.ff = "only";
      init.defaultBranch = "main";
      merge.tool = "vimdiff3";
      mergetool.path = "nvim";
      diff.tool = "nvimdiff";
      difftool.prompt = false;
      "difftool \"nvimdiff\"".cmd = ''nvim -d "$LOCAL" "$REMOTE"'';
      format.signOff = "yes";
      format.coverLetter = "auto";
    };

    aliases = {
      au = "add --update"; # only add changes on files already known to git (includes deletions)
      pl = "pull";
      plum = "pull upstream master";
      plr = "pull --rebase";
      ps = "push";
      pf = "push --force";
      rom = "rebase origin/master";
      puo = "push --set-upstream origin";
      s = "status";
      c = "commit";
      ca = "commit --amend";
      yeet = "push";
      feet = "push --force";
      br = "branch --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(contents:subject) %(color:green)(%(committerdate:relative)) [%(authorname)]' --sort=-committerdate"; # taken from https://snyk.io/blog/10-git-aliases-for-faster-and-productive-git-workflow/
      co = "checkout";
      cob = "checkout -b";
      rc = "rebase --continue";
      # cefh = "config user.email 'florian_filip.warzecha@hsbi.de'";
      # cep = "config user.email 'liketechnik@disroot.org'";
      r = "restore";
      rs = "restore --staged";
      ds = "diff --staged";
    };

    delta.enable = true;
    delta.options = {
      tabs = 4;
      navigate = true;
      light = false;
      dark = true;
      line-numbers = true;
      syntax-theme = "ansi";
      line-numbers-minus-style = "red";
      line-numbers-zero-style = "normal";
      line-numbers-plus-style = "green";
      minus-style = "normal normal dim";
      minus-non-emph-style = "normal normal dim";
      minus-emph-style = "normal normal dim bold";
      minus-empty-line-marker-style = "normal red";
      plus-style = "syntax normal";
      plus-non-emph-style = "syntax normal";
      plus-emph-style = "syntax normal bold";
      plus-empty-line-marker-style = "normal green";
      zero-style = "syntax italic normal";
      # commit-style = "syntax";
      # file-style = "green";
    };

    ignores = [ 
      "*~" 
      "tab.yml"
      "Session.vim"
      ".ccls-cache"
      ".cache"
      "compile_commands.json"
      "compile_flags.txt"
      "*.jjignore.*"
    ];

    lfs.enable = true;

    signing.key = "92D8A09D03DDB774AABD53B9E1362F07D750DB5C";
    signing.signByDefault = true;

    userName = "Florian Warzecha";
  };
}
