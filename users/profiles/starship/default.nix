{ pkgs, lib, ... }:
{
  programs.bash.enable = true;
  programs.starship = {
    enable = true;
    package = pkgs.starship;
    enableBashIntegration = true;
    settings = {
      format = "$all";
      add_newline = true;
      line_break = {
        disabled = false;
      };
      character = {
        success_symbol = "[](bold green)";
        error_symbol = "[](bold red)";
        vicmd_symbol = "[](bold green)";
      };
      battery.display = [
        {
          threshold = 20;
          style = "bold red";
        }
        {
          threshold = 30;
          style = "bold yellow";
        }
      ];
      directory = {
        truncation_length = 8;
        truncation_symbol = "../";
      };
      git_commit = {
        only_detached = false;
      };
      git_status = {
        ahead = "\${count}";
        diverged = "\${ahead_count}⇣\${behind_count}";
        behind = "\${count}";
      };
      memory_usage = {
        disabled = false;
        threshold = -1;
        format = "using [\${ram} \${ram_pct}( | \${swap} \${swap_pct})]($style) mem ";
        style = "bold green";
      };
      package = {
        symbol = " ";
      }; 
      python = {
        symbol = " ";
      };
      rust = {
        symbol = " ";
      };
      status = {
        style = "bg:red";
        format = "[$symbol$status_common_meaning$status_signal_name$status_maybe_int]($style) ";
        disabled = false;
        map_symbol = true;
      };
      time = {
        disabled = false;
        format = " [$time]($style) ";
        time_format = "%H:%M:%S";
      };
      # custom.tab = {
      #   description = "The current tab in the tab terminal multiplexer";
      #   command = "tab --starship";
      #   when = "tab --starship";
      #   shell = ["sh"];
      #   format = "[$output]($style) ";
      #   style = "bold blue";
      # };
    };
  };
}
