{ config, pkgs, lib, ... }:
{
  xdg.configFile."vis/config".source = ./config;
  xdg.configFile."vis/colors/base16_glowing-greens".source = ./colors/base16_glowing-greens;
}
