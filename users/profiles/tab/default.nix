{ pkgs, ... }:
{
  imports = [ ../../modules/programs/tab.nix ];

  programs.tab = {
    enable = true;
    package = pkgs.unstable.tab-rs;
    settings = {
      shell = "bash";
      doc = "global workspace";
      workspace = [
        {
          workspace = "/home/florian/Documents/programming";
        }
        {
          workspace = "/home/florian/Documents/fh";
        }
        {
          workspace = "/home/florian/Documents/games/minecraft/eigene_modpacks";
        }
        {
          tab = "notes";
          dir = "/home/florian/Notes";
          doc = "Nextcloud synced Textfiles";
        }
        {
          tab = "pass";
          dir = "/home/florian/.password-store";
          doc = "Password manager";
        }
        {
          tab = "tlp";
          dir = "/home/florian";
          doc = "power savings management";
        }
        {
          tab = "irc";
          dir = "/home/florian";
          doc = "tiny instance";
        }
        {
          tab = "diary";
          dir = "/home/florian/Documents/private/diary";
          doc = "diary entries";
        }
        {
          tab = "blog";
          dir = "/home/florian/Documents/private/blog";
          doc = "diary/todos/notes/etc entries";
        }
      ];
      key_bindings = [
        {
          action = "Disconnect";
          keys = "ctrl-P";
        }
        {
          action = "SelectInteractive";
          keys = "ctrl-T";
        }
      ];
    };
  };
}
