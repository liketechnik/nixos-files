{ pkgs, ... }:
let
in {
  xdg.configFile."waybar/config".source = ./waybar/config;
  xdg.configFile."waybar/style.css".source = ./waybar/style.css;
  home.file.".waybar-scripts/dunst.sh".source = ./waybar-scripts/dunst.sh;
  home.file.".waybar-scripts/spotify.sh".source = ./waybar-scripts/spotify.sh;

  xdg.configFile."dunst/dunstrc".source = ./dunstrc;

  home.file.".swaylock/config".source = ./swaylock-config;
  home.file."swayidle/config".source = ./swayidle-config;

  xdg.configFile."river/scripts/actions.sh".source = ./actions.sh;
  xdg.configFile."river/init".source = ./init;

  home.sessionVariables.NIXOS_OZONE_WL = "1";
}
