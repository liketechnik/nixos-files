#!/usr/bin/env bash
set -e -o pipefail

function notify() {
    notify-desktop -u normal -t 5000 "$1" "$2"
}

function rfkill-action() {
    set +e +o pipefail
    output=$(pkexec --user root rfkill "$@" 2>&1)
    exit_status=$?
    set -e -o pipefail

    if [[ ! $exit_status -eq 0 ]]
    then
        notify "rfkill" "$output"
    fi
}

function modprobe-action() {
    set +e +o pipefail
    output=$(pkexec --user root modprobe "$@" 2>&1)
    exit_status=$?
    set -e -o pipefail

    if [[ ! $exit_status -eq 0 ]]
    then
        notify "modprobe" "$output"
        exit
    fi
}

function cpu-profile-set() {
    set +e +o pipefail
    output=$(pkexec --user root "$@" 2>&1)
    exit_status=$?
    set -e -o pipefail

    if [[ ! $exit_status -eq 0 ]]
    then
        notify "cpu profile set" "$output"
        exit
    fi
}

function tailor-profile-set() {
    set +e +o pipefail
    output=$(tailor profile set "$1")
    exit_status=$?
    set -e -o pipefail

    if [[ ! $exit_status -eq 0 ]]
    then
        notify "tailor profile set $1" "$output"
        exit
    fi
}

function enable-wifi() {
    if rfkill list wifi | grep --quiet yes
    then
        rfkill-action unblock wlan
        # give some time for enabling to happen
        sleep 2
    fi
}

function enable-bluetooth() {
    if rfkill list bluetooth | grep --quiet yes
    then
        rfkill-action unblock bluetooth
        # give some time for enabling to happen
        sleep 2
    fi
}

choices='
swaylock
suspend
shutdown
shutdown now
shutdown cancel
grimflow copy area
grimflow save area
swappy copy area
swappy save area
bluetoothctl
bluetooth in-ears
bluetooth headset
bluetooth disconnect
nmtui
nmtui-connect
polymc
pass
pass otp
rfkill
rfkill block bluetooth
rfkill block wlan
rfkill unblock bluetooth
rfkill unblock wlan
rfkill block all
rfkill unblock all
webcam enable
webcam disable
iwctl station wlan0 scan
cpu profile default
cpu profile quiet
cpu profile performance
cpu profile show
'
# tail is used to strip the first empty line
# was:
# background: base02, text: base05, match: base0C,
# selection: base01, selection-text: base03, border: base05
# was 2:
# background: base00, text: base05, match: base0E, selection: base02,
# selection-text: base0B, base01
# is:
# transparency: D9 (85%)
# background: base00
# selection: base02
# border: base01
# text: base05
# selection-text: base06
# match: base0E
# selection-match: base17
# counter-clor: ? can't se any use
# placeholder-color: base03
# prompt-color: base05
# input-color: base06
choice=$(printf '%s' "$choices" | tail -n +2 - | fuzzel --background-color=070d0bd9 --text-color=cafbe9d9 --match-color=f1e0fed9 --selection-color=2a3c35d9 --selection-text-color=d3ffeed9 --border-color=131e1ad9 --selection-match-color=f8f0ffd9 --placeholder-color=a2f7d9d9 --prompt-color=cafbe9d9 --input-color=d3ffeed9 --dmenu --no-sort --layer=overlay --font "Recursive Mn Lnr St:size=16")

case $choice in
    'swaylock')
        swaylock
        ;;
    'suspend')
        notify "$(systemctl suspend)"
        ;;
    'shutdown')
        notify "$(shutdown -hP +1)"
        ;;
    'shutdown now')
        notify "$(shutdown -hP now)"
        ;;
    'shutdown cancel')
        shutdown -c
        notify "Canceled shutdown"
        ;;
    'swappy copy area')
        grimflow --notify save area - | swappy -f - -o - | wl-copy --type image/png && notify Screenshot "Copied to clipboard"
        ;;
    'swappy save area')
        path=$(echo "" |  fuzzel --background-color=070d0bd9 --text-color=cafbe9d9 --match-color=f1e0fed9 --selection-color=2a3c35d9 --selection-text-color=d3ffeed9 --border-color=131e1ad9 --selection-match-color=f8f0ffd9 --placeholder-color=a2f7d9d9 --prompt-color=cafbe9d9 --input-color=d3ffeed9 --prompt="Path (.png): " --no-sort --dmenu --layer=overlay --font "Recursive Mn Lnr St:size=16")
        grimflow --notify save area - | swappy -f - -o "$path" && notify Screenshot "Saved to $path"
        ;;
    'grimflow copy area')
        grimflow --notify copy area
        ;;
    'grimflow save area')
        # was:
        # background: base02, text: base05, match: base0C,
        # selection: base01, selection-text: base03, border: base05
        # is
        # background: base00, text: base05, match: base0E, selection: base02,
        # selection-text: base0B, base01
        # is:
        # transparency: D9 (85%)
        # background: base00
        # selection: base02
        # border: base01
        # text: base05
        # selection-text: base06
        # match: base0E
        # selection-match: base17
        path=$(echo "" | fuzzel --background-color=070d0bd9 --text-color=cafbe9d9 --match-color=f1e0fed9 --selection-color=2a3c35d9 --selection-text-color=d3ffeed9 --border-color=131e1ad9 --selection-match-color=f8f0ffd9 --placeholder-color=a2f7d9d9 --prompt-color=cafbe9d9 --input-color=d3ffeed9 --prompt="Path (.png): " --no-sort --dmenu --layer=overlay --font "Recursive Mn Lnr St:size=16")
        grimflow --notify save area "$path"
        ;;
    'bluetoothctl')
        exec wezterm start -- bluetoothctl
        ;;
    'bluetooth in-ears')
        enable-bluetooth
        notify "$(bluetoothctl connect 74:45:CE:BF:6B:2D)"
        ;;
    'bluetooth headset')
        enable-bluetooth
        notify "$(bluetoothctl connect 38:18:4C:6D:B7:47)"
        ;;
    'bluetooth disconnect')
        notify "$(bluetoothctl disconnect)"
        ;;
    'nmtui')
        exec wezterm start -- nmtui
        ;;
    'nmtui-connect')
        exec wezterm start -- nmtui-connect
        ;;
    'polymc') # temporary, until polymc has a desktop file or can be started without arguments (yes I'm too lazy to write a nix wrapper around the command)
        exec polymc --dir /home/florian/.local/share/polymc
        ;;
    'pass')
        # was:
        # background: base02, text: base05, match: base0C,
        # selection: base01, selection-text: base03, border: base05
        # is
        # background: base00, text: base05, match: base0E, selection: base02,
        # selection-text: base0B, base01
        # is:
        # transparency: D9 (85%)
        # background: base00
        # selection: base02
        # border: base01
        # text: base05
        # selection-text: base06
        # match: base0E
        # selection-match: base17
        entry="$(cd ~/.password-store && fd --no-ignore --hidden --type f --extension gpg --exclude otp/ -x 'echo' '{.}' ';' | fuzzel --background-color=070d0bd9 --text-color=cafbe9d9 --match-color=f1e0fed9 --selection-color=2a3c35d9 --selection-text-color=d3ffeed9 --border-color=131e1ad9 --selection-match-color=f8f0ffd9 --placeholder-color=a2f7d9d9 --prompt-color=cafbe9d9 --input-color=d3ffeed9 --prompt="Entry: " --dmenu --layer=overlay --font "Recursive Mn Lnr St:size=16")"

        output_fd=$(mktemp)
        pass show -c "$entry" >"$output_fd" 2>&1 & disown
        pass_pid=$!

        while [[ $(du "$output_fd" | head --bytes=1) -eq 0 ]]
        do
            set +e
            ps -p $pass_pid >/dev/null 2>&1
            process_alive=$?
            set -e

            if [[ (! $process_alive -eq 0) && ($(du "$output_fd" | head --bytes=1) -eq 0) ]] 
            then
                echo "Serious Problem: pass process died without any output!" > "$output_fd"
            fi
        done

        notify "pass" "$(cat "$output_fd")"
        rm "$output_fd"
        ;;
    'pass otp')
        # was:
        # background: base02, text: base05, match: base0C,
        # selection: base01, selection-text: base03, border: base05
        # is
        # transparency: D9 (85%)
        # background: base00, text: base05, match: base0E, selection: base02,
        # selection-text: base0B, base01
        # is:
        # background: base00
        # selection: base02
        # border: base01
        # text: base05
        # selection-text: base06
        # match: base0E
        # selection-match: base17
        entry="$(cd ~/.password-store && fd --no-ignore --hidden --type f --extension gpg -x 'echo' '{.}' ';' --search-path otp | fuzzel --background-color=070d0bd9 --text-color=cafbe9d9 --match-color=f1e0fed9 --selection-color=2a3c35d9 --selection-text-color=d3ffeed9 --border-color=131e1ad9 --selection-match-color=f8f0ffd9 --placeholder-color=a2f7d9d9 --prompt-color=cafbe9d9 --input-color=d3ffeed9 --prompt="Entry: " --dmenu --layer=overlay --font "Recursive Mn Lnr St:size=16")"

        output_fd=$(mktemp)
        pass otp -c "$entry" >"$output_fd" 2>&1 & disown
        pass_pid=$!

        while [[ $(du "$output_fd" | head --bytes=1) -eq 0 ]]
        do
            set +e
            ps -p $pass_pid >/dev/null 2>&1
            process_alive=$?
            set -e

            if [[ (! $process_alive -eq 0) && ($(du "$output_fd" | head --bytes=1) -eq 0) ]] 
            then
                echo "Serious Problem: pass process died without any output!" > "$output_fd"
            fi
        done

        notify "pass otp" "$(cat "$output_fd")"
        rm "$output_fd"
        ;;
    'rfkill')
        set +e +o pipefail
        # shellcheck disable=SC2016
        output=$(nu -c 'rfkill --output TYPE,DEVICE,SOFT,HARD --json | from json | get rfkilldevices | each {|device| [$device.device " (" $device.type "): " $device.soft " (" $device.hard ")"] | str collect } | str collect (char newline)' 2>&1)
        set -e -o pipefail

        notify "rfkill" "$output"
        ;;
    'rfkill block bluetooth')
        rfkill-action block bluetooth
        ;;
    'rfkill unblock bluetooth')
        rfkill-action unblock bluetooth
        ;;
    'rfkill block wlan')
        rfkill-action block wlan
        ;;
    'rfkill unblock wlan')
        rfkill-action unblock wlan
        ;;
    'rfkill block all')
        rfkill-action block wlan bluetooth
        ;;
    'rfkill unblock all')
        rfkill-action unblock wlan bluetooth
        ;;
    'webcam enable')
        modprobe-action uvcvideo
        notify "webcam" "enabled"
        ;;
    'webcam disable')
        modprobe-action -r uvcvideo
        notify "webcam" "disabled"
        ;;
    'iwctl station wlan0 scan')
        enable-wifi
        iwctl station wlan0 scan
        ;;
# TODO: this should be host specific...
    'cpu profile default')
        # was 1400000 / 2900000 for acpi-cpufreq
        max_freq="2900000"
        # had echo 0> /sys/devices/system/cpu/cpufreq/boost when not using amd-pstate active
        cpu-profile-set bash -c "for cpu in /sys/devices/system/cpu/cpu{1..15}; do echo 1 > \$cpu/online; done && for cpu in /sys/devices/system/cpu/cpu*/cpufreq/; do echo powersave > \$cpu/scaling_governor && echo balance_power > \$cpu/energy_performance_preference && echo ${max_freq} > \$cpu/scaling_max_freq; done"
        tailor-profile-set default
        notify "cpu profile set" "default"
        ;;
    'cpu profile quiet')
        # was 1400000 for acpi-cpufreq / nonactive amd-psate
        max_freq="1416000"
        cpu-profile-set bash -c "for cpu in /sys/devices/system/cpu/cpu{0..7}/cpufreq/; do echo powersave > \$cpu/scaling_governor && echo power > \$cpu/energy_performance_preference && echo ${max_freq} > \$cpu/scaling_max_freq; done && for cpu in /sys/devices/system/cpu/cpu{1..7}/online; do echo 1 > \$cpu; done && for cpu in /sys/devices/system/cpu/cpu{8..15}/online; do echo 0 > \$cpu; done"
        tailor-profile-set quiet
        notify "cpu profile set" "quiet"
        ;;
    'cpu profile performance')
        max_freq="4300000"
        cpu-profile-set bash -c "for cpu in /sys/devices/system/cpu/cpu{1..15}; do echo 1 > \$cpu/online; done && for cpu in /sys/devices/system/cpu/cpu*/cpufreq/; do echo powersave > \$cpu/scaling_governor && echo balance_performance > \$cpu/energy_performance_preference && echo ${max_freq} > \$cpu/scaling_max_freq; done"
        tailor-profile-set performance
        notify "cpu profile set" "performance"
        ;;
    'cpu profile show')
        if [ -e /sys/devices/system/cpu/cpufreq/boost ]
        then
            if [ "$(cat /sys/devices/system/cpu/cpufreq/boost)" -eq 0 ]
            then
                boost=off
            else
                boost=on
            fi
        else
            boost="n/a"
        fi
        available_governors="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors)"
        governor="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)"
        frequency_min="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq)"
        frequency_max="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq)"
        # match amd-psate or amd-pstate-epp
        if [[ "$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_driver)" =~ amd-pstate.* ]]
        then
            frequency_range="$(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq) / $(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq)"
        else
            frequency_range="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies)"
        fi
        if [ -e /sys/devices/system/cpu/cpu0/cpufreq/energy_performance_preference ]
        then
            epp="$(cat /sys/devices/system/cpu/cpu0/cpufreq/energy_performance_preference)"
            available_epps="$(cat /sys/devices/system/cpu/cpu0/cpufreq/energy_performance_available_preferences)"
        else
            epp="n/a"
            available_epps="n/a"
        fi
        cores="$(cat /sys/devices/system/cpu/online)"
        cores_off="$(cat /sys/devices/system/cpu/offline)"
        total_cores="$(cat /sys/devices/system/cpu/cpu*/online | wc --lines)"
        # cpu0 cannot be taken offline, and therefore has no online file
        total_cores=$((total_cores + 1))
        tailor="$(tailor profile list | grep active | cut -d '(' -f 1)"
        tailor_available="$(tailor profile list | grep --invert active | tr '\n' ' ')"

        notify "cpu profile show" "
Governor: ${governor} (${available_governors})
Min/Max Frequency: ${frequency_min} / ${frequency_max} (${frequency_range})
Boost: ${boost}
Enabled Cores: ${cores}
Disabled Cores: ${cores_off}
Total Cores: ${total_cores}
EPP: ${epp} (${available_epps})
Tailor: ${tailor} (${tailor_available})
"
        ;;
    *)
        set +e +o pipefail
        output=$($choice 2>&1)        
        exit_status=$?
        set -e -o pipefail

        notify "Command Execution" "$(echo -e "Command: '$choice'\nExit Code: $exit_status\n\n$output")"
        ;;
esac
