#!/usr/bin/env bash

function printStatus() {
    if [[ -n $(echo $(playerctl -p spotify status) | grep Playing) ]]; then
        icon="󰐊 "
    else
        icon="󰏤 "
    fi

    artist="$(playerctl -p spotify metadata artist)"
    title="$(playerctl -p spotify metadata title)"
    album="$(playerctl -p spotify metadata album)"

    # (some) similiarity between title/album,
    # so remove album
    if [[ -n $(echo "$title" | agrep -i -5 "$album") ]]; then
        album=""
    else
        album=" 󰌳 ${album}"
    fi

    echo $(echo "${icon} 󰠃 ${artist} 󰎄 ${title}${album}" | sed -e 's/&/&amp;/g' -e 's/Extended Mix//g')
}

while IFS= read -r line
do
    printStatus
done < <(playerctl -p spotify metadata --format "{{artist}}{{album}}{{title}}{{status}}" --follow)
