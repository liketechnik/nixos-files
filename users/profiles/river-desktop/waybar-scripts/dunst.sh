#!/usr/bin/env bash

if dunstctl is-paused | grep -q "false"
then
    COUNT="$(dunstctl count displayed)"
    if [[ "${COUNT}" -gt 0 ]]
    then
        SYMBOL="󰂞 "
    else
        SYMBOL="󰂚 "
    fi
else
    COUNT="$(dunstctl count waiting)"
    if [[ "${COUNT}" -gt 0 ]]
    then
        SYMBOL="󱅫 "
    else
        SYMBOL="󰂛 "
    fi
fi

echo "${COUNT} ${SYMBOL}"
