{ config, lib, pkgs, ... }:
let
  bartib-unwrapped = import ../../../packages/bartib.nix { pkgs = pkgs; };
  bartib = pkgs.symlinkJoin {
    name = "wezterm-${bartib-unwrapped.version}";

    nativeBuildInputs = [ pkgs.makeWrapper ];

    paths = [ bartib-unwrapped ];

    postBuild = ''
      rm "$out/bin/bartib"
      makeWrapper "${bartib-unwrapped}/bin/bartib" "$out/bin/bartib" --add-flags "-f ${config.home.homeDirectory}/activities.bartib"
    '';
  };
in {
  home.packages = [ bartib ];
}
