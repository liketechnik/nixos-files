{ pkgs, ... }:
let
in {
  xdg.configFile."zathura/zathurarc".text = ''
    set adjust-open "best-fit"
    set pages-per-row 1
    set font "Recursive Mn Lnr St normal 10"
    set selection-clipboard clipboard
    set page-padding 6
    set window-title-home-tilde true
    set window-title-page true
    set statusbar-page-percent true
    set statusbar-home-tilde true

    map <A-0> adjust_window best-fit
    map <C-=> zoom in
    map <C--> zoom out
  '';
}
