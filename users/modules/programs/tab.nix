{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.programs.tab;

  yamlFormat = pkgs.formats.yaml { };
in {
  options.programs.tab = {
    enable = mkEnableOption "tab";

    package = mkOption {
      type = types.package;
      default = pkgs.tab-rs;
      defaultText = literalExample "pkgs.tab-rs";
      description = "package to use";
    };

    settings = mkOption {
      type = yamlFormat.type;
      default = { };
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      home.packages = [ cfg.package ];

      xdg.configFile."tab.yml" = mkIf (cfg.settings != { }) {
        text = builtins.toJSON cfg.settings;
      };
    })
  ];
}

