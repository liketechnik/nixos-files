{ config, pkgs, ... }:
{
  imports = [
    ./tiny.nix
    ./email.nix
  ];

  home-manager.users.florian = { pkgs, home, config, ... }: {
    imports = [
      ../profiles/direnv
      ../profiles/git
      ../profiles/inputrc
      ../profiles/starship
      # ../profiles/tab
      ../profiles/zoxide
      ../profiles/bartib
      ../profiles/bat
      ../profiles/xplr
      ../profiles/zathura

      ../profiles/nushell

      ../profiles/river-desktop

      ../profiles/cli-visualizer
    ];

    home.homeDirectory = "/home/florian";
    home.stateVersion = "18.09";

    xdg.mimeApps = {
      enable = true;

      associations = {
        added = {
          "application/java-archive" = [ "org.kde.ark.desktop" ];
          "application/x-compressed-tar" = [ "org.kde.ark.desktop" ];

          "application/pdf" = [ "org.pwmt.zathura.desktop" "org.kde.okular.desktop" "okularApplication_pdf.desktop" ];

          "image/png" = [ "org.nomacs.ImageLounge.desktop" "org.kde.gwenview.desktop" ];

          "application/octet-stream" = [ "org.kde.kate.desktop" ];
          "text/calendar" = [ "org.kde.kate.desktop" ];
          "text/csv" = [ "org.kde.kate.desktop" ];
          "text/markdown" = [ "org.kde.kate.desktop" ];
          "text/plain" = [ "org.kde.kate.desktop" ];
          "text/x-csrc" = [ "org.kde.kate.desktop" ];
          "text/x-diff" = [ "org.kde.kate.desktop" ];
          "text/xml" = [ "org.kde.kate.desktop" ];

          "x-scheme-handler/mailto" = [ "aerc.desktop" "firefox.desktop" ];
        };
        removed = {
        };
      };
      defaultApplications = {
        "text/xml" = [ "org.kde.kate.desktop" "firefox.desktop" ];
        "text/markdown" = [ "org.kde.kate.desktop" ];
        "text/plain" = [ "org.kde.kate.desktop" ];

        "application/pdf" = [ "org.pwmt.zathura.desktop" "org.kde.okular.desktop" "okularApplication_pdf.desktop" ];

        "image/png" = [ "feh.desktop" "org.nomacs.ImageLounge.desktop" "org.kde.gwenview.desktop" ];
        "image/jpg" = [ "feh.desktop" "org.nomacs.ImageLounge.desktop" "org.kde.gwenview.desktop" ];
        "image/jpeg" = [ "feh.desktop" "org.nomacs.ImageLounge.desktop" "org.kde.gwenview.desktop" ];

        "application/x-compressed-tar" = [ "org.kde.ark.desktop" ];

        "application/x-extension-htm" = [ "firefox.desktop" ];
        "application/x-extension-html" = [ "firefox.desktop" ];
        "application/x-extension-shtml" = [ "firefox.desktop" ];
        "application/x-extension-xht" = [ "firefox.desktop" ];
        "application/x-extension-xhtml" = [ "firefox.desktop" ];
        "application/xhtml+xml" = [ "firefox.desktop" ];
        "text/html" = [ "firefox.desktop" ];

        "x-scheme-handler/chrome" = [ "firefox.desktop" ];
        "x-scheme-handler/http" = [ "firefox.desktop" ];
        "x-scheme-handler/https" = [ "firefox.desktop" ];

        "x-scheme-handler/arc-file" = [ "advanced-rest-client.desktop" ];
        "x-scheme-handler/sgnl" = [ "signal-desktop.desktop" ];
        "x-scheme-handler/signalcaptcha" = [ "signal-desktop.desktop" ];
        "x-scheme-handler/discord" = [ "com.discordapp.Discord.desktop" "discord.desktop" ];

        "application/msword" = [ "writer.desktop" ];
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document" = [ "writer.desktop" ];

        "x-scheme-handler/mailto" = [ "aerc.desktop" "firefox.desktop" ];
      };
    };
  };

  users.users.florian = {
    uid = 1000;
    initialPassword = "nixos";
    description = "Florian Warzecha";
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "video" "libvirtd" "lp" "wireshark" "input" "scanner" "plugdev" ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQD3QVmQbnDcOCViYEjM9HfX317+RaQPTPFZ3jq7U+58irS9ScH04IuKStEE5FskslrP0VLjo/jAscx7Y5Ov94ivHWMCU0aKTLBOFo27vw+ou/huUYScfmt1leMSxMXGyhQvHK9QNImUsXA4sADhgnsM3o52MD4EUwcSu9+U3vmi/Qe8sZHp+F/6PInW/ViIVjtSL7rcfQ7fnN19eGgMk11QqW0r8fG2CeCnwBOAn2P+7fQx9chVnolVPqTmB+hymZJIfiiK46ULEAZ0udWjuM66/MUOWy9xgQv0/OxUNdgUR5o7YyTa+yZS7gIoXlmZSbs7TxzdmZSkE8N9qU6W1J4u3clAtBBM61EpHrfu7uk5I/G9s3NnWsDQnngBqotUKMUo+1C7VFMTLOwgQVRTLk0IY3UTVVijXNtsb2A0TRGkiiRRCsx8gn5ZJpOhvMsM6C9Urf3vQGCnYUGUYpO7pjN+/djlTOCbF+7YjM0PQ0cu1x+Tk8jOdU4P/uB4dyWjnyuB7huMkFPemI3VRfWawoa+OSSQpWG9zGnrbN2O8eu1vDdfmuz3a7qN4KY35G4ZPvvf1y6LES29VF088PPCkYcABBK9xTDn8WhNIDAIo2h8c9w8+WEPCLsL85sVrc5cFUW1JZVo0NUe2I1yUtu/S4zvaSOHPEv+wZWvdyDJPpLrvw== (florian@florian-76k)"
    ];
  };

}
