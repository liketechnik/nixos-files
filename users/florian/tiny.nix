{ config, pkgs, ... }:
{
  home-manager.users.florian = { pkgs, home, config, ... }: {
    home.packages = [ pkgs.tiny ];

    xdg.configFile."tiny/config.yml".source = config.lib.file.mkOutOfStoreSymlink "/run/secrets/irc_pass_expanded";
  };

  sops.secrets.irc_pass = { };

  sops-expand.secrets.tiny-irc-florian = let 
    configText = ''
      servers:
        - addr: "chat.freenode.net"
          port: 6697
          realname: "liketechnik"
          nicks: ["liketechnik"]
          tls: true
          sasl:
            username: "liketechnik"
            password: "REPLACE_PASSWORD"
          join:
            - '#rust'
            - '#home-manager'
            - '#nixos'

      defaults:
        realname: "liketechnik"
        tls: true
        nicks: ["liketechnik"]
    '';
    sourcePath = config.sops.secrets.irc_pass.path;
  in {
    targetFile = "irc_pass_expanded";
    owner = config.users.users.florian.name;
    group = config.users.users.nobody.group;
    expandCmd = ''echo '${configText}' | ${pkgs.gnused}/bin/sed "s/REPLACE_PASSWORD/$(cat ${sourcePath})/"'';
  };
}
