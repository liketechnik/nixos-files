#!/usr/bin/env bash

#
# poll.sh - An example poll script for astroid.
#
# Intended to be run by astroid. See the following link for more information:
# https://github.com/astroidmail/astroid/wiki/Polling
#
# In particular, in order for this script to be run by astroid, it needs to be
# located at ~/.config/astroid/poll.sh
#

# Exit as soon as one of the commands fail.
set -e

# Fetch new mail.
mbsync -a

# Import new mail into the notmuch database.
notmuch new

# Here you can process the mail in any way you see fit. See the following link
# for examples:
# https://github.com/astroidmail/astroid/wiki/Advanced-Polling-and-Processing-mail#automatic-tagging

# notify about unread, new mail
notifymuch

notmuch tag --batch <<EOF

    # Tag email sent from other clients as 'sent'
    # except when it also was sent to an inbox
    +sent -unread -inbox tag:new and (folder:/.*[Ss]ent.*/ or folder:/.*[gG]esendet.*/) 
    +inbox +unread tag:new and folder:/.*[iI]nbox.*/

    # remove draft mail from other clients from inbox and mark as read
    -unread -inbox tag:new and tag:draft

    # We've finished processing incoming mail
    -new tag:new
EOF

