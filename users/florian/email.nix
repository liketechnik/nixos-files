{ config, pkgs, lib, ... }:
let 
  globalconfig = config;
in lib.mkIf (config.networking.hostName == "mantis") {
  home-manager.users.florian = { pkgs, home, config, ... }: {
    xdg.configFile."astroid/config".source = config.lib.file.mkOutOfStoreSymlink "/run/secrets/astroid-config-florian-expanded";
    xdg.configFile."astroid/poll.sh".source = ./email/astroid/poll.sh;
    # from commit 3efcc7c
    xdg.configFile."astroid/ui/part.scss".source = ./email/astroid/part.scss;
    # from commit 9818553
    xdg.configFile."astroid/ui/thread-view.scss".source = ./email/astroid/thread-view.scss;

    xdg.configFile."aerc/aerc.conf".source = ./email/aerc/aerc.conf;
    xdg.configFile."aerc/binds.conf".source = ./email/aerc/binds.conf;

    xdg.configFile."notifymuch/notifymuch.cfg".source = ./email/notifymuch/notifymuch.cfg;

    home.file.".notmuch-config".source = ./email/notmuch-config;

    home.file.".msmtprc".source = config.lib.file.mkOutOfStoreSymlink globalconfig.sops.secrets."email/msmtprc".path;

    home.file.".mbsyncrc".source = config.lib.file.mkOutOfStoreSymlink globalconfig.sops.secrets."email/mbsyncrc".path;

    xdg.configFile."aerc/accounts.conf".source = config.lib.file.mkOutOfStoreSymlink globalconfig.sops.secrets."email/aerc/accounts.conf".path;
    # see accounts.conf
    xdg.configFile."aerc/notmuch-querymap.conf".source = ./email/aerc/notmuch-querymap.conf;
  };

  sops.secrets."email/astroid-accounts" = {
    format = "binary";
    sopsFile = ../../secrets/email/astroid-accounts;
    owner = config.users.users.florian.name;
    group = config.users.users.nobody.group;
  };

  sops.secrets."email/msmtprc" = {
    format = "binary";
    sopsFile = ../../secrets/email/msmtprc;
    owner = config.users.users.florian.name;
    group = config.users.users.nobody.group;
  };

  sops.secrets."email/mbsyncrc" = {
    format = "binary";
    sopsFile = ../../secrets/email/mbsyncrc;
    owner = config.users.users.florian.name;
    group = config.users.users.nobody.group;
  };

  sops.secrets."email/aerc/accounts.conf" = {
    format = "binary";
    sopsFile = ../../secrets/email/accounts.conf;
    owner = config.users.users.florian.name;
    group = config.users.users.nobody.group;
  };

  sops-expand.secrets."email/astroid-config-florian" = let
    configText = ./email/astroid/config;
    sourcePath = config.sops.secrets."email/astroid-accounts".path;
  in {
    targetFile = "astroid-config-florian-expanded";
    owner = config.users.users.florian.name;
    group = config.users.users.nobody.group;
    expandCmd = ''cat ${configText} | ${pkgs.gnused}/bin/sed '/"accounts":/r ${sourcePath}' '';
  };
}
